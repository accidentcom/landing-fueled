<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
	 * 
	 */
	public function index()
	{	
		$test_mode = FALSE;
		if (request()->has('test_mode')) {
			$test_mode = request()->get('test_mode');
		}

		if (request()->has('accident_type')) {
            $accident_type = request()->get('accident_type');
        } else {
            $accident_type = 'Auto Accident';
		}
		
		if (request()->has('redirect_to')) {
            $redirect_to = request()->get('redirect_to');
        } else {
            $redirect_to = 'https://accident.com/';
        }

		$steps = view('home.questions.all_questions');
		return view('home.main', array('steps' => $steps, 'accident_type'=>$accident_type, 'test_mode' => $test_mode, 'redirect_to'=> $redirect_to));
	}

	public function main_auto_wizard(){
		$test_mode = FALSE;
		if (request()->has('test_mode')) {
			$test_mode = request()->get('test_mode');
		}

		if (request()->has('accident_type')) {
            $accident_type = request()->get('accident_type');
        } else {
            $accident_type = 'Car Accident';
		}
		
		if (request()->has('redirect_to')) {
            $redirect_to = request()->get('redirect_to');
        } else {
            $redirect_to = 'https://accident.com/';
        }

		$steps = view('home.questions.all_auto_questions');
		return view('home.main_auto_wiz', array('steps' => $steps, 'accident_type'=>$accident_type, 'test_mode' => $test_mode, 'redirect_to'=> $redirect_to));
	}


	public function v2_form(){
		$test_mode = FALSE;
		if (request()->has('test_mode')) {
			$test_mode = request()->get('test_mode');
		}

		if (request()->has('accident_type')) {
            $accident_type = request()->get('accident_type');
        } else {
            $accident_type = 'Auto Accident';
		}
		
		if (request()->has('redirect_to')) {
            $redirect_to = request()->get('redirect_to');
        } else {
            $redirect_to = 'https://accident.com/';
        }


		$steps = view('home.questions.all_questions');
		return view('home.v2_form', array('steps' => $steps, 'accident_type'=>$accident_type, 'test_mode' => $test_mode,'redirect_to'=> $redirect_to));
	}


	public function auto_wizard(){
		$test_mode = FALSE;
		if (request()->has('test_mode')) {
			$test_mode = request()->get('test_mode');
		}

		if (request()->has('accident_type')) {
            $accident_type = request()->get('accident_type');
        } else {
            $accident_type = 'Car Accident';
		}
		if (request()->has('redirect_to')) {
            $redirect_to = request()->get('redirect_to');
        } else {
            $redirect_to = 'https://accident.com/';
        }
		$steps = view('home.questions.all_auto_questions');
		return view('home.auto_wizard_form', array('steps' => $steps, 'accident_type'=>$accident_type, 'test_mode' => $test_mode, 'redirect_to'=> $redirect_to));
	}

	public function version2()
	{
		$test_mode = FALSE;
		if (request()->has('accident_type')) {
            $accident_type = request()->get('accident_type');
        } else {
            $accident_type = 'Auto Accident';
        }
		if (request()->has('test_mode')) {
			$test_mode = request()->get('test_mode');
		}
		return view('home.mainv2', array('accident_type' => $accident_type, 'test_mode' => $test_mode));
	}

	public function version_typeform()
	{
		$test_mode = FALSE;
		if (request()->has('accident_type')) {
            $accident_type = request()->get('accident_type');
        } else {
            $accident_type = 'Auto Accident';
        }
		if (request()->has('test_mode')) {
			$test_mode = request()->get('test_mode');
		}
		return view('home.main_typeform', array('accident_type' => $accident_type, 'test_mode' => $test_mode));
	}

	public function auto_sparker()
	{
		$test_mode = FALSE;
		if (request()->has('accident_type')) {
            $accident_type = request()->get('accident_type');
        } else {
            $accident_type = 'Car Accident';
        }
		if (request()->has('test_mode')) {
			$test_mode = request()->get('test_mode');
		}
		return view('home.auto_sparker', array('accident_type' => $accident_type, 'test_mode' => $test_mode));
	}


	public function version_auto_injury()
	{
		$test_mode = FALSE;
		if (request()->has('accident_type')) {
            $accident_type = request()->get('accident_type');
        } else {
            $accident_type = 'Car Accident';
        }
		if (request()->has('test_mode')) {
			$test_mode = request()->get('test_mode');
		}
		return view('home.main_auto_injury', array('accident_type' => $accident_type, 'test_mode' => $test_mode));
	}

	public function terms()
	{
		$test_mode = FALSE;
		if (request()->has('accident_type')) {
            $accident_type = request()->get('accident_type');
        } else {
            $accident_type = 'Car Accident';
        }
		if (request()->has('test_mode')) {
			$test_mode = request()->get('test_mode');
		}
		$footerPath = $this->getFooterPath();
		return view('home.terms_of_service', array('accident_type' => $accident_type, 'test_mode' => $test_mode, 'footerPath' => $footerPath));
	}

	/**
	 * Get footer path
	 */
	public function getFooterPath()
	{
		$domain = $_SERVER['SERVER_NAME'];
		$footerPath = 'partials.footer.footer_injury';
		if ($domain == 'injury.accident.com') {
			$footerPath = 'partials.footer.footer_injury';
		} else if ($domain == 'auto.accident.com') {
			$footerPath = 'partials.footer.footer_auto';
		}
		return $footerPath;
	}
}
