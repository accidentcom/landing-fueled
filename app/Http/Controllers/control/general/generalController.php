<?php

namespace App\Http\Controllers\control\general;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Log;
use DateTime;

require_once base_path().'/../AccidentApp.php';

class generalController extends Controller
{
	private $accidentApp = null;
	
	public function __construct()
    {
        try{
			
			$this->accidentApp = new \AccidentApp();
			
        }catch(Exception $e){
            // error occured
			
        }
    }
	
	/**
	 * Encrypt passed string
	 *
	 * @param string $string
	 *
	 * @return encrypted string
	 */
	public function encrypt_string($string) 
	{
		if(!empty($this->accidentApp)){
			return base64_encode(openssl_encrypt($string, $this->accidentApp->getEncrypMethod(), $this->accidentApp->getEncryptKey(), OPENSSL_RAW_DATA, $this->accidentApp->getEncryptIv()));
		}
		return '';
	}
	
	/**
	 * Decrypt passed string
	 *
	 * @param string $string
	 *
	 * @return encrypted string
	 */
	public function decrypt_string($string) 
	{
		if(!empty($this->accidentApp)){
			return openssl_decrypt(base64_decode($string), $this->accidentApp->getEncrypMethod(), $this->accidentApp->getEncryptKey(), OPENSSL_RAW_DATA, $this->accidentApp->getEncryptIv());
		}
		return '';
	}
	
	/**
	 * Function to convert datetime from one to another timezone
	 *
	 * @param $fromTime datetime
	 * @param $fromTimezone timezone
	 * @param $toTimezone datetime
	 * @param $format date format
	 *
	 * @return date in $format
	 */
	public function timeZoneConvert ($fromTime, $fromTimezone = 'UTC', $toTimezone, $format = 'Y-m-d H:i:s') {
		// create a $dt object with the fromTimezone timezone
		$dt = new DateTime($fromTime, new DateTimeZone($fromTimezone));

		// change the timezone of the object without changing it's time
		$dt->setTimezone(new DateTimeZone($toTimezone));

		// format the datetime
		return $dt->format($format);
	}
}