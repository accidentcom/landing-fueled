<?php

namespace App\Http\Controllers\control\lead;
use GuzzleHttp\Client as GuzzleClient;
use App\Http\Controllers\Controller;
use App\Http\Controllers\control\general\generalController;
use App\model\email\emailSent;
use App\model\general\applicationPreferences;
use App\model\lead\lead;
use App\model\lead\leadLocation;
use App\model\lead\leadRawData;
use App\model\lead\leadFourLL;
use DB;
use Hashids\Hashids;
require_once base_path() . '/../AccidentApp.php';
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Intercom;
use Mbarwick83\Shorty\Facades\Shorty;
use Propaganistas\LaravelPhone\PhoneNumber;
use Twilio\Rest\Client;

class LeadController extends Controller
{
    private $accidentApp = null;
    private $twilioRestClientMaster = null;

    public function save_lead(Request $request)
    {
        
        $this->accidentApp = new \AccidentApp();
        $generalControllerObject = new generalController();
        $requestData = $request->all();
        $testMode = !empty($requestData['test_mode'])?$requestData['test_mode'] : 'false';
        $injury = $requestData['data-injury'];
        $four_ll_src = 'ACC';

        $srcData = array(
            'auto accident' => 'ACCAuto',
            'medical malpractice' => 'ACC',
            'personal injury' => 'ACC',
            'workers compensation' => 'ACCWC'
        );
        $typeOfLegal = array(
            'auto accident' => 'Auto Accident Injury',
            'medical malpractice' => 'Medical Malpractice',
            'personal injury' => 'Personal Injury',
            'workers compensation' => 'Workers Comp'
        );
        $fourLLCaseTypes = array(
            'auto accident',
            'workers compensation',
            'medical malpractice',
            'personal injury',
            'slip and fall'
        );
        $accTyp = $requestData['accident_type'];

        $personalInjuryCases = array('slip and fall');

        if (in_array(strtolower($requestData['accident_type']), $personalInjuryCases)) {
            $requestData['accident_type'] = 'Personal Injury';
        }

        $autoInjuryCases = array('car accident', 'motorcycle accident', 'truck accident', 'pedestrian accident', 'work-related accident', 'bicycle accident');

        if (in_array(strtolower($requestData['accident_type']), $autoInjuryCases)) {
            $requestData['accident_type'] = 'Auto Accident';
        }
        
        $ping_data = null;
        if(in_array(strtolower($requestData['accident_type']), $fourLLCaseTypes)){
            $src = $srcData[strtolower($requestData['accident_type'])];
            $ping_data = array(
                'Key' => config('4ll.api_key'),
                'API_Action'=>'pingPostLead',
                'Mode'=>'ping',
                'SRC'=>$src,
                'Return_Best_Price'=>'1',
                'TYPE'=> '31',
                'TCPA'=> 'Yes',
                'Email'=> strtolower($requestData['email']),
                'Have_Attorney'=> (isset($requestData['got_attorney'][$injury])?$requestData['got_attorney'][$injury]:''),
                'Zip'=> $requestData['address_zipcode'],
                'Type_Of_Legal_Problem' => $typeOfLegal[strtolower($requestData['accident_type'])],
                'Incident_Date_Option_B'=>  $requestData['accident_incident_year']
            );
            
            if ($requestData['test_mode'] == 'true') { // check if test mode is 'ON'
				$ping_data['Test_Lead'] = '1';
				$ping_data['Zip'] = '99999';
			}
            
            if($requestData['accident_type'] == 'Auto Accident'){
                $at_fault = $requestData['at_fault'][$injury];
                if($at_fault != 'No'){
                    $at_fault = 'Yes';
                }
                $ping_data += array(
                    'Doctor_Treatment'=> $requestData['medical_treatment'][$injury],
                    'Were_You_At_Fault'=> $at_fault
                );
            }elseif($requestData['accident_type'] == 'Workers Compensation'){
                $ping_data += array(
                    'Doctor_Treatment'=> $requestData['medical_treatment'][$injury]
                );
            }elseif($requestData['accident_type'] == 'Medical Malpractice'){
                $ping_data += array(
                    'Doctor_Treatment'=> $requestData['medical_treatment'][$injury]
                );
            }elseif($requestData['accident_type'] == 'Personal Injury'){
                $ping_data += array(
                    'Doctor_Treatment'=> $requestData['medical_treatment'][$injury]
                );
            }else{
                $ping_data = null;
            }
    
        }
        $caseHistory = '';
        $questions = $requestData['questions'];
        
       /*  $custom_att = $requestData['custom_att']; */
        $hashids = new Hashids(config('constant.SALT_KEY'), config('constant.MIN_HASH_LENGTH'));
        DB::beginTransaction();

        try {

            $leadCaseHistory = $this->buildCaseHistory($questions, $requestData);
            
            $lead_uuid = (string) Str::uuid();
            try {
                $leadContact = (string) PhoneNumber::make($requestData['phone'], 'US');
            } catch (\Exception $e) {
                $leadContact = isset($requestData['phone']) ? (string) $requestData['phone'] : '';
            }
            $address = (!empty($requestData['address'])) ? $requestData['address'] : $requestData['address_zipcode'];
            $requestData['lead_location'] = !empty($address) ? $address : $requestData['addresss_state'];

            /* $custom_data = $this->build_custom_attribute($requestData, $custom_att); */
            $created_at = time();

            $new_lead_data = [
                'name' => $requestData['first_name'] . " " . $requestData['last_name'],
                'phone' => $leadContact,
                'email' => strtolower($requestData['email']),
                'created_at' => $created_at,/* 
                "custom_attributes" => $custom_data, */
            ];

            $lead = new lead;
            $lead->lead_name = $generalControllerObject->encrypt_string($requestData['first_name'] . " " . $requestData['last_name']);
            $lead->lead_email = $generalControllerObject->encrypt_string(strtolower($requestData['email']));
            $lead->lead_contact = $generalControllerObject->encrypt_string($leadContact);
            $lead->lead_zipcode = !empty($requestData['address_zipcode']) ? $requestData['address_zipcode'] : null;
            $lead->lead_location = $requestData['lead_location'];
            $lead->lead_case_type = ucwords($accTyp);
            $lead->lead_case_history = $generalControllerObject->encrypt_string(htmlentities($leadCaseHistory));
            $lead->lead_status = config('constant.ATTORNEY_PRESENT_LEAD_STATUS'); // create as a new lead
            $lead->reference_id = null;
            $lead->lead_spam_score = 1;
            $lead->reference_type = null;
            $lead->lead_uuid = $lead_uuid;
            $lead->accepted_tos = 1;
            $lead->lead_ip_address = \Request::ip();
            $lead->test_mode = $requestData['test_mode'] == 'true' ? TRUE : FALSE;
            $createLeadResult = $lead->save();
           
            if ($createLeadResult == true) {
                $intercom_lead = $this->create_intercom_lead($new_lead_data);
                Log::info('Intercom Lead As User Created =' . $intercom_lead['lead_intercom_id']);
                
                $leadId = $lead->id;

                // create lead location entry
                $leadLocation = new leadLocation;
                $leadLocation->lead_id = $leadId;
                $leadLocation->lead_zip = !empty($requestData['address_zipcode']) ? $requestData['address_zipcode'] : null;
                $leadLocation->lead_city = !empty($requestData['addresss_city']) ? $requestData['addresss_city'] : null;
                $leadLocation->lead_county = !empty($requestData['address_county']) ? $requestData['address_county'] : null;
                $leadLocation->lead_state = !empty($requestData['addresss_state']) ? $requestData['addresss_state'] : null;

                $createLeadLocationResult = $leadLocation->save();
                if ($createLeadLocationResult) {
                    $leadLocationId =  $leadLocation->id;
                    $hashids = new Hashids(config('constant.SALT_KEY'), config('constant.MIN_HASH_LENGTH'));
                    $attorneyShortUrl = $adminShortUrl = null;
                    $attorneyLeadHandler = applicationPreferences::where(['type' => 'LEAD_DETAIL_HANDLER'])->first();
                    if (!empty($attorneyLeadHandler)) {
                        $preferenceData = $attorneyLeadHandler->toArray();
                        $attorneyDetailUrl = $preferenceData['data'] . $hashids->encode($leadId);
                        $attorneyShortUrl = $attorneyDetailUrl;
                        try {
                            $attorneyShortUrl = Shorty::shorten($attorneyDetailUrl);
                        } catch (\Exception $e) {
                            Log::info('exception during attorney link short =' . $e);
                        }
                    }
                   
                    $adminLeadHandler = applicationPreferences::where(['type' => 'ADMIN_LEAD_DETAIL_HANDLER'])->first();
                    if (!empty($adminLeadHandler)) {
                        $preferenceData = $adminLeadHandler->toArray();
                        $adminDetailUrl = $preferenceData['data'] . $hashids->encode($leadId);
                        $adminShortUrl = $adminDetailUrl;
                        try {
                            $adminShortUrl = Shorty::shorten($adminDetailUrl);
                        } catch (\Exception $e) {
                            Log::info('exception during admin link short =' . $e);
                        }
                    }
                    $leadShortenUrls = new \App\model\lead\leadShortenUrls();
                    $leadShortenUrls->lead_id = $leadId;
                    $leadShortenUrls->admin_detail_url = $adminShortUrl;
                    $leadShortenUrls->attorney_detail_url = $attorneyShortUrl;
                    $createLeadShortUrlsResult = $leadShortenUrls->save();
                    
                }
                
                //send email
                $lead_email_data = array(
                    'userName' => $requestData['first_name'] . ' ' . $requestData['last_name'],
                    'userEmail' => strtolower($requestData['email']),
                    'userPhone' => $leadContact,
                    'location' => $requestData['lead_location'],
                    'case_history' => $leadCaseHistory,
                    'caseType' => $requestData['accident_type'],
                    'lead_id' => $leadId,
                );
                
                $send_email = $this->send_email_to_team($lead_email_data, $requestData, $testMode);
                
                //Save Raw Lead Raw Data
                $raw = $this->store_raw_data($leadId, $leadLocationId, $requestData, $requestData['questions']);
                //Ping 4LL and SAVE RESPONSE
                
                if($ping_data != null){
                    /* $fourll_ping_response = $this->ping_4ll($ping_data); */
                    $fourll_ping_response = array();
                    /* if(!empty($fourll_ping_response['status'])){ */
                        $ping_request = $ping_data;
                        $ping_data['Mode'] = 'post';
                        $ping_data['Lead_ID'] = ''/* $fourll_ping_response['lead_id'] */;
                        $ping_data['First_Name'] = $requestData['first_name'];
                        $ping_data['Last_Name'] = $requestData['last_name'];
                        $ping_data['Phone'] = str_replace(array( '(', ')','-',' ' ), '', $requestData['phone']);
                        $ping_data['IP_Address'] = \Request::ip();
                        $ping_data['Comments'] = $requestData['description'];
                        $ping_data['Landing_Page'] = 'Accident.com';
                        
                        $this->save_fourll_post_data($leadId, $leadLocationId, $fourll_ping_response, $ping_data, $ping_request);
                        
                    //}
                }

                $this->add_lead_note($intercom_lead['lead_intercom_id'], $leadCaseHistory);

                $leadUpdateResult = lead::where(['lead_email' => $generalControllerObject->encrypt_string(strtolower($requestData['email'])), 'lead_uuid' => $lead_uuid])
                    ->update(
                        array(
                            'intercom_lead_id' => $intercom_lead['lead_intercom_id'],
                            'intercom_type' => $intercom_lead['intercom_type'],
                        )
                    );

                $email_lead_data = array();
                $emailLinks = array();
                /* $hashids             = new Hashids(config('constant.SALT_KEY'), config('constant.MIN_HASH_LENGTH')); */
                // Generate form feedback email links - emoji links
                foreach (config('constant.EMAIL_SENT_TYPE_VALUE') as $typeValueKey => $typeValueId) {

                    // Create entry in email_sent table
                    $emailSent = new emailSent;
                    $emailSent->reference_id = $leadId;
                    $emailSent->email_to = config('constant.EMAIL_SENT_EMAIL_TO_NEW_LEAD_FEEDBACK');
                    $emailSent->type = config('constant.EMAIL_SENT_TYPE_NOTIFICATION');
                    $emailSent->type_value = $typeValueId;
                    $emailSent->status = config('constant.EMAIL_SENT_STATUS_ACTIVE');
                    $emailSentResult = $emailSent->save();
                    //$emailLinks["link_".$typeValueId]     = config('constant.FEEDBACK_SITE_URL').'/feedback/'.$hashids->encode($emailSent->id);
                    $emailLinks["link_" . $typeValueId] = config('constant.FEEDBACK_SITE_URL') . '?r=' . $typeValueKey . '&t=' . $requestData['accident_type'];
                }
                $email_lead_data['lead_name'] = $requestData['first_name'] . ' ' . $requestData['last_name'];
                $email_lead_data['email'] = strtolower($requestData['email']);
                $email_lead_data['feedback'] = $emailLinks;

                $email_lead_data['unsubscribe_link'] = config('constant.UNSUBSCRIBE_LINK') . "?x=" . base64_encode(strtolower($requestData['email']));
                $email_lead_data['termsofservice_link'] = config('constant.TERMS_OF_SERVICE_LINK');

                $template_id = 'd-c12c6c265c6c4f7bab522866ce211bab';
                $subject_thnk = "Thank you for choosing Accident.com";
                //$this->sendgrid_transactional_email(strtolower($requestData['email']), $template_id, $subject_thnk, $email_lead_data);
                
                //send sms to Jack
                $this->sendNewLeadInfoSMS($requestData, $leadContact, $testMode);

                DB::commit();


                $apiResponse = $this->checkAndUpdateIfLeadHasMatchingAttorney('00000', $requestData['accident_type'], $hashids->encode($leadId));
                $resContent	 = json_decode($apiResponse->getBody()->getContents(), TRUE);
                if (!empty($resContent)) {
                    Log::info('Status updated for LEAD : ' . $leadId);
                } else {
                    Log::info('Empty data. No data to process.');
                }

                $t_uri = "/thank-you/".strtolower(preg_replace('/\s+/', '', $requestData['accident_type']));
                $auto_injury_array = array(
                    'Car Accident',
                    'Motorcycle Accident',
                    'Truck Accident',
                    'Work-Related Accident',
                    'Pedestrian Accident',
                    'Bicycle Accident'
                );
                if(in_array($accTyp, $auto_injury_array)){
                    $t_uri = "/thank-you/auto/".strtolower(preg_replace('/\s+/', '', $accTyp));
                } 
        
                $redirect_thankyou = url($t_uri);

                 
                if ($testMode == 'true') {
                    $redirect_thankyou   = !empty($requestData['redirect_to'])?$requestData['redirect_to']:'https://accident.com/';
                }
                
                echo json_encode(array(
                    'status' => 'success',
                    'redirect_to' => $redirect_thankyou,
                ));
            }

        } catch (\Exception $e) {

            DB::rollback();
            echo json_encode(array(
                'status' => 'error',
                'message' => $e,
            ));
        }
    }

    public function thank_you(Request $request)
    {
        $redirect_url = '/';
        $accident_type = '';
        if(request()->has('redirect_url') && !empty(request()->get('redirect_url'))){
            $redirect_url = request()->get('redirect_url');
        }

        if (request()->has('accident_type')) {
            $accident_type = request()->get('accident_type');
        }
        return view('home/thankyou_pages/thankyou', array('accident_type' => $accident_type, 'redirect_url' => $redirect_url));
    }

    public function thank_you_general(Request $request)
    {
        $redirect_url = '/';
        $accident_type = '';
        /* if(request()->has('redirect_url') && !empty(request()->get('redirect_url'))){
            $redirect_url = request()->get('redirect_url');
        } */

        if (request()->has('accident_type')) {
            $accident_type = request()->get('accident_type');
        }
        return view('home/thankyou_pages/thankyou_v2', array('accident_type' => $accident_type, 'redirect_url' => $redirect_url));
    }

    public function thank_you_auto(Request $request)
    {
        $redirect_url = '/';
        $accident_type = '';
        /* if(request()->has('redirect_url') && !empty(request()->get('redirect_url'))){
            $redirect_url = request()->get('redirect_url');
        } */

        if (request()->has('accident_type')) {
            $accident_type = request()->get('accident_type');
        }
        return view('home/thankyou_pages/thankyou_auto', array('accident_type' => $accident_type, 'redirect_url' => $redirect_url));
    }


    public function buildCaseHistory($questions, $request = array())
    {
        $ans = '';
      
        $counter = 1;
        $caseHistory = '';
        $injury = $request['data-injury'];
        try{
            foreach ($questions as $key => $question) {
                if(is_array($question)){
                    if(is_array($request[$key][$injury])){
                        $ans = join(",",$request[$key][$injury]);
                    }else{
                        $ans = $request[$key][$injury];
                    }
                    $caseHistory .= "Q" . $counter . ". " . $question[$injury] . "<br/>";
                    $caseHistory .= "A: " . $ans . "<br/>";
                    $caseHistory .= '<br/>';
                }else{
                    $caseHistory .= "Q" . $counter . ". " . $question . "<br/>";
                    $caseHistory .= "A: " . $request[$key] . "<br/>";
                    $caseHistory .= '<br/>';
                }
                $counter++;
            }

            return $caseHistory;

        }catch(\Exception $e ){
            Log::info(print_r($e));
        }
    }

    //Build Custom Attribute array for Intercom
    public function build_custom_attribute($requestData, $custom_att)
    {
        $custom_attributes = array(
            'Location' => $requestData['lead_location'],
            'Case Type' => $requestData['accident_type'],
        );
        foreach ($custom_att as $key => $value) {
            $custom_attributes[$custom_att[$key]] = $requestData[$key];
        }
        return $custom_attributes;
    }

    /*
    Add Note For Intercom Lead
     */
    public function add_lead_note($lead_intercom_id, $note_content)
    {
        $add_note = Intercom::notes()->create([
            "admin_id" => env('INTERCOM_SUPPORT'),
            "body" => $note_content,
            "user" => [
                "id" => $lead_intercom_id,
            ],
        ]);
    }

    public function create_intercom_lead($new_lead_data)
    {
        $this->accidentApp = new \AccidentApp();
        $generalControllerObject = new generalController();
        
        $check_lead_data = lead::where([
            'lead_email' => $generalControllerObject->encrypt_string(strtolower($new_lead_data['email'])),
            'intercom_type' => 'user/lead',
        ])->first();
        if (!isset($check_lead_data)) {
            $new_lead = Intercom::users()->create($new_lead_data);
            Intercom::tags()->tag([
                "name" => "LEAD",
                "users" => [
                    ["id" => $new_lead->id],
                ],
            ]);
            $intercom_type = 'user/lead';
        } else {
            try{
                $new_lead = Intercom::leads()->create($new_lead_data);
                $intercom_type = 'lead';
            }catch(\Exception $e){
                Log::info(print_r($e));
            }
        }

        return array('lead_intercom_id' => $new_lead->id, 'intercom_type' => $intercom_type);
    }

    public function send_email_to_team($lead_email_data, $requestData, $testMode)
    {
        $this->accidentApp = new \AccidentApp();
        // check and remove if email has beed added to unsubscribe list
        $email = $lead_email_data['userEmail'];
        $caseType = $lead_email_data['caseType'];

        $sendgrid = new \SendGrid(config('sendgridapi.api_key'));
        $getsendgridRecord = $this->getSendgridRecord($email);
        if (!empty($getsendgridRecord->body())) {
            $unsubscribe = $this->sendgridSubscribe($email);
        }

        $emailContent = view('email/mailTeam', $lead_email_data)->render();

        $subject = '';
        $subject .= "New " . $caseType . " Lead in " . $lead_email_data['location'];

        
      

        try {
            $email = new \SendGrid\Mail\Mail();

            $email->setFrom("lead-notification@accident.com", "Accident.com");
            $email->setSubject($subject);
            if ($testMode == 'true') {
                $email->addTo(config('constant.DEVELOPERS_EMAIL'), "Developers");
                $email->addCc(config('constant.TEST_INTERCOM_INBOUND_EMAIL'));
            } else {
                $email->addTo(config('constant.TEAM_EMAIL'), "Team");
                $email->addCc(config('constant.LIVE_INTERCOM_INBOUND_EMAIL'));
                $email->addBcc(config('constant.CUSTOMER_SERVICE_EMAIL'));
            }
            $email->addContent(
                "text/html", $emailContent
            );
            $sendgrid = new \SendGrid($this->accidentApp->getSendGridToken());
            $response = $sendgrid->send($email);
        } catch (\Exception $e) {

        }
    }

    /**
     * Get sendGrid record
     */
    public function getSendgridRecord($email)
    {
        $sendgrid = new \SendGrid(config('sendgridapi.api_key'));
        $response = $sendgrid->client->asm()->suppressions()->global()->_($email)->get();
        return $response;
    }

    /**
     * Subscribe email
     */
    public function sendgridSubscribe($email)
    {
        $sendgrid = new \SendGrid(config('sendgridapi.api_key'));
        $response = $sendgrid->client->asm()->suppressions()->global()->_($email)->delete();
        return $response;
    }

    public function sendgrid_transactional_email($email_id, $template_id, $subject, $email_lead_data)
    {

        $from = new \SendGrid\Mail\From("support@accident", "Support Accident");
        $tos = [
            new \SendGrid\Mail\To(
                $email_id,
                $email_lead_data['lead_name'],
                $email_lead_data
            ),
        ];
        $email = new \SendGrid\Mail\Mail(
            $from,
            $tos
        );
        $email->setSubject($subject);
        $email->setTemplateId($template_id);
        $sendgrid = new \SendGrid(config('sendgridapi.api_key'));
        try {
            $response = $sendgrid->send($email);
        } catch (Exception $e) {
            print $e . "\n";
        }
    }

    public function sendNewLeadInfoSMS($requestData, $leadPhone, $testMode)
    {
        
        $from = "+18558218344";
        if ($testMode == 'true') {
			$contacts = config('constant.TEST_CONTACTS');
            //$to   = "+919923260875";
        } else {
			$contacts = config('constant.CONTACTS_TO_SMS');
            //$to   = "+16467011444";
        }
        
        $message = "Hello,\n";
        $message .= "There is a new " . $requestData['accident_type'] . " lead" .
            ((!empty($requestData['addresss_state'])) ? " in " . $requestData['addresss_state'] : ".") . ". \n";
        $message .= "Name: " . $requestData['first_name'] . "\n" .
            "Email: " . $requestData['first_name'] . "\n" .
            "Phone: " . $leadPhone . "\n";
        //"Please click to view more lead details " . $adminShortUrl . "\n";
        $message .= "Reply STOP to unsubscribe from such notifications - Accident.com";

        //$status = $this->sendSms($from, $to, $message); //send SMS to admin about new lead arrival
		$status = '';
		foreach ($contacts as $contact) {
			$to = $contact;
			$status = $this->sendSms($from, $to, $message); //send SMS to admin about new lead arrival
			sleep(5);
		}
		Log::info($status);
    }

    /**
     * Send twilio SMS
     *
     * @param string $from
     * @param string $to
     * @param string $message
     * @return string
     */
    public function sendSms($from, $to, $message)
    {
        // START : Send SMS to Attorney
        if ($this->twilioRestClientMaster == null) {
            $client = $this->loadMasterAccountClient();

        } else {
            $client = $this->twilioRestClientMaster;
        }
        try {
            $client->messages->create(
                $to,
                array(
                    'from' => $from,
                    'body' => $message,
                )
            );
            return "success";
        } catch (Exception $e) {
        }
    }

    /**
     * Instantiate Twilio client
     */
    private function loadMasterAccountClient()
    {
        $row = DB::table('application_api')->where('environment', config('api_environment'))->first();    
        try {
            $this->twilioRestClientMaster = new Client($row->twilioMasterAccountSid, $row->twilioMasterAccountSecretToken);
        } catch (Twilio\Exceptions\ConfigurationException $e) {
            echo "exception";
        }
        return $this->twilioRestClientMaster;
    }

    public function store_raw_data($leadId, $leadLocationId, $requestData, $questions_data)
    {
        try{
            $injury = $requestData['data-injury'];
            $rawData = array();
            $rawData['firstName'] = $requestData['first_name'];
            $rawData['lastName'] = $requestData['last_name'];
            $rawData['phone'] = $requestData['phone'];
            $rawData['email'] = $requestData['email'];
            $rawData['zipCode'] = $requestData['address_zipcode'];
            $rawData['city'] = $requestData['addresss_city'];
            $rawData['county'] = $requestData['address_county'];
            $rawData['state'] = $requestData['addresss_state'];
            $rawData['caseType'] = $requestData['accident_type'];


            foreach ($questions_data as $key => $question) {
                if(is_array($question)){
                    $rawData['caseHistory'][$question[$injury]] = $requestData[$key][$injury];
                }else{
                    $rawData['caseHistory'][$question] = $requestData[$key];
                }
            }
            

            if(isset($rawData['caseHistory']['Help us better understand your situation with more details HERE'])){
				$caseDesc = $rawData['caseHistory']['Help us better understand your situation with more details HERE'];
				if($caseDesc == NULL || strlen($caseDesc) <= 255){
					$rawData['intelligence'] = array('case_description' => array(
													'message' => 'case description is less than 255 characters'
												)
											);	
				}
            }

            if(isset($rawData['caseHistory']['Do you currently have a lawyer representing your claim?'])){
				$gotAtt = $rawData['caseHistory']['Do you currently have a lawyer representing your claim?'];
				if($gotAtt == 'Yes'){
					$rawData['intelligence'] += array('got_attorney' => array(
													'message' => 'this case has a lawyer representing the claim'
												)
											);	
				}
            }
                    
            $rawDataJson = json_encode($rawData);

            $leadRawData = new leadRawData;
            $leadRawData->lead_id = $leadId;
            $leadRawData->lead_location_id = $leadLocationId;
            $leadRawData->raw_data = $rawDataJson;
            $leadRawData->save();
        }catch(\Exception $e){
            return $e;
        }
    }

    public function ping_4ll($ping_data){
        $query = http_build_query($ping_data);
        $url = 'https://leads.4legalleads.com/new_api/api.php?'.$query;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $data = curl_exec($ch);
        curl_close($ch);

        $xml = simplexml_load_string($data);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);
        return $array;
    }

    public function save_fourll_post_data($leadId, $leadLocationId, $fourll_ping_response, $ping_data, $ping_request_json){
        /*unset($ping_data['Test_Lead']);*/
        try{
            $leadFourLL = new leadFourLL;
            $leadFourLL->lead_id = $leadId;
            $leadFourLL->lead_location_id = $leadLocationId;
            $leadFourLL->fourll_leadid = (isset($fourll_ping_response['lead_id']))? $fourll_ping_response['lead_id']: NULL;/* 
            $leadFourLL->fourll_ping_response = (isset($fourll_ping_response['lead_id']))?json_encode($fourll_ping_response) : NULL; */
            $leadFourLL->fourll_post_json = json_encode($ping_data);
            $leadFourLL->fourll_ping_request_json = json_encode($ping_request_json);
          /*   $leadFourLL->fourll_ping_status = (isset($fourll_ping_response))?$fourll_ping_response['status']:NULL;
            $leadFourLL->fourll_lead_price  = (isset($fourll_ping_response))?$fourll_ping_response['price']:NULL; */
            return $leadFourLL->save();
        }catch(\Exception $e){
            return $e;
        }
    }

    public function checkAndUpdateIfLeadHasMatchingAttorney($zipcode, $caseType, $leadId)
	{
		try {
			/* make curl call */
			$client = new GuzzleClient();
			$apiResponse = $client->request('POST', config('accident_admin.api_url'), [
								'headers' => [
									'X-API-KEY' => config('accident_admin.api_key'),
									'Accept'     => 'application/json'
								],
								'auth' => ['admin', 'zAEfe$Scyf'],
								'json' => [ "data" =>
									[
										['zipcode' => $zipcode, 'caseType'=> $caseType, 'leadId'=> $leadId]
									]
								]
							]);
			return $apiResponse;
		} catch (Exception $ex) {
			Log::info('Call to accident admin API FAILED: '.$ex->getMessage());
		}

	}
    
}