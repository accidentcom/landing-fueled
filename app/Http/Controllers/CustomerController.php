<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CustomerController extends Controller
{
    /**
	 * Unsubscribe to email
	 */
	public function unsubscribe()
	{
		$successMessage = "Unsubscribed to email successfully";
		$errorMessage = "We're sorry. Email unsubscription failed";
		$result = array("status" => "error", "message" => $errorMessage);
		if (request()->has('x')) {
			$param = request()->get('x');
			$email = base64_decode($param);
			try {
				$request_body = json_decode('{
					"recipient_emails": [
						"' .$email. '"
					]
				}');
				$sendgrid = new \SendGrid(config('sendgridapi.api_key'));
				$response = $sendgrid->client->asm()->suppressions()->global()->post($request_body);
				if (!$response) {
					$result = array("status" => "error", "message" => $errorMessage);
					Log::info('UNSUBSCRIBE : Email subscribtion failed for : ' . $email);
					return view('customer.unsubscribe', $result);
				}
				if ($response->statuscode() == 201) {
					Log::info('UNSUBSCRIBE : Email subscribtion successfull for : ' . $email);
					$result = array("status" => "success", "message" => $successMessage);
				} else {
					Log::info('UNSUBSCRIBE : Email subscribtion failed for : ' . $email);
					$result = array("status" => "error", "message" => $errorMessage);
				}
				return view('customer.unsubscribe', $result);
			} catch (Exception $ex) {
				Log::info('UNSUBSCRIBE : Email unsubscribtion failed for : ' . $email);
				$result = array("status" => "error", "message" => $errorMessage);
				return view('customer.unsubscribe', $result);
			}
		}
		return view('customer.unsubscribe', $result);
	}

	/**
	 * Subscribe to email
	 */
	public function subscribe()
	{
		$successMessage = "Subscribed to emails successfully";
		$errorMessage = "We're sorry. Email subscription failed";
		$result = array("status" => "error", "message" => $errorMessage);
		if (request()->has('x')) {
			$param = request()->get('x');
			$email = base64_decode($param);
			try {
				$sendgrid = new \SendGrid(config('sendgridapi.api_key'));
				$response = $sendgrid->client->asm()->suppressions()->global()->_($email)->delete();
				if (!$response) {
					$result = array("status" => "error", "message" => $errorMessage);
					return view('customer.subscribe', $result);
				}
				if ($response->statuscode() == 204) {
					$result = array("status" => "success", "message" => $successMessage);
				} else {
					$result = array("status" => "error", "message" => $errorMessage);
				}
				Log::info('SUBSCRIBE : Email subscribtion successfull for ' . $email);
				return view('customer.subscribe', $result);
			} catch (Exception $ex) {
				Log::info('SUBSCRIBE : Email subscribtion failed for ' . $email);
				return view('customer.subscribe', $result);
			}
		}
		return view('customer.subscribe', $result);
	}
}
