<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
class FreeConsultationController extends Controller
{
    /**
	 * Send data to type form
	 */
	public function index(Request $request){
			return redirect('/');
	}
	public function store(Request $request)
	{
		$caseType = $request->get('case_type');
		$testMode = $request->get('test_mode');
		$baseUri = "https://accident.typeform.com/to/";
		// change below id's with respect to type form ids
		$caseTypes = array(
			'Auto Accident' => 'z5ou6n',
			'Medical Malpractice' => 'lZlRre',
			'Product Liability' => 'mXa00y',
			'Workers Compensation' => 'aQIj70',
			'Personal Injury' => 'gfHMGn',
			'Wrongful Death' => 'r3e0jX',
			'Slip And Fall'=>'TxQp4o',
			
			//---Auto-Injury---TypeForm---ID'S
			'Car Accident' => 'xAEh52',
			'Motorcycle Accident' => 'J1j99R',
			'Truck Accident' => 'vLURHO',
			'Work-Related Accident' => 'UwQRdl',
			'Pedestrian Accident'=> 'uswlVz',
			'Bicycle Accident' => 'JgySVs'
		);
		$ipAddress = \Request::ip();
		$t_uri = "/thank-you/".strtolower(preg_replace('/\s+/', '', $caseType));
		$gtm_script = view('gtm_views.gtm_script');
		$gtm_noscript = view('gtm_views.gtm_noscript');
		$auto_injury_array = array(
			'Car Accident',
			'Motorcycle Accident',
			'Truck Accident',
			'Work-Related Accident',
			'Pedestrian Accident',
			'Bicycle Accident'
		);
		if(in_array($caseType, $auto_injury_array)){
			$t_uri = "/thank-you/auto/".strtolower(preg_replace('/\s+/', '', $caseType));
			$gtm_script = view('gtm_views.gtm_auto_script');
			$gtm_noscript = view('gtm_views.gtm_auto_noscript');
		}
		


		$redirect_thankyou = url($t_uri);
		
		$uri = $baseUri . $caseTypes[$caseType] . "?zipcode=".$request->get('address_zipcode')."&city=".$request->get('addresss_city')."&state=".$request->get('addresss_state')."&type=". $caseType ."&address=".$request->get('addresss_formated')."&county=".$request->get('address_county')."&redirect_url=".$redirect_thankyou."&ipaddress=".$ipAddress."&test_mode=".$testMode;		
		return view('home.freeConsultation',  compact('uri', 'caseType','gtm_script','gtm_noscript'));
	}

	public function loadv2Form(Request $request){
		$requestData = $request->all();
		
		$template_data = array(
			'redirect_to' => $requestData['redirect_to'],
			'test_mode' => $requestData['test_mode'],
			'address_ele' => $requestData['address_ele'],
			'location' => $requestData['location'],
			'accType' => $requestData['accidentType']
		);
		//dd($template_data);
		$test_mode = FALSE;
		if (request()->has('test_mode')) {
			$test_mode = request()->get('test_mode');
		}

		if (request()->has('accident_type')) {
            $accident_type = request()->get('accident_type');
        } else {
			if(isset($requestData['accidentType'])){
				$accident_type = $requestData['accidentType'];
			}else{
				$accident_type = '';
			}
        }
		$steps = view('home.questions.all_questions');
		return view('home.questions.v2_questions', array('steps' => $steps, 'accident_type'=>$accident_type, 'test_mode' => $test_mode,'template_data' => $template_data));
	}

	public function loadAutoForm(Request $request){
		$requestData = $request->all();
		
		$template_data = array(
			'redirect_to' => $requestData['redirect_to'],
			'test_mode' => $requestData['test_mode'],
			'address_ele' => $requestData['address_ele'],
			'location' => $requestData['location'],
			'accType' => $requestData['accidentType']
		);
		//dd($template_data);
		$test_mode = FALSE;
		if (request()->has('test_mode')) {
			$test_mode = request()->get('test_mode');
		}

		if (request()->has('accident_type')) {
            $accident_type = request()->get('accident_type');
        } else {
			if(isset($requestData['accidentType'])){
				$accident_type = $requestData['accidentType'];
			}else{
				$accident_type = '';
			}
        }
		$steps = view('home.questions.all_auto_questions');
		return view('home.questions.auto_w_questions', array('steps' => $steps, 'accident_type'=>$accident_type, 'test_mode' => $test_mode,'template_data' => $template_data));
	}
}
