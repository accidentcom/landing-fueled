<?php

namespace App\model\lead;

use Illuminate\Database\Eloquent\Model;
use DB;

class leadRawData extends Model
{
    protected $table 	= 'lead_raw_data';
	public $timestamps 	= false;
}