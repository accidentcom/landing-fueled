<?php

namespace App\model\lead;

use Illuminate\Database\Eloquent\Model;
use DB;

class leadShortenUrls extends Model
{
    protected $table 	= 'lead_shorten_urls';
	public $timestamps 	= false;
}