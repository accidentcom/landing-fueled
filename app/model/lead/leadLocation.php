<?php

namespace App\model\lead;

use Illuminate\Database\Eloquent\Model;
use DB;

class leadLocation extends Model
{
    protected $table 	= 'lead_location';
	public $timestamps 	= false;
}