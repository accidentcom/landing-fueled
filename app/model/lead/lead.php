<?php

namespace App\model\lead;

use Illuminate\Database\Eloquent\Model;
use DB;

class lead extends Model
{
    protected $table 	= 'leads';
	public $timestamps 	= false;
}