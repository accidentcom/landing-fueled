<?php

namespace App\model\general;

use Illuminate\Database\Eloquent\Model;
use DB;

class applicationPreferences extends Model
{
    protected $table 	= 'application_preferences';
	public $timestamps 	= false;
}