<?php

namespace App\model\email;

use Illuminate\Database\Eloquent\Model;
use DB;

class emailSent extends Model
{
    protected $table 	= 'email_sent';
	public $timestamps 	= false;
}