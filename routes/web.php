<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//AUTO SUB-DOMAINS
Route::group(array('domain' => 'demoautostg.accident.com'), function(){

    //Auto TypeForm(Sparker)
    Route::get('/', array(
        'uses' => 'HomeController@auto_sparker'
    ));


    //Auto Wizard(Wilio)
    Route::get('/main', array(
        'uses' => 'HomeController@version_auto_injury'
    ));
    

    //Auto Wizard(Sparker)
    Route::get('/index', array(
        'uses' => 'HomeController@auto_wizard'
    ));

     //Auto TypeForm
     Route::get('/app', array(
        'uses' => 'HomeController@main_auto_wizard'
    ));

});

Route::group(array('domain' => 'auto.accident.com'), function(){

    //Auto TypeForm(Sparker)
    Route::get('/', array(
        'uses' => 'HomeController@auto_sparker'
    ));


    //Auto Wizard(Wilio)
    Route::get('/main', array(
        'uses' => 'HomeController@version_auto_injury'
    ));
    

    //Auto Wizard(Sparker)
    Route::get('/index', array(
        'uses' => 'HomeController@auto_wizard'
    ));

     //Auto TypeForm 
     Route::get('/app', array(
        'uses' => 'HomeController@main_auto_wizard'
    ));

});

//--------------------Injury SUB-DOMAINS

Route::group(array('domain' => 'demostagging.accident.com'), function(){

    //Injury TypeForm(Sparker)
    Route::get('/', array(
        'uses' => 'HomeController@Version2'
    ));

    //Injury Wizard(Wilio)
    Route::get('/main', array(
    'uses' => 'HomeController@version_typeform'
    ));

    //Injury Wizard(Sparker)
    Route::get('/index', array(
        'uses' => 'HomeController@v2_form'
    ));

    //Injury TypeForm
     Route::get('/app', array(
        'uses' => 'HomeController@index'
    ));

});

Route::group(array('domain' => 'injury.accident.com'), function(){

    //Injury TypeForm(Sparker)
    Route::get('/', array(
        'uses' => 'HomeController@Version2'
    ));

    //Injury Wizard(Wilio)
    Route::get('/main', array(
    'uses' => 'HomeController@version_typeform'
    ));

    //Injury Wizard(Sparker)
    Route::get('/index', array(
        'uses' => 'HomeController@v2_form'
    ));

    //Injury TypeForm
     Route::get('/app', array(
        'uses' => 'HomeController@index'
    ));

});

//---------------------


Route::group(array('domain' => 'demoinj.accident.com'), function(){

    //Injury TypeForm(Sparker)
    Route::get('/', array(
        'uses' => 'HomeController@Version2'
    ));

    //Injury Wizard(Wilio)
    Route::get('/main', array(
    'uses' => 'HomeController@version_typeform'
    ));

    //Injury Wizard(Sparker)
    Route::get('/index', array(
        'uses' => 'HomeController@v2_form'
    ));

    //Injury TypeForm
     Route::get('/app', array(
        'uses' => 'HomeController@index'
    ));

});

Route::group(array('domain' => 'demoauto.accident.com'), function(){

    //Auto TypeForm(Sparker)
    Route::get('/', array(
        'uses' => 'HomeController@auto_sparker'
    ));


    //Auto Wizard(Wilio)
    Route::get('/main', array(
        'uses' => 'HomeController@version_auto_injury'
    ));
    

    //Auto Wizard(Sparker)
    Route::get('/index', array(
        'uses' => 'HomeController@auto_wizard'
    ));

     //Auto TypeForm 
     Route::get('/app', array(
        'uses' => 'HomeController@main_auto_wizard'
    ));

});

Route::group(array('domain' => 'customer.accident.com'), function(){
    Route::get('/unsubscribe', array(
        'uses' => 'CustomerController@unsubscribe'
    ));
    Route::get('/subscribe', array(
        'uses' => 'CustomerController@subscribe'
    ));
});

//Route::get('/unsubscribe', 'CustomerController@unsubscribe');
//Route::get('/subscribe', 'CustomerController@subscribe');
/* Route::get('/v_typeform', 'HomeController@Index');
Route::get('/main_auto_wiz', 'HomeController@main_auto_wizard');
Route::get('/auto_sparker', 'HomeController@auto_sparker');
//Route::get('/v2', 'HomeController@Version2');
Route::get('/v2_form', 'HomeController@v2_form');
Route::get('/', 'HomeController@version_typeform');
Route::get('/auto-injury', 'HomeController@version_auto_injury');
Route::get('/auto-injury-wizard', 'HomeController@auto_wizard'); */


Route::resource('freeConsultation', 'FreeConsultationController');
Route::post('/start-evaluation', 'FreeConsultationController@loadv2Form');
Route::get('/start-evaluation', function () {
    return redirect('/');
});

Route::post('/start-evaluation-auto', 'FreeConsultationController@loadAutoForm');
Route::get('/start-evaluation-auto', function () {
    return redirect('/');
});

Route::post('/save-lead', 'control\lead\LeadController@save_lead');
Route::get('/terms', 'HomeController@terms'); // terms of service


//Thankyou Routes
//Route::get('/thankyou', 'control\lead\LeadController@thank_you');
Route::get('/thank-you/{caseType}', 'control\lead\LeadController@thank_you_general');
Route::get('/thank-you/auto/{caseType}', 'control\lead\LeadController@thank_you_auto');

