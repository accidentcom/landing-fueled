<!DOCTYPE html>

<html lang="en" class="js no-touchevents no-browser-firefox browser-chrome gr__accidentCom-landing_com" style="">
    <head>
        <title>Lorem Ipsum is simply dummy text | Accident.com  @yield('title')</title>

		<!-- metadata -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="robots" content="noindex, nofollow">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-T343B9G');</script>
		<!-- End Google Tag Manager -->

		<!-- css -->
		@include('common.stylesheet')
		<!-- javascripts -->
		@include('common.scripts')

    </head>
    <body class="accident-domain-accidentCom-landing accident-theme-blue domain-id-accidentCom-landing path-type-accident media-range-wide" data-theme="blue" data-toggle="accidentResponsiveHelper" >
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T343B9G"
						  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

		<div id="page-container" class="region grid-region-page-container container">
            <div id="page" class="region grid-region-page">
				<div id="page-hd" class="clearfix">
                    <div class="region-header clearfix">
						<div class="header-left">
							<div class="navbar-brand">
								<span class="icon-bar brand-color-1"></span>
								<span class="icon-bar brand-color-2"></span>
								<span class="icon-bar brand-color-3"></span>
								<span class="icon-bar brand-color-4"></span>
							</div>
							<div class="accident-logo accident-logo-accidentCom-landing">
								<a href="{{ url('/') }}"><div class="logo-image"></div></a>
								<div class="logo-text">
									<a href="{{ url('/') }}"><span class="logo-step-1">Accident</span><span class="logo-domain-extension">.com</span></a>
								</div>
							</div>
						</div>
						<div class="header-right">
							<div class="accident-business-hours">
								<p>Mon-Fri: 9AM–5PM PST</p>
							</div>
						</div>
					</div>
                </div>
				<div id="page-bd" class="clearfix">
                    <div id="page-content" class="column-1-layout clearfix">

						<div class="region-content-top">
							<div id="content-top" class="clearfix">


								

								@yield('content')

								

								<div class="accident-aside-column column last">
									<div class="panel panel-info box accident-aside accident-about-info">
										<div class="panel-heading box-hd">
											<h3 class="panel-title"><span>About Us</span></h3>
										</div>
										<div class="panel-body box-bd">
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
										</div>
									</div>
									<div class="panel panel-info box accident-aside accident-media-info">
										<div class="panel-heading box-hd">
											<h3 class="panel-title"><span>Seen In: </span></h3>
										</div>
										<div class="panel-body box-bd">
											<ul class="accident-media-list clearfix">
												<li class="media-1 first"><div class="accident-partner-usa-today"><span>Lorem Ipsum</span></div></li>
												<li class="media-2"><div class="accident-partner-kiplinger"><span>Lorem Ipsum</span></div></li>
												<li class="media-3"><div class="accident-partner-washington-post"><span>Lorem Ipsum</span></div></li>
												<li class="media-4 last"><div class="accident-partner-newsweek"><span>Lorem Ipsum</span></div></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="region-content">
							<div id="content-ct" class="column region grid-region-content-bottom clearfix">
								<div class="panel panel-default accident-collapse">
									<div class="panel-heading" role="tab" id="accident-collapse-hd">
										<h3 class="panel-title">
											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="https://www.accidentattorneys.com/#accident-collapse-bd" aria-expanded="true" aria-controls="accident-collapse-bd" style="display: block;">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry
											</a>
										</h3>
									</div>
									<div id="accident-collapse-bd" class="panel-collapse collapse" role="tabpanel" aria-labelledby="accident-collapse-hd">
										<div class="panel-body">
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
											<h4>Lorem Ipsum</h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
											<h4>Lorem Ipsum</h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
											<h4>Lorem Ipsum</h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
											<p><a href="https://www.accident.com">Lorem Ipsum is simply dummy text</a> Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						
					</div>
				</div>

				<div id="page-ft" class="clearfix">
					<div class="region-footer clearfix">
						<p class="navbar-text footer-disclaimer">
							Copyright 2017. All right reserved. ATTORNEY ADVERTISING. The information provided on this website is not legal advice. No attorney-client relationship is formed by the use of this site. The attorney listings on the site are paid-for attorney advertisements and do not constitute a referral or endorsement by a state agency or bar association. It is not stated or implied that a lawyer is certified as a specialist in any particular field of law. No results are guaranteed, and prior results do not guarantee a similar outcome. This site is informational, only, not dispositive; it is up to you to decide whether a particular lawyer is right for you. Contingency fee refers only to attorney’s fees; a client may incur or be liable for other costs or expenses. Use of this site is subject to your agreement to these Terms and Conditions. If you are seeking an attorney in Florida, please read the additional state advertising disclosure.
						</p>
					</div>
				</div>
			</div>
		</div>
    </body>
</html>