Please accept our <a href="{{url('/terms')}}" target="_blank">Terms
    and conditions</a>
    <br>
<a class="read_more_terms" href="#" data-toggle="modal" data-target="#terms-txt">read more...</a>
<p class="read-more-text" style="font-size: 0.7em; display:none;">By agreeing to our terms of service, you consent and request to be contacted by accident.com, third parties/affiliates working on our behalf, and law firm(s) by phone, email, and text/SMS to the home or mobile number(s) you provided even if your provided number is on a national or state do not call list. In some cases, pre-recorded messages and automated technology may be used to contact you for marketing purposes. Please read our terms and conditions. There is no requirement that you provide consent as a condition of any purchase.
</p>