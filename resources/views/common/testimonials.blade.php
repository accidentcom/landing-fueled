<div class="container d-none">
    <h2 class="legal-areas-title">Testimonials</h2>
    <span class="short-underline">
        <hr>
    </span>
    <div class="row">
        <div id="reccomended" class="owl-carousel owl-theme owl-loaded owl-drag">
            <div class="item">
                <div class="review_listing">
                    <div class="clearfix">
                        <figure><img src="https://accident.nyc3.cdn.digitaloceanspaces.com/images/james.jpg" alt=""></figure>
<!--											<span class="rating"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><em>4.50/5.00</em></span>-->
                        <!--<small>Shops</small>-->
                    </div>
                    <h3 class="text-center"><strong>James</strong> from <a href="#">Denver, Colorado</a></h3>
                    <!--<h4>"Avesome Experience"</h4>-->
                    <p>“I was working on a new building development when I fell down, hit my head and suffered a concussion. I wasn’t sure if I needed a workers compensation attorney or a premises liability attorney. Accident.com made this process quick and easy”</p>
<!--										<ul class="clearfix">
                        <li><small>Published: 26.08.2018</small></li>
                        <li><a href="reviews-page.html" class="btn_1 small">Read review</a></li>
                    </ul>-->
                </div>
            </div>
            <div class="item">
                <div class="review_listing">
                    <div class="clearfix">
                        <figure><img src="https://accident.nyc3.cdn.digitaloceanspaces.com/images/mary.jpg" alt=""></figure>
                    </div>
                    <h3 class="text-center"><strong>Mary</strong> from <a href="#">Miami, Florida</a></h3>
                    <p>“Someone rear-ended me on the Long Island Expressway. I was in the hospital recovering when I found Accident.com. My lawyer came to my hospital room and guided me through the process. Rather than worrying about my hospital bill, I settled before I got out. “</p>
                </div>
            </div>
            <div class="item">
                <div class="review_listing">
                    <div class="clearfix">
                        <figure><img src="https://accident.nyc3.cdn.digitaloceanspaces.com/images/nathan.jpg" alt=""></figure>
                    </div>
                    <h3><strong>Nathan</strong> from <a href="#">Brooklyn, New York</a></h3>
                    <p>“It doesn’t feel good when your doctor tells you that you have the flu when you really have Lyme disease. For months I was not myself and almost lost my job. Without the help of my lawyer from Accident.com, I wouldn’t have known to pursue this claim.”</p>
                </div>
            </div>
            <div class="item">
                <div class="review_listing">
                    <div class="clearfix">
                        <figure><img src="https://accident.nyc3.cdn.digitaloceanspaces.com/images/linda.jpg" alt=""></figure>
                    </div>
                    <h3 class="text-center"><strong>Linda</strong> from <a href="#">Knoxville, Tennessee</a></h3>
                    <p>“I was in the supermarket and the floor was slippery. Unfortunately, there was no wet floor sign and I fell on my back. I came across this free case evaluation service and my premises liability lawyer told me that my case was worth pursuing.”</p>
                </div>
            </div>
            <div class="item">
                <div class="review_listing">
                    <div class="clearfix">
                        <figure><img src="https://accident.nyc3.cdn.digitaloceanspaces.com/images/patrik.jpg" alt=""></figure>
                    </div>
                    <h3 class="text-center"><strong>Patrick</strong> from <a href="#">San Antonio, Texas</a></h3>
                    <p>“Why not get your free consultation? I didn’t think it was worth it either until my brother told me that he used Accident.com to recover lost wages after a work related injury. With Accident.com, I discovered I was owed lost compensation too.”</p>
                </div>
            </div>
            <div class="item">
                <div class="review_listing">
                    <div class="clearfix">
                        <figure><img src="https://accident.nyc3.cdn.digitaloceanspaces.com/images/jhon.jpg" alt=""></figure>
                    </div>
                    <h3 class="text-center"><strong>John</strong> from <a href="#">Las Vegas, Nevada</a></h3>
                    <p>“I would like to thank Accident.com for providing this needed service. The process can be difficult and sometimes painful, but we were in good hands. We were given support and an experienced lawyer that made everything as simple as possible.”</p>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
	<h2 class="legal-areas-title">Testimonials</h2>
	<span class="short-underline">
		<hr>
	</span>
	<div class="row">
		<div id="reccomended" class="owl-carousel owl-theme owl-loaded owl-drag">
			<div class="item">
				<div class="review_listing">
					<div class="clearfix">
						<figure><img src="https://accident.nyc3.cdn.digitaloceanspaces.com/images/james.jpg" alt=""></figure>
						<span class="rating"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star empty"></i><small>4/5.00</small></span>
						<!--<small>Shops</small>-->
					</div>
					<h3><strong>James</strong> from <a href="javascript:void(0)">Denver, Colorado</a></h3>
					<h4>"Quick and Easy"</h4>
					<p>“I was working on a new building development when I fell down, hit my head and suffered a concussion. I wasn’t sure if I needed a workers compensation attorney or a premises liability attorney. Accident.com made this process quick and easy”</p>
					<!--										<ul class="clearfix">
																<li><small>Published: 26.08.2018</small></li>
																<li><a href="reviews-page.html" class="btn_1 small">Read review</a></li>
															</ul>-->
				</div>
			</div>
			<div class="item">
				<div class="review_listing">
					<div class="clearfix">
						<figure><img src="https://accident.nyc3.cdn.digitaloceanspaces.com/images/mary.jpg" alt=""></figure>
						<span class="rating"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><small>5/5.00</small></span>
						<!--<small>Shops</small>-->
					</div>
					<h3><strong>Mary</strong> from <a href="javascript:void(0)">Miami, Florida</a></h3>
					<h4>"Guided Me Through The Process"</h4>
					<p>“Someone rear-ended me on the Long Island Expressway. I was in the hospital recovering when I found Accident.com. My lawyer came to my hospital room and guided me through the process. Rather than worrying about my hospital bill, I settled before I got out. “</p>
				</div>
			</div>
<!--			<div class="item">
				<div class="review_listing">
					<div class="clearfix">
						<figure><img src="https://accident.nyc3.cdn.digitaloceanspaces.com/images/nathan.jpg" alt=""></figure>
						<span class="rating"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><small>4/5.00</small></span>
						<small>Shops</small>
					</div>
					<h3><strong>Nathan</strong> from <a href="#">Brooklyn, New York</a></h3>
					<h4>"Avesome Experience"</h4>
					<p>“It doesn’t feel good when your doctor tells you that you have the flu when you really have Lyme disease. For months I was not myself and almost lost my job. Without the help of my lawyer from Accident.com, I wouldn’t have known to pursue this claim.”</p>
				</div>
			</div>-->
			<div class="item">
				<div class="review_listing">
					<div class="clearfix">
						<figure><img src="https://accident.nyc3.cdn.digitaloceanspaces.com/images/linda.jpg" alt=""></figure>
						<span class="rating"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star empty"></i><small>4/5.00</small></span>
						<!--<small>Shops</small>-->
					</div>
					<h3><strong>Linda</strong> from <a href="javascript:void(0)">Knoxville, Tennessee</a></h3>
					<h4>"Worth Pursuing"</h4>
					<p>“I was in the supermarket and the floor was slippery. Unfortunately, there was no wet floor sign and I fell on my back. I came across this free case evaluation service and my premises liability lawyer told me that my case was worth pursuing.”</p>
				</div>
			</div>
			<div class="item">
				<div class="review_listing">
					<div class="clearfix">
						<figure><img src="https://accident.nyc3.cdn.digitaloceanspaces.com/images/patrik.jpg" alt=""></figure>
						<span class="rating"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><small>5/5.00</small></span>
						<!--<small>Shops</small>-->
					</div>
					<h3><strong>Patrick</strong> from <a href="javascript:void(0)">San Antonio, Texas</a></h3>
					<h4>"I Discovered I Was Owed Lost Compensation"</h4>
					<p>“Why not get your free consultation? I didn’t think it was worth it either until my brother told me that he used Accident.com to recover lost wages after a work related injury. With Accident.com, I discovered I was owed lost compensation too.”</p>
				</div>
			</div>
			<div class="item">
				<div class="review_listing">
					<div class="clearfix">
						<figure><img src="https://accident.nyc3.cdn.digitaloceanspaces.com/images/jhon.jpg" alt=""></figure>
						<span class="rating"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star empty"></i><i class="icon_star empty"></i><small>3/5.00</small></span>
						<!--<small>Shops</small>-->
					</div>
					<h3><strong>John</strong> from <a href="javascript:void(0)">Las Vegas, Nevada</a></h3>
					<h4>"We Were In Good Hands"</h4>
					<p>“I would like to thank Accident.com for providing this needed service. The process can be difficult and sometimes painful, but we were in good hands. We were given support and an experienced lawyer that made everything as simple as possible.”</p>
				</div>
			</div>
		</div>
	</div>
</div>