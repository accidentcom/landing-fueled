<script type="text/javascript" src="{{ asset('/js/jquery-3.1.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/tether.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/jquery.maskedinput.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/jquery.geocomplete.min.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQvx07EJzPf9uxT-j5g2FnYMuAgRURCQY&libraries=places"></script>
<script type="text/javascript" src="{{ asset('/js/current_place_geocoder.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/jquery.nice-select.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/HoldOn.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/modernizr.min.js') }}"></script>
<script type="text/javascript" src="{{asset('/js/common_scripts.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/velocity.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/survey_func.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/functions.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/moment.min.js')}}"></script>
<script src="{{asset('/js/focus-element-overlay.min.js')}}"></script>
{{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="http://www.xverify.com/js/clients/accidentllc/client.js"></script>
<script type="text/javascript" src="{{asset('js/x_verify.plugin.min.js')}}"></script>  --}}

{{-- <script type="text/javascript" src="{{ asset('/js/mdb.min.js') }}" defer></script> --}}

<script type="text/javascript" src="{{asset('/js/bootstrap-material-datetimepicker.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/theia-sticky-sidebar.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/ResizeSensor.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/js.cookie.js') }}"></script>

<!--<script type="text/javascript" src="{{ asset('/js/jquery.redirect.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/modernizr.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/modernizr.detects.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/mustache.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/js.cookie.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/xss.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/jquery.maskedinput.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/jquery.geocomplete.min.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQvx07EJzPf9uxT-j5g2FnYMuAgRURCQY&libraries=places"></script>
<script type="text/javascript" src="http://www.xverify.com/sharedjs/jquery-ui.min.js"></script>
<script type="text/javascript" src="http://www.xverify.com/js/clients/AccidentLLC/client.js"></script>
<script type="text/javascript" src="http://www.xverify.com/sharedjs/jquery.xverify.plugin.js"></script>-->
<!--<script type="text/javascript" src="{{ asset('/js/x_verify.plugin.js') }}"></script>-->
<script type="text/javascript">
	var loaderGif = "{{ asset('/images/accident/loader.gif') }}";

	var APP_ID = "b22xvgrz";
window.intercomSettings = {
    app_id: APP_ID
  };
(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/' + APP_ID;var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()

</script>
<script type="text/javascript" src="{{ asset('/js/loader.js') }}"></script>
<!--<script type="text/javascript" src="{{ asset('/js/form-validations.js') }}"></script>-->
<script type="text/javascript" src="{{ asset('/js/custom.js') }}"></script>
