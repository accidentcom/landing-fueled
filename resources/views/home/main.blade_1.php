<!DOCTYPE html>

<html lang="en" class="js no-touchevents no-browser-firefox browser-chrome gr__accidentCom-landing_com">
    <head>
        <title>Injured in an Accident? Get a FREE Case Consultation- (Recommended)  @yield('title')</title>

		<!-- metadata -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="description" content="Get a FREE Case Consultation for your queries">
		<meta name="keywords" content="accident,attorney,injury,lawyer,case,consultation,medical malpractice,wrongful,compensation,claim,wrongful,personal,slip,hospital,death">
		<!--<meta name="robots" content="">-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<!-- Favicons-->
		<link rel="shortcut icon" href="{{asset('/images/favicon.ico')}}" type="image/x-icon">


		<!-- GOOGLE WEB FONT -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,400" rel="stylesheet">

		<!-- css -->
		@include('common.stylesheets')
		<!-- javascripts -->
		@include('common.scripts')
    </head>
	<body>
		<div class="container-fluid page-wrapper">
			<header class="bg-accident-deepnavy">
				<div class="container">
					<nav class="navbar navbar-expand-lg bg-accident-deepnavy navbar-darkr fixed-top scrolling-navbar">
						<a href="#">
							<div class="accident-logo"></div>
						</a>
						<div class="collapse navbar-collapse main-menu">
							<ul class="navbar-nav ml-auto">
								<li class="nav-item active">
									<a class="nav-link waves-effect waves-light">Auto Accident</a>
								</li>
								<li class="nav-item active">
									<a class="nav-link waves-effect waves-light">Medical Malpractice</a>
								</li>
								<li class="nav-item active">
									<a class="nav-link waves-effect waves-light">Personal Injury</a>
								</li>
								<li class="nav-item active">
									<a class="nav-link waves-effect waves-light">Product Liability</a>
								</li>
							</ul>
						</div>
					</nav>
					<div class="row mt-3 pt-6">
						<div class="col-md-7 pl-0">
							<h1 class="heading">Injured in an Accident?</h1>
							<h6 class="sub-heading">Find out how much you're owed in compensation from an attorney near you!</h6>
						</div>
									</div>
					<div class="row start-eval">
						<h1>START EVALUATION</h1>
						<div class="col-lg-2 case-type">
							<a href="#">
								<div class="auto-accident-icon-orange"></div>
								<span>Auto Accident</span>
							</a>
						</div>
						<div class="col-lg-2 case-type">
							<a href="#">
								<div class="med-malpractice-icon-orange"></div>
								<span>Medical Malpractice</span>
							</a>
						</div>
						<div class="col-lg-2 case-type">
							<a href="#">
								<div class="personal-injury-icon-orange"></div>
								<span>Personal Injury</span>
							</a>
						</div>
						<div class="col-lg-2 case-type">
							<a href="#">
								<div class="workers-comp-icon-orange"></div>
								<span>Worker's comp</span>
							</a>
						</div>
						<div class="col-lg-2 case-type">
							<a href="#">
								<div class="ped-accident-blue"></div>
								<span>Product Liability</span>
							</a>
						</div>
						<div class="col-lg-2 case-type">
							<a href="#">
								<div class="wrongful-death-icon-orange"></div>
								<span>Wrong Death</span>
							</a>
						</div>

						<form method="post" action="">
							<div class="row no-gutters custom-search-input-2">
								<div class="col-lg-3">
									<div class="form-group">
										<input class="form-control" type="text" placeholder="Where">
										<i class="icon_pin_alt"></i>
									</div>
								</div>
								<div class="col-lg-3">
									<select class="wide" id="heroFormAccidentTypes">
										<option>All Categories</option>
										<option>Shops</option>
										<option>Hotels</option>
										<option>Restaurants</option>
										<option>Bars</option>
										<option>Events</option>
										<option>Fitness</option>
									</select>

								</div>
								<div class="col-lg-2">
									<input type="submit" value="Search">
								</div>
							</div>
							/row
						</form>
					</div>
					</div>
				</div>
			</header>
			<main class="content">
				<div class="hiw pt-4 pb-4">
					<div class="container">
						<div class="row">
							<h1 class="hiw-title">How It Works</h1>
							<span class="short-underline">
								<hr>
							</span>
							<div class="col-lg-4 hiw-box">
								<div><i class="pe-7s-search"></i></div>
								<div class="hiw-box-content">
									<h2>1. Find a Lawyer</h2>
									<p>Submit a contact form via desktop or mobile device</p>
								</div>
							</div>
							<div class="col-lg-4 hiw-box">
								<div><i class="pe-7s-info"></i></div>
								<div class="hiw-box-content">
									<h2>2. Get Connected</h2>
									<p>We directly connect your injury attorney in your area to you</p>
								</div>
							</div>
							<div class="col-lg-4 hiw-box">
								<div><i class="pe-7s-like2"></i></div>
								<div class="hiw-box-content">
									<h2>3. Free Consultation</h2>
									<p>You get a free, no obligation injury claim consultation</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="common-legal-areas pt-4">
					<div class="container">
						<div class="row">
							<h2 class="legal-areas-title">Most Common Legal Areas</h2>
							<span class="short-underline">
								<hr>
							</span>
							<div class="col-lg-4">
								<div class="card-deck legal-area">
									<div class="card mb-4">
										<div class="card-body">
											<a href="#">
												<div class="auto-accident-icon-orange logo"></div>
											</a>
											<h4 class="card-title">Auto Accident</h4>
											<ul>
												<li><a href="#">car</a></li>
												<li><a href="#">Motorcycle</a></li>
												<li><a href="#">Commercial</a></li>
												<li><a href="#">Hit and Run</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="card-deck legal-area">
									<div class="card mb-4">
										<div class="card-body">
											<a href="#">
												<div class="med-malpractice-icon-orange logo"></div>
											</a>
											<h4 class="card-title">Medical Malpractice</h4>
											<ul>
												<li>
													<a href="#">Misdiagnosis</a>
												</li>
												<li>
													<a href="#">Negligence</a>
												</li>
												<li>
													<a href="#">Child Birth</a>
												</li>
												<li>
													<a href="#">Nursing Home</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="card-deck legal-area">
									<div class="card mb-4">
										<div class="card-body">
											<a href="#">
												<div class="personal-injury-icon-orange logo"></div>
											</a>
											<h4 class="card-title">Personal Injury</h4>
											<ul>
												<li>
													<a href="#">Slip and Fall</a>
												</li>
												<li>
													<a href="#">Wrongful Death</a>
												</li>
												<li>
													<a href="#">Slip and Fall</a>
												</li>
												<li>
													<a href="#">Premises Liability</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-4">
								<div class="card-deck legal-area">
									<div class="card mb-4">
										<div class="card-body">
											<a href="#">
												<div class="ped-accident-blue logo"></div>
											</a>
											<h4 class="card-title">Product Liability</h4>
											<ul>
												<li>
													<a href="#">Defective Design</a>
												</li>
												<li>
													<a href="#">Medical Device</a>
												</li>
												<li>
													<a href="#">Lack of Instruction</a>
												</li>
												<li>
													<a href="#">Risk/Benefit</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="card-deck legal-area">
									<div class="card mb-4">
										<div class="card-body">
											<a href="#">
												<div class="workers-comp-icon-orange logo"></div>
											</a>
											<h4 class="card-title">Worker's Comp</h4>
											<ul>
												<li>
													<a href="#">Injured at Work</a>
												</li>
												<li>
													<a href="#">Disability Insurance</a>
												</li>
												<li>
													<a href="#">ERISA Disability</a>
												</li>
												<li>
													<a href="#">Denied Compensation</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="card-deck legal-area">
									<div class="card mb-4">
										<div class="card-body">
											<a href="#">
												<div class="wrongful-death-icon-orange logo"></div>
											</a>
											<h4 class="card-title">Wrongful Death</h4>
											<ul>
												<li>
													<a href="#">Catostrophic Injury</a>
												</li>
												<li>
													<a href="#">Medical Malpractice</a>
												</li>
												<li>
													<a href="#">Negligence</a>
												</li>
												<li>
													<a href="#">Homicide</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 ask-attorney">
								<span class="first-half">Don't see your case here?</span><span class="second-half"> You may still have a claim.</span>
								<button>Ask An Attorney</button>
							</div>
						</div>
					</div>
				</div>
<!--				<div class="owl-carousel owl-theme">
					<div> Your Content </div>
					<div> Your Content </div>
					<div> Your Content </div>
					<div> Your Content </div>
					<div> Your Content </div>
					<div> Your Content </div>
					<div> Your Content </div>
				</div>-->

<!--<div id="rrrrrr">
	<div class="col-lg-12">
		<div class="card-deck legal-area">
			<div class="card mb-4">
				<div class="card-body">
					<a href="#">
						<div class="wrongful-death-icon-orange logo"></div>
					</a>
					<h4 class="card-title">Wrongful Death</h4>
					<ul>
						<li>
							<a href="#">Catostrophic Injury</a>
						</li>
						<li>
							<a href="#">Medical Malpractice</a>
						</li>
						<li>
							<a href="#">Negligence</a>
						</li>
						<li>
							<a href="#">Homicide</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-12">
		<div class="card-deck legal-area">
			<div class="card mb-4">
				<div class="card-body">
					<a href="#">
						<div class="wrongful-death-icon-orange logo"></div>
					</a>
					<h4 class="card-title">Wrongful Death</h4>
					<ul>
						<li>
							<a href="#">Catostrophic Injury</a>
						</li>
						<li>
							<a href="#">Medical Malpractice</a>
						</li>
						<li>
							<a href="#">Negligence</a>
						</li>
						<li>
							<a href="#">Homicide</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>-->


<div id="carousel-legal-cases" class="carousel slide carousel-fade">

  <ol class="carousel-indicators">
    <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-1z" data-slide-to="1"></li>
    <li data-target="#carousel-example-1z" data-slide-to="2"></li>
  </ol>
    <div class="carousel-inner" role="listbox">

	<div class="carousel-item active">
            <div class="col-md-4">
                <div class="card">
                    <img class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(51).jpg" alt="Card image cap">
                    <div class="card-block">
                        <h4 class="card-title">Card title</h4>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Button</a>
                    </div>
                </div>
            </div>
	</div>
		<div class="carousel-item">
            <div class="col-md-4">
                <div class="card">
                    <img class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(51).jpg" alt="Card image cap">
                    <div class="card-block">
                        <h4 class="card-title">Card title</h4>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Button</a>
                    </div>
                </div>
            </div>
	</div>
	</div>
</div>


				<div class="articles-resource-center pt-4">
					<div class="container">
						<h2 class="legal-areas-title">Articles from the Resource Center</h2>
						<span class="short-underline">
							<hr>
						</span>
						<div class="row">
							<div class="col-md-6">
								<div class="card-body resource-center">
									<h4 class="title">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
									<p class="content">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card-body resource-center">
									<h4 class="title">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
									<p class="content">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="card-body resource-center">
									<h4 class="title">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
									<p class="content">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card-body resource-center">
									<h4 class="title">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
									<p class="content">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>


	</main>
	<footer class="bg-accident-deepnavy page-footer">
		<div class="container text-md-left">
			<div class="row footer-links text-md-left pb-1">
				<div class="col-md-2 footer-logo mx-auto mt-3">
					<h6 class="mb-4 text-color-manatee font-weight-bold">Accident.com</h6>
				</div>
				<hr class="w-100 clearfix d-md-none">
				<div class="col-md-3 mx-auto mt-3">
					<h6 class="mb-4 text-color-manatee font-weight-bold">Practice Areas</h6>
					<ul class="text-uppercase footer-font-small">
						<li class="mb-1"><a href="#">Auto Accident</a></li>
						<li class="mb-1"><a href="#">Medical Malpractice</a></li>
						<li class="mb-1"><a href="#">Personal Injury</a></li>
						<li class="mb-1"><a href="#">Product Liability</a></li>
						<li class="mb-1"><a href="#">Worker's Compensation</a></li>
						<li class="mb-1"><a href="#">Wrongful Death</a></li>
					</ul>
				</div>
				<hr class="w-100 clearfix d-md-none">
				<div class="col-md-2 mx-auto mt-3">
					<h6 class="mb-4 text-color-manatee font-weight-bold">About Us</h6>
					<ul class="text-uppercase footer-font-small">
						<li class="mb-1"><a href="#">Our Mission</a></li>
						<li class="mb-1"><a href="#">Our Vision</a></li>
						<li class="mb-1"><a href="#">Our Process</a></li>
						<li class="mb-1"><a href="#">Our Security</a></li>
					</ul>
				</div>
				<hr class="w-100 clearfix d-md-none">
				<div class="col-md-2 mx-auto mt-3">
					<h6 class="mb-4 text-color-manatee font-weight-bold">Important Links</h6>
					<ul class="text-uppercase footer-font-small">
						<li class="mb-1"><a href="https://accident.com/terms/" target="blank">Terms Of Service</a></li>
						<li class="mb-1"><a href="#">Contact A Lawyer</a></li>
					</ul>
				</div>
				<div class="col-md-3 contact-email mx-auto mt-3">
					<h6 class="mb-4 text-color-manatee font-weight-bold">We are always in touch with our clients</h6>
					<p class="text-uppercase footer-font-small">
						concierge@accident.com</p>
				</div>
			</div>
			<hr class="bg-manatee">
			<div class="row d-flex align-items-center">
				<div class="col-md-12">
					<p class="text-color-manatee footer-font-small text-md-left">
						<small>ATTORNEY ADVERTISING. The information provided on this website is not legal advice. No attorney-client relationship is formed by the use of this site. The attorney listings on the site are paid-for attorney advertisements and do not constitute a referral or endorsement by a state agency or bar association. It is not stated or implied that a lawyer is certified as a specialist in any particular field of law. No results are guaranteed, and prior results do not guarantee a similar outcome. This site is informational, only, not dispositive; it is up to you to decide whether a particular lawyer is right for you. Contingency fee refers only to attorney’s fees; a client may incur or be liable for other costs or expenses. Use of this site is subject to your agreement to these Terms and Conditions. If you are seeking an attorney in Florida, please read the additional state advertising disclosure.</small>
					</p>
					<p class="text-center text-color-manatee footer-font-small text-md-left">
						<small>Copyright 2017. All right reserved.</small>
					</p>
				</div>
			</div>
		</div>
	</footer>
</div>
</body>
</html>