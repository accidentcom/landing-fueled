<!DOCTYPE html>
<html lang="en">

<head>
    @include('gtm_views.gtm_auto_script')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Thank You | Accident.com</title>

    <!-- Favicons-->
		<link rel="shortcut icon" href="{{asset('/images/favicon.ico')}}" type="image/x-icon">
    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">

    <!-- BASE CSS -->
    @include('common.stylesheets')
    
    <script type="text/javascript">
    function delayedRedirect(){
        window.location = "{{url($redirect_url)}}";
    }
    function getCookie(name) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length == 2) return parts.pop().split(";").shift();
    }
    
    </script>
<script type="text/javascript" src="{{ asset('/js/thankyoujs.js') }}"></script>

</head>
<!-- onLoad="setTimeout('delayedRedirect()', 5000)" -->
<body style="background-color:#fff;">
    @include('gtm_views.gtm_auto_noscript')

<header class="d-none thank-you-header"><img class="thank-you-accident-logo" src="{{asset('/images/accident_logo.png')}}" alt="accident-logo"></header>
<!-- END SEND MAIL SCRIPT -->   

<div id="success">
    <div class="icon icon--order-success svg">
        <a id="accident_logo_redirect" href=""><img class="thank-you-accident-logo" src="{{asset('/images/accident_logo1.png')}}" alt="accident-logo"></a>
         <svg xmlns="http://www.w3.org/2000/svg" width="72px" height="72px">
          <g fill="none" stroke="#F9981D" stroke-width="2">
             <circle cx="36" cy="36" r="35" style="stroke-dasharray:240px, 240px; stroke-dashoffset: 480px;"></circle>
             <path d="M17.417,37.778l9.93,9.909l25.444-25.393" style="stroke-dasharray:50px, 50px; stroke-dashoffset: 0px;"></path>
          </g>
         </svg>
     </div>
	<h4><span>We have successfully recieved your information</span>Thank you for your time</h4>
	<small class="d-none">You will be redirect back in 5 seconds.</small>
</div>
</body>
</html>