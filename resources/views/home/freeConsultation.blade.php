<html>
	<head>
		{!! $gtm_script !!}
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<link rel="shortcut icon" href="https://accident.com/wp-content/uploads/2018/04/accident_icon.ico">
		<title>{{ $caseType }} Consultation</title>
		<meta name="description" content="Get a FREE Case Consultation for your queries">
		<style type="text/css"> html{ margin: 0; height: 100%; overflow: hidden; } iframe{ position: absolute; left:0; right:0; bottom:0; top:0; border:0; } </style>
	</head>
	<body>
		{!! $gtm_noscript !!}
		<iframe id="typeform-full" width="100%" height="100%" frameborder="0" src="{{ $uri }}"></iframe>
		<script type="text/javascript" src="https://embed.typeform.com/embed.js"></script>
	</body>
</html>