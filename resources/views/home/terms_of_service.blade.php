<!DOCTYPE html>

<html lang="en" class="js no-touchevents no-browser-firefox browser-chrome gr__accidentCom-landing_com" style="">
    <head>
		@include('gtm_views.gtm_script')
        <title>Injured in an Accident? Get a FREE Case Consultation- (Recommended)  @yield('title')</title>

		<!-- metadata -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="description" content="Don’t Let Insurance Companies Take Advantage Of You. Briefly Describe Your Case to Speak with a Top Attorney Now. Fast, Free, and Confidential Consultation.">
		<meta name="keywords" content="accident,attorney,injury,lawyer,case,consultation,medical malpractice,wrongful,compensation,claim,wrongful,personal,slip,hospital,death">
		<!--<meta name="robots" content="">-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<!-- Favicons-->
		<link rel="shortcut icon" href="{{asset('/images/favicon.ico')}}" type="image/x-icon">


		<!-- GOOGLE WEB FONT -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,400" rel="stylesheet">

		<!-- css -->
		@include('common.stylesheets')
		<!-- javascripts -->
		@include('common.scripts')
    </head>
	<body>
		@include('gtm_views.gtm_noscript')
		<div id="page" class="theia-exception">
			<div class="container-fluid page-wrapper">
				@include('partials.other_pages_navbar')
			</div>

			<div class="sub_header_in sticky_header">
				<div class="container">
					<h1>Terms & Policies</h1>
				</div>
			</div>
			<main>
				<div class="container margin_60_35">
					<div class="row">
						<aside class="col-lg-3" id="faq_cat">
							<div class="box_style_cat" id="faq_box">
								<ul id="cat_nav">
									<li><a href="#definitions" class="active">Definitions</a></li>
									<li><a href="#platform-services" class="active">Use of the Accident Platform Services</a></li>
									<li><a href="#your-information" class="active">You and Your Information</a></li>
									<li><a href="#property-rights" class="active">Proprietary Rights; License</a></li>
									<li><a href="#confidentiality" class="active">Confidentiality</a></li>
									<li><a href="#privacy" class="active">Privacy</a></li>
									<li><a href="#assumption" class="active">Assumption of Risk and Waiver of Claims</a></li>
									<li><a href="#disclaimers" class="active">Representations and Warranties; Disclaimers</a></li>
									<li><a href="#indemnification" class="active">Indemnification</a></li>
									<li><a href="#limits-liability" class="active">Limits of Liability</a></li>
									<li><a href="#termination" class="active">Term and Termination</a></li>
									<li><a href="#relation-parties" class="active">Relationship of the Parties</a></li>
									<li><a href="#miscellaneous" class="active">Miscellaneous Terms</a></li>
								</ul>
							</div>
						</aside>
						<div class="col-lg-9" id="faq">
							<div class="card">
								<div id="collapseOne_payment" class="collapse show" role="tabpanel" data-parent="#payment">
									<div class="card-body">
										<p>This User Agreement (“Agreement”) constitutes a legal agreement between you, an individual (“you” or “User”) and Accident LLC (“Accident” or “Company”), whereby the Company agrees to provide you information about independent providers of legal services using the Accident Platform Services (as defined below). The Accident Platform Services enable you to seek legal services from an Attorney who has agreed to pay the Company a fee to use the Accident Platform Services. You desire to enter into this Agreement for the purpose of accessing and using the Accident Platform Services. You acknowledge and agree that Company is a technology services provider that does not provide legal services. In order to use the Accident Platform Services, you must agree to the terms and conditions that are set forth below. Upon your execution (electronic or otherwise) of this Agreement, you and Company shall be bound by the terms and conditions set forth herein. IMPORTANT: PLEASE NOTE THAT TO USE THE ACCIDENT PLATFORM SERVICES, YOU MUST AGREE TO THE TERMS AND CONDITIONS SET FORTH BELOW. BY VIRTUE OF YOUR ELECTRONIC EXECUTION OF THIS AGREEMENT, YOU WILL BE ACKNOWLEDGING THAT YOU HAVE READ AND UNDERSTOOD ALL OF THE TERMS OF THIS AGREEMENT.</p>
									</div>
								</div>
							</div>
							<div role="tablist" class="add_bottom_45 accordion_2" id="definitions">
								<div class="card">
									<div class="card-header" role="tab">
										<h5 class="mb-0">
											<a data-toggle="collapse" href="#collapseOne_definitions" aria-expanded="true">1. Definitions</a>
										</h5>
									</div>
									<div id="collapseOne_definitions" class="collapse show" role="tabpanel" data-parent="#definitions">
										<div class="card-body">
											<p>1.1 “Affiliate” means an entity that, directly or indirectly, controls, is under the control of, or is under common control with a party, where control means having more than fifty percent (50%) of the voting stock or other ownership interest or the majority of the voting rights of such entity.</p>
											<p>	1.2 “Company” means Accident LLC and its officers, shareholders, directors, employees, and agents.</p>
											<p>	1.3 “Company Data” means all data related to the access and use of the Accident Platform Services hereunder, including all data related to Users (including User Information), all data related to Attorneys and to the provision of information relating to Legal Services via the Accident Platform Services and the User ID.</p>
											<p>	1.4 “Term” is as defined in Section 12.1.</p>
											<p>	1.5 “User ID” means the identification and password key assigned by Company to you that enables you to use and access the User App.</p>
											<p>	1.6 “Attorney” means an independent provider of legal services who has agreed to pay the Company a fee to be permitted to use the Accident Platform Services.</p>
											<p>	1.7 “Legal Services” means an Attorney’s provision of legal services to Users.</p>
											<p>	1.8 “Accident Platform Services” mean Accident’s that enable you to seek Legal Services; such Accident Platform Services include access to Accident’s software, websites, and related support services systems, as may be updated or modified from time to time.</p>
											<p>	1.9 “User” means you or any other end user authorized by Accident to use the Accident Platform for the purpose of seeking Legal Services offered by Attorneys.</p>
											<p>	1.10 “User Information” means information about you or any other User made available to Attorneys in connection with such User’s request for Legal Services, which may include the User’s name and contact information.</p>
											<p>	1.11 “Territory” means the location in which you reside, as represented in your User Information.</p>
										</div>
									</div>
								</div>
							</div>
							<div role="tablist" class="add_bottom_45 accordion_2" id="platform-services">
								<div class="card">
									<div class="card-header" role="tab">
										<h5 class="mb-0">
											<a data-toggle="collapse" href="#collapseOne_platform-services" aria-expanded="true">2. Use of the Accident Platform Services</a>
										</h5>
									</div>
									<div id="collapseOne_platform-services" class="collapse show" role="tabpanel" data-parent="#platform-services">
										<div class="card-body">
											<p>2.1 User IDs. Accident will issue you a User ID to enable you to access and use the Accident Platform in accordance with this Agreement.  Company reserves the right to deactivate your User ID if you have materially breached any provision of this Agreement.  You agree that you will maintain your User ID in confidence and not share your User ID with any third party.  You will immediately notify Company of any actual or suspected breach or improper use or disclosure of your User ID.</p>
											<p>2.2 Provision of Legal Services.  If an Attorney accepts your request for Legal Services, the Accident Platform Services will provide that Attorney with certain User Information about you. You shall not contact any Attorneys for any reason other than for the purposes of seeking Legal Services using the Accident Platform Services.</p>
											<p>2.3 Your Relationship with Attorneys.  Company does not investigate or certify the skills, qualifications or background of any Attorney, and Company makes no recommendation, endorsement or other representation concerning any Attorney. You acknowledge and agree that you are solely responsible for performing such investigation and taking such precautions as you deem reasonable regarding any Attorney. You shall have the sole responsibility for any damages or liabilities that arise from your acceptance of Legal Services.  When you submit an inquiry concerning Legal Services in a particular Territory and particular field of law, Company will provide you with contact information for a randomly-selected Attorney who has advised Company that such Attorney practices in that field and in that Territory.  You consent to Company’s sharing of your User Information with that Attorney, so the Attorney may contact you through the Platform, and you consent to being contacted by that Attorney through the Platform.  After an initial consultation with an Attorney, you may elect not to retain such Attorney at your sole discretion.</p>
											<p>2.4 Your Relationship with Company.   Company is not your attorney and does not provide Legal services. Company has no role in determining the financial arrangements you may choose to make with any Attorney, and does not receive a percentage of any compensation you may pay to any Attorney. You have no attorney-client relationship with Company. Company is not responsible or liable for the actions or inactions of any Attorney. Company retains the right to deactivate or otherwise restrict you from accessing or using the Accident Platform Services in the event of a violation or alleged violation of this Agreement, or any act or omission by you that causes harm to Company’s or its Affiliates’ brand, reputation or business as determined by Company in its sole discretion.</p>
										</div>
									</div>
								</div>
							</div>
							<div role="tablist" class="add_bottom_45 accordion_2" id="your-information">
								<div class="card">
									<div class="card-header" role="tab">
										<h5 class="mb-0">
											<a data-toggle="collapse" href="#collapseOne_your-information" aria-expanded="true">3. You and Your Information</a>
										</h5>
									</div>
									<div id="collapseOne_your-information" class="collapse show" role="tabpanel" data-parent="#your-information">
										<div class="card-body">
											<p>You acknowledge and agree that you shall be asked to provide the following information prior to seeking Legal Services through the Accident Platform: Name, email, phone number, case type, case description. </p>
										</div>
									</div>
								</div>
							</div>
							<div role="tablist" class="add_bottom_45 accordion_2" id="property-rights">
								<div class="card">
									<div class="card-header" role="tab">
										<h5 class="mb-0">
											<a data-toggle="collapse" href="#collapseOne_property-rights" aria-expanded="true">4. [Not Used]</a>
										</h5>
									</div>
								</div>
							</div>
							<div role="tablist" class="add_bottom_45 accordion_2" id="property-rights">
								<div class="card">
									<div class="card-header" role="tab">
										<h5 class="mb-0">
											<a data-toggle="collapse" href="#collapseOne_property-rights" aria-expanded="true">5. Proprietary Rights; License</a>
										</h5>
									</div>
									<div id="collapseOne_property-rights" class="collapse show" role="tabpanel" data-parent="#property-rights">
										<div class="card-body">
											<p>5.1 License Grant. Subject to the terms and conditions of this Agreement, Company hereby grants you a non-exclusive, non-transferable, non-sublicensable, non-assignable license, during the term of this Agreement, to use the Accident Platform Services solely for the purpose of seeking Legal Services. All rights not expressly granted to you are reserved by Company, its Affiliates and their respective licensors.</p>
											<p>5.2 Restrictions. You shall not, and shall not allow any other party to: (a) license, sublicense, sell, resell, transfer, assign, distribute or otherwise provide or make available to any other party the Accident Platform Services or any Company Data in any way; (b) modify or make derivative works based upon the Accident Platform Services; (c) improperly use the Accident Platform Services, including by creating Internet “links” to any part of the Accident Platform Services, “framing” or “mirroring” any part of the Accident Platform Services on any other websites or systems, or “scraping” or otherwise improperly obtaining data from the Accident Platform Services; (d) reverse engineer, decompile, modify, or disassemble the Accident Platform Services, except as allowed under applicable law; or (e) send spam or otherwise duplicative or unsolicited messages. In addition, you shall not, and shall not allow any other party to, access or use the Accident Platform Services to: (i) design or develop a competitive or substantially similar product or service; (ii) copy or extract any features, functionality, or content thereof; (iii) launch or cause to be launched on or in connection with the Accident Platform Services an automated program or script, including web spiders, crawlers, robots, indexers, bots, viruses or worms, or any program which may make multiple server requests per second, or unduly burden or hinder the operation and/or performance of the Accident Platform Services; or (iv) attempt to gain unauthorized access to the Accident Platform Services or its related systems or networks.</p>
											<p>5.3 Ownership. The Accident Platform Services and Company Data, including all intellectual property rights therein, are and shall remain (as between you and Company) the property of Company, its Affiliates or their respective licensors. Neither this Agreement nor your use of the Accident Platform Services or Company Data conveys or grants to you any rights in or related to the Accident Platform Services or Company Data, except for the limited license granted above. Other than as specifically permitted by the Company in connection with the Accident Platform Services, you are not permitted to use or reference in any manner Company’s, its Affiliates’, or their respective licensors’ company names, logos, products and service names, trademarks, service marks, trade dress, copyrights or other indicia of ownership, alone and in combination with other letters, punctuation, words, symbols and/or designs (the “ACCIDENT Marks and Names”) for any commercial purposes. You agree that you will not try to register or otherwise use and/or claim ownership in any of the ACCIDENT Marks and Names, alone or in combination with other letters, punctuation, words, symbols and/or designs, or in any confusingly similar mark, name or title, for any goods and services.</p>
										</div>
									</div>
								</div>
							</div>
							<div role="tablist" class="add_bottom_45 accordion_2" id="confidentiality">
								<div class="card">
									<div class="card-header" role="tab">
										<h5 class="mb-0">
											<a data-toggle="collapse" href="#collapseOne_confidentiality" aria-expanded="true">6. Confidentiality</a>
										</h5>
									</div>
									<div id="collapseOne_confidentiality" class="collapse show" role="tabpanel" data-parent="#confidentiality">
										<div class="card-body">
											<p>6.1 Each party acknowledges and agrees that in the performance of this Agreement and/or use of services pursuant to it may have access to or may be exposed to, directly or indirectly, confidential information of the other party (“Confidential Information”). Confidential Information includes Company Data, User IDs, User Information, and the transaction volume, marketing and business plans, business, financial, technical, operational and such other non-public information of each party (whether disclosed in writing or verbally) either (x) that such party designates as being proprietary or confidential, or (y) of which the other party should reasonably know that it should be treated as confidential.</p>
											<p>6.2 Each party acknowledges and agrees that: (a) all Confidential Information shall remain the exclusive property of the disclosing party; (b) it shall not use Confidential Information of the other party for any purpose except in furtherance of this Agreement; (c) it shall not disclose Confidential Information of the other party to any third party, except to its employees, officers, contractors, agents, service providers and personal representatives  (“Permitted Persons”) as necessary to perform and/or use services pursuant to this Agreement, provided that Permitted Persons are bound in writing to obligations of confidentiality and non-use of Confidential Information no less protective than the terms hereof, and provided further that the party disclosing the Confidential Information to its Permitted Persons is responsible for the conduct of such Permitted Persons; and (d) it shall return or destroy all Confidential Information of the disclosing party, upon the termination of this Agreement or at the request of the other party (subject to applicable law and, with respect to Company, its internal record-keeping requirements).</p>
											<p>6.3 Notwithstanding the foregoing, Confidential Information shall not include any information to the extent it: (a) is or becomes part of the public domain through no act or omission on the part of the receiving party; (b) was possessed by the receiving party prior to the date of this Agreement without an obligation of confidentiality; or (c) is disclosed to the receiving party by a third party having no obligation of confidentiality with respect thereto.</p>
											<p>6.4  Company may disclose Confidential Information it such disclosure is required pursuant to law, court order, subpoena or governmental authority, provided the receiving party notifies the disclosing party thereof and provides the disclosing party a reasonable opportunity to contest or limit such required disclosure.</p>
										</div>
									</div>
								</div>
							</div>
							<div role="tablist" class="add_bottom_45 accordion_2" id="privacy">
								<div class="card">
									<div class="card-header" role="tab">
										<h5 class="mb-0">
											<a data-toggle="collapse" href="#collapseOne_privacy" aria-expanded="true">7. Privacy</a>
										</h5>
									</div>
									<div id="collapseOne_privacy" class="collapse show" role="tabpanel" data-parent="#privacy">
										<div class="card-body">
											<p>7.1 Disclosure of Your Information. Subject to applicable law, Company and its Affiliates may, but shall not be required to, provide to you or an Attorney any information (including personal information (e.g., information you submit about yourself ) and any Company Data) about you if: (a) it is necessary to enforce the terms of this Agreement; (b) it is required, in Company’s or any Affiliate’s sole discretion, by applicable law or regulatory requirements (e.g., Company or its Affiliates receive a subpoena, warrant, or other legal process for information); (c) it is necessary, in Company’s or any Affiliate’s sole discretion, to (1) protect the safety, rights, property or security of Company or its Affiliates, the Accident Platform Services or any third party; (2) to protect the safety of the public for any reason including the facilitation of insurance claims related to the Accident Platform Services; (3) to detect, prevent or otherwise address fraud, security or technical issues; (4) to prevent or stop activity which Company or any of its Affiliates, in their sole discretion, may consider to be, or to pose a risk of being, an illegal, unethical, legally actionable or otherwise objectionable activity); or (d) it is required or necessary, in Company’s or any Affiliate’s sole discretion, for purposes related to your use of the Accident Platform Services. You understand that Company may retain your personal data for legal, regulatory, safety and other necessary purposes after this Agreement is terminated.</p>
											<p>7.2 Company and its Affiliates may collect your personal data during the course of your application for, and use of, the Accident Platform Services.  As part of the Accident Platform services, Company shall record all telephone calls and electronic communications through Platform, and you hereby consent to such recording.  Except as restricted by other provisions of this Agreement, such information may be stored, processed, transferred, and accessed by Company and its Affiliates, third parties, and service providers for business purposes, including for marketing, lead generation, service development and improvement, analytics, industry and market research, and other purposes consistent with Company’s and its Affiliates’ legitimate business needs. You expressly consent to such use of personal data.</p>
										</div>
									</div>
								</div>
							</div>
							<div role="tablist" class="add_bottom_45 accordion_2" id="assumption">
								<div class="card">
									<div class="card-header" role="tab">
										<h5 class="mb-0">
											<a data-toggle="collapse" href="#collapseOne_assumption" aria-expanded="true">8.  Assumption of Risk and Waiver of Claims</a>
										</h5>
									</div>
									<div id="collapseOne_assumption" class="collapse show" role="tabpanel" data-parent="#assumption">
										<div class="card-body">
											<p>You hereby assume full responsibility for, and waive all claims against Company and its Affiliates for, any and all expenses, losses and damages that you incur while using or as a result of using the Accident Platform Services.</p>
										</div>
									</div>
								</div>
							</div>
							<div role="tablist" class="add_bottom_45 accordion_2" id="disclaimers">
								<div class="card">
									<div class="card-header" role="tab">
										<h5 class="mb-0">
											<a data-toggle="collapse" href="#collapseOne_disclaimers" aria-expanded="true">9. Representations and Warranties; Disclaimers</a>
										</h5>
									</div>
									<div id="collapseOne_disclaimers" class="collapse show" role="tabpanel" data-parent="#disclaimers">
										<div class="card-body">
											<p>9.1 By You. You hereby represent and warrant that: (a) you have full power and authority to enter into this Agreement and perform your obligations hereunder; (b) you have not entered into, and during the Term will not enter into, any agreement that would prevent you from complying with this Agreement; and (c) you will comply with all applicable laws, rules and policies in your performance of this Agreement.</p>
											<p>9.2 Disclaimer of Warranties. COMPANY AND ITS AFFILIATES PROVIDE, AND YOU ACCEPT, THE ACCIDENT PLATFORM SERVICES AND THE COMPANY DATA ON AN “AS IS” AND “AS AVAILABLE” BASIS. COMPANY AND ITS AFFILIATES DO NOT REPRESENT, WARRANT OR GUARANTEE THAT YOUR ACCESS TO OR USE OF THE ACCIDENT PLATFORM SERVICES, USER APP OR THE COMPANY DATA WILL BE UNINTERRUPTED OR ERROR FREE. COMPANY AND ITS AFFILIATES MAKE NO REPRESENTATIONS, WARRANTIES OR GUARANTEES AS TO THE ACTIONS OR INACTIONS OF THE ATTORNEYS WHO MAY PROVIDE LEGAL SERVICES TO YOU. BY USING THE ACCIDENT PLATFORM SERVICES, YOU ACKNOWLEDGE AND AGREE THAT YOU MAY BE INTRODUCED TO AN ATTORNEY OR OTHER THIRD PARTY THAT MAY POSE HARM OR RISK TO YOU OR OTHER THIRD PARTIES. YOU ARE ADVISED TO TAKE REASONABLE PRECAUTIONS WITH RESPECT TO INTERACTIONS WITH THIRD PARTIES ENCOUNTERED IN CONNECTION WITH THE USE OF THE ACCIDENT PLATFORM SERVICES, INCLUDING ATTORNEYS. COMPANY AND ITS AFFILIATES EXPRESSLY DISCLAIM ALL LIABILITY FOR ANY ACT OR OMISSION OF YOU, ANY ATTORNEY OR OTHER THIRD PARTY.</p>
											<p>9.3 No Service Guarantee. COMPANY AND ITS AFFILIATES DO NOT GUARANTEE THE AVAILABILITY OR UPTIME OF THE ACCIDENT PLATFORM SERVICES. YOU ACKNOWLEDGE AND AGREE THAT THE ACCIDENT PLATFORM SERVICES MAY BE UNAVAILABLE AT ANY TIME AND FOR ANY REASON (e.g., DUE TO SCHEDULED MAINTENANCE OR NETWORK FAILURE). FURTHER, THE ACCIDENT PLATFORM SERVICES MAY BE SUBJECT TO LIMITATIONS, DELAYS, AND OTHER PROBLEMS INHERENT IN THE USE OF THE INTERNET, ELECTRONIC COMMUNICATION AND YOUR DEVICE, AND COMPANY AND ITS AFFILIATES ARE NOT RESPONSIBLE FOR ANY DELAYS, DELIVERY FAILURES, OR OTHER DAMAGES, LIABILITIES OR LOSSES RESULTING FROM SUCH PROBLEMS.</p>
										</div>
									</div>
								</div>
							</div>
							<div role="tablist" class="add_bottom_45 accordion_2" id="indemnification">
								<div class="card">
									<div class="card-header" role="tab">
										<h5 class="mb-0">
											<a data-toggle="collapse" href="#collapseOne_indemnification" aria-expanded="true">10. Indemnification</a>
										</h5>
									</div>
									<div id="collapseOne_indemnification" class="collapse show" role="tabpanel" data-parent="#indemnification">
										<div class="card-body">
											<p>You shall indemnify, defend (at Company’s option) and hold harmless Company and its Affiliates and their respective officers, directors, employees, agents, successors and assigns from and against any and all liabilities, expenses (including legal fees), damages, penalties, and fines arising out of or related to: (a) your breach of your representations, warranties or obligations under this Agreement; or (b) a claim by a third party (including Attorneys) directly or indirectly related to your use of the Accident Platform Services.</p>
										</div>
									</div>
								</div>
							</div>
							<div role="tablist" class="add_bottom_45 accordion_2" id="limits-liability">
								<div class="card">
									<div class="card-header" role="tab">
										<h5 class="mb-0">
											<a data-toggle="collapse" href="#collapseOne_limits-liability" aria-expanded="true">11. Limits of Liability</a>
										</h5>
									</div>
									<div id="collapseOne_limits-liability" class="collapse show" role="tabpanel" data-parent="#limits-liability">
										<div class="card-body">
											<p>COMPANY AND ITS AFFILIATES SHALL NOT BE LIABLE UNDER THIS AGREEMENT FOR ANY OF THE FOLLOWING, WHETHER BASED ON CONTRACT, TORT OR ANY OTHER LEGAL THEORY, EVEN IF A PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES: (i) ANY INCIDENTAL, PUNITIVE, SPECIAL, EXEMPLARY, CONSEQUENTIAL, OR OTHER INDIRECT DAMAGES OF ANY TYPE OR KIND; OR (ii) YOUR OR ANY THIRD PARTY’S PROPERTY DAMAGE, OR LOSS OR INACCURACY OF DATA, OR LOSS OF BUSINESS, REVENUE, PROFITS, USE OR OTHER ECONOMIC ADVANTAGE. IN NO EVENT SHALL THE LIABILITY OF COMPANY OR ITS AFFILIATES UNDER THIS AGREEMENT EXCEED.</p>
										</div>
									</div>
								</div>
							</div>
							<div role="tablist" class="add_bottom_45 accordion_2" id="termination">
								<div class="card">
									<div class="card-header" role="tab">
										<h5 class="mb-0">
											<a data-toggle="collapse" href="#collapseOne_termination" aria-expanded="true">12. Term and Termination</a>
										</h5>
									</div>
									<div id="collapseOne_termination" class="collapse show" role="tabpanel" data-parent="#termination">
										<div class="card-body">
											<p>12.1 Term. This Agreement shall commence on the date accepted by you and shall continue until terminated as set forth herein (the “Term”).</p>
											<p>12.2 Termination. Either party may terminate this Agreement without cause at any time upon seven written notice to the other party</p>
											<p>12.3 Effect of Termination. Sections 1, 2.3, 2.5, 4.5, 5.3, 6, 7, 9, 10, 11, 12.3, 13 and 14 shall survive the termination of this Agreement.</p>
										</div>
									</div>
								</div>
							</div>
							<div role="tablist" class="add_bottom_45 accordion_2" id="relation-parties">
								<div class="card">
									<div class="card-header" role="tab">
										<h5 class="mb-0">
											<a data-toggle="collapse" href="#collapseOne_relation-parties" aria-expanded="true">13. Relationship of the Parties</a>
										</h5>
									</div>
									<div id="collapseOne_relation-parties" class="collapse show" role="tabpanel" data-parent="#relation-parties">
										<div class="card-body">
											<p>13.1 Except as otherwise expressly provided herein the relationship between the parties under this Agreement is solely that of independent contracting parties. The parties expressly agree that no joint venture, partnership, agency or attorney-client relationship exists between Company and you.</p>
											<p>13.2 You have no authority to bind Company or its Affiliates and you undertake not to hold yourself out as an employee, agent or authorized representative of Company or its Affiliates.</p>
										</div>
									</div>
								</div>
							</div>
							<div role="tablist" class="add_bottom_45 accordion_2" id="miscellaneous">
								<div class="card">
									<div class="card-header" role="tab">
										<h5 class="mb-0">
											<a data-toggle="collapse" href="#collapseOne_miscellaneous" aria-expanded="true">14. Miscellaneous Terms</a>
										</h5>
									</div>
									<div id="collapseOne_miscellaneous" class="collapse show" role="tabpanel" data-parent="#miscellaneous">
										<div class="card-body">
											<p>14.1 Modification. In the event Company modifies the terms and conditions of this Agreement at any time, such modifications shall be binding on you only upon your acceptance, in accordance with the terms of this Agreement, of the modified Agreement. Company reserves the right to modify any information referenced at hyperlinks from this Agreement from time to time. You hereby acknowledge and agree that, by using the Accident Platform Services you are bound by any future amendments and additions to information referenced at hyperlinks herein, or documents incorporated herein. Continued use of the Accident Platform Services or User App after any such changes shall constitute your consent to such changes.</p>
											<p>14.2 Supplemental Terms; Terms of Service. Supplemental terms may apply to your use of the Accident Platform Services, such as use policies or terms related to certain features and functionality, which may be modified from time to time (“Supplemental Terms”). You may be presented with such Supplemental Terms from time to time. Supplemental Terms are in addition to, and shall be deemed a part of, this Agreement. Supplemental Terms shall prevail over this Agreement in the event of a conflict.</p>
											<p>14.3 Severability. If any provision of this Agreement is or becomes invalid or non-binding, the parties shall remain bound by all other provisions hereof. In that event, the parties shall replace the invalid or non-binding provision with provisions that are valid and binding and that have, to the greatest extent possible, a similar effect as the invalid or non-binding provision, given the contents and purpose of this Agreement.</p>
											<p>14.4 Assignment. Company may assign or transfer this Agreement or any or all of its rights or obligations under this Agreement from time to time without consent.</p>
											<p>14.5 Entire Agreement. This Agreement, including all Supplemental Terms, constitutes the entire agreement and understanding of the parties with respect to its subject matter and replaces and supersedes all prior or contemporaneous agreements or undertakings regarding such subject matter. In this Agreement, the words “including” and “include” mean “including, but not limited to.” The recitals form a part of this Agreement.</p>
											<p>14.6 No Third Party Beneficiaries. There are no third party beneficiaries to this Agreement. Nothing contained in this Agreement is intended to or shall be interpreted to create any third-party beneficiary claims.</p>
											<p>14.7 Notices. Any notice delivered by Company to you under this Agreement will be delivered by email to the email address associated with your account or by posting on the portal available to you on the Accident Platform Services. Any notice delivered by you to Company under this Agreement will be delivered by contacting Company at Concierge@accident.com</p>
											<p>14.8 Governing Law.   All disputes concerning or arising out of this Agreement shall be resolved under the law of the State of New York, without regard to its conflict of law principles, and the parties hereby agree to exclusive jurisdiction and venue in the state and federal courts located in New York County, New York.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
		</div>

		@include($footerPath);
	<script>
        var app_url = "{{url('/v2')}}";
        var case_type = {!! json_encode($accident_type) !!};
        var test_mode = {!! json_encode($test_mode) !!};
        /*
         $('input[name="accident_type"]').change(function(){
         var cur = $(this).val();
         $("#v2HeroForm").attr('action',"{{ url('start-evaluation').'?caseType='}}"+cur);
         }); */

	</script>
</body>
</html>