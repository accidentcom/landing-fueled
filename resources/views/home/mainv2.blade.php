<!DOCTYPE html>

<html lang="en" class="js no-touchevents no-browser-firefox browser-chrome gr__accidentCom-landing_com">
    <head>
		@include('gtm_views.gtm_script')
        <title>Injured in an Accident? Get a FREE Case Consultation- (Recommended)  @yield('title')</title>
		<!-- metadata -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="description" content="Don’t Let Insurance Companies Take Advantage Of You. Briefly Describe Your Case to Speak with a Top Attorney Now. Fast, Free, and Confidential Consultation.">
		<meta name="keywords" content="accident,attorney,injury,lawyer,case,consultation,medical malpractice,wrongful,compensation,claim,wrongful,personal,slip,hospital,death">
		<!--<meta name="robots" content="">-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<!-- Favicons-->
		<link rel="shortcut icon" href="{{asset('/images/favicon.ico')}}" type="image/x-icon">
		<!-- GOOGLE WEB FONT -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,400" rel="stylesheet">
		<!-- css -->
		@include('common.stylesheets')
		<!-- javascripts -->
		@include('common.scripts')
    </head>
	<body>
		@include('gtm_views.gtm_noscript')
		<div class="container-fluid page-wrapper">
			@include('partials.navbar')
			<section class="bg-accident-deepnavy content-bgz main-first-box">
				<div class="container pb-5">
					<div class="row pt-5 headings">
						<div class="col-md-12">
							<h1 class="heading">Injured in an Accident?</h1>
							<h5 class="sub-heading">Find out how much you're owed in compensation from an attorney near you!</h5>
						</div>
					</div>
					<div class="start-eval" rel="popover" data-content="Enter Your Zip Code And Select The Injury Type To Start The Evaluation">
						<div class="row accident-type-logos cc-selector">
							<h1 class="heading-1">What's My Claim Worth?</h1>
							<h2 class="heading-2">Choose Accident Type</h2>
							<!--<div class="cc-selector">-->
							<div class="col-md-2  case-type">
								<a href="#">
									<input id="auto" type="radio" name="accident_type" value="Auto Accident" onchange="getVals(this, 'accident_type');" checked/>
									<label class="drinkcard-cc auto-injury" for="auto"></label>
									<span class="caption">Auto Accident</span>
								</a>
							</div>
							<div class="col-md-2 case-type">
								<a href="#">
									<input id="medical" type="radio" name="accident_type" value="Medical Malpractice" onchange="getVals(this, 'accident_type');" />
									<label class="drinkcard-cc medical-injury"for="medical"></label>
									<span class="caption">Medical Malpractice</span>
								</a>
							</div>
							<div class="col-md-2 case-type">
								<a href="#">
									<input id="personal" type="radio" name="accident_type" value="Personal Injury" onchange="getVals(this, 'accident_type');" />
									<label class="drinkcard-cc personal-injury" for="personal"></label>
									<span class="caption">Personal Injury</span>
								</a>
							</div>
							<div class="col-md-2 case-type">
								<a href="#">
									<input id="worker" type="radio" name="accident_type" value="Workers Compensation" onchange="getVals(this, 'accident_type');" />
									<label class="drinkcard-cc worker-injury" for="worker"></label>
									<span class="caption">Worker's Comp</span>
								</a>
							</div>
							<div class="col-md-2 case-type">
								<a href="#">
									<input id="productL" type="radio" name="accident_type" value="Slip And Fall" onchange="getVals(this, 'accident_type');" />
									<label class="drinkcard-cc slip-and-fall-injury" for="productL"></label>
									<span class="caption">Slip And Fall</span>
								</a>
							</div>
							<div class="col-md-2 case-type">
								<a href="#">
									<input id="wrongful" type="radio" name="accident_type" value="Wrongful Death" onchange="getVals(this, 'accident_type');" />
									<label class="drinkcard-cc wrongful-injury" for="wrongful"></label>
									<span class="caption">Wrongful Death</span>
								</a>
							</div>
						</div>
						<div class="row my-4">
							<div class="container">
								<form id="mainHeroForm" method="post" action="{{ route('freeConsultation.store') }}" class="hero-form" >
									@csrf
									<div class="row no-gutters custom-search-input-2">
										<div class="col-lg-5">
											<select name="accidentType" class="wide">
												<option value="Auto Accident" selected="selected">Auto Accident</option>
												<option value="Medical Malpractice">Medical Malpractice</option>
												<option value="Personal Injury">Personal Injury</option>
												<option value="Slip And Fall">Slip And Fall</option>
												<option value="Workers Compensation">Worker's Compensation</option>
												<option value="Wrongful Death">Wrongful Death</option>
											</select>
											<input type="hidden" name="case_type" id="casaType" value="Auto Accident">
											<input type="hidden" name="redirect_url" id="redirect_url" value="<?php echo URL::current(); ?>">
											<input type="hidden" name="test_mode" id="testMode" value="false">
										</div>

										<div class="col-lg-5">
												<div class="form-group">
													<input class="geo-complete form-control" name="location" id="hero-form-location" type="text" placeholder="ZIP Code" auto-complete='false'>
													<i class="icon_pin_alt"></i>
												</div>
												<div class="address-info">
													<input type="hidden" name="address_street_number" id="address_street_number" data-geo="street_number">
													<input type="hidden" name="address_street_address" id="address_street_address" data-geo="route">
													<input type="hidden" name="address_zipcode" id="address_zipcode" data-geo="postal_code">
													<input type="hidden" name="addresss_city" id="addresss_city" data-geo="locality">
													<input type="hidden" name="address_county" id="address_county" data-geo="administrative_area_level_2">
													<input type="hidden" name="addresss_state" id="addresss_state"  data-geo="administrative_area_level_1">
													<input type="hidden" name="addresss_formated" id="addresss_formatted"  data-geo="formatted_address">
												</div>
										</div>

										<div class="col-lg-2">
											<input id="v2Sub" type="submit" value="Start Evaluation">
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
			
			<main class="content">
				<div class="hiw pt-4 pb-4">
					<div class="container">
						<div class="row">
							<h1 class="hiw-title">How It Works</h1>
							<span class="short-underline">
								<hr>
							</span>
							<div class="col-lg-4 hiw-box">
								<div><i class="pe-7s-search"></i></div>
								<div class="hiw-box-content">
									<h2>1. Find a Lawyer</h2>
									<p>Submit a contact form via desktop or mobile device</p>
								</div>
							</div>
							<div class="col-lg-4 hiw-box">
								<div><i class="pe-7s-info"></i></div>
								<div class="hiw-box-content">
									<h2>2. Get Connected</h2>
									<p>We directly connect your injury attorney in your area to you</p>
								</div>
							</div>
							<div class="col-lg-4 hiw-box">
								<div><i class="pe-7s-like2"></i></div>
								<div class="hiw-box-content">
									<h2>3. Free Consultation</h2>
									<p>You get a free, no obligation injury claim consultation</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="common-legal-areas pt-4">
					<div class="container">
						<div class="row">
							<h2 class="legal-areas-title">Most Common Legal Areas</h2>
							<span class="short-underline">
								<hr>
							</span>
							<div class="col-lg-4">
								<div class="populate_case_type card-deck toggle-hover-description legal-area">
									<div class="card mb-4">
										<div class="card-body">
											<a href="#" class="legal-area-image">
												<div class="auto-accident-icon-orange logo"></div>
											</a>
											<h4 class="card-title">Auto Accident</h4>
											<div class="legal-area-info ml-5 mr-5">
												<ul>
													<li>Car</li>
													<li>Motorcycle</li>
													<li>Commercial</li>
													<li>Hit and Run</li>
												</ul>
											</div>
											<div class="legal-area-description">
												5.5 million car accidents occur every year in the U.S. — 3 million cause injuries and 40,000 are fatal. Don’t let insurance companies take advantage of you. Speak to an accident attorney before you settle.
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="populate_case_type card-deck toggle-hover-description legal-area">
									<div class="card mb-4">
										<div class="card-body">
											<a href="#" class="legal-area-image">
												<div class="med-malpractice-icon-orange logo"></div>
											</a>
											<h4 class="card-title">Medical Malpractice</h4>
											<div class="legal-area-info ml-5 mr-5">
												<ul>
													<li><a href="#">Misdiagnosis</a></li>
													<li><a href="#">Negligence</a></li>
													<li><a href="#">Child Birth</a></li>
													<li><a href="#">Nursing Home</a></li>
												</ul>
											</div>
											<div class="legal-area-description">
												Medical negligence is the third leading cause of death in the U.S. and results in over $3 billion in payouts each year. Retain an attorney who is experienced and licensed in the state where malpractice occurred.
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="populate_case_type card-deck toggle-hover-description legal-area">
									<div class="card mb-4">
										<div class="card-body">
											<a href="#" class="legal-area-image">
												<div class="personal-injury-icon-orange logo"></div>
											</a>
											<h4 class="card-title">Personal Injury</h4>
											<div class="legal-area-info ml-5 mr-5">
												<ul>
													<li><a href="#">Slip and Fall</a></li>
													<li><a href="#">Wrongful Death</a></li>
													<li><a href="#">Premises Liability</a></li>
												</ul>
											</div>
											<div class="legal-area-description">
												Was the defendant liable? If so, what are the nature and extent of your damages? Act quickly, as the statute of limitations in each state limits the time you have to file a personal injury claim.
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-4">
								<div class="populate_case_type card-deck toggle-hover-description legal-area">
									<div class="card mb-4">
										<div class="card-body">
											<a href="#" class="legal-area-image">
												<div class="slip-and-fall-blue logo"></div>
											</a>
											<h4 class="card-title">Slip And Fall</h4>
											<div class="legal-area-info ml-5 mr-5">
												<ul>
													<li><a href="#">Defective Design</a></li>
													<li><a href="#">Medical Device</a></li>
													<li><a href="#">Lack of Instruction</a></li>
													<li><a href="#">Risk/Benefit</a></li>
												</ul>
											</div>
											<div class="legal-area-description">
												Product liability suits have the highest median settlement, yet only account for 5% of all personal injury cases. These cases are different from ordinary injury law, so experience in this area is vital.
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="populate_case_type card-deck toggle-hover-description legal-area">
									<div class="card mb-4">
										<div class="card-body">
											<a href="#" class="legal-area-image">
												<div class="workers-comp-icon-orange logo"></div>
											</a>
											<h4 class="card-title">Worker's Comp</h4>
											<div class="legal-area-info ml-5 mr-5">
												<ul>
													<li><a href="#">Injured at Work</a></li>
													<li><a href="#">Disability Insurance</a></li>
													<li><a href="#">ERISA Disability</a></li>
													<li><a href="#">Denied Compensation</a></li>
												</ul>
											</div>
											<div class="legal-area-description">
												Injured on the job? Denied social security? U.S. employers spend over $95 billion every year on workers’ compensation insurance. Over 140 million workers are covered — chances are, you are too.
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="populate_case_type card-deck toggle-hover-description legal-area">
									<div class="card mb-4">
										<div class="card-body">
											<!--<div class=" ">-->
											<a href="#" class="legal-area-image">
												<div class="wrongful-death-icon-orange logo"></div>
											</a>
											<h4 class="card-title">Wrongful Death</h4>
											<div class="legal-area-info ml-5 mr-5">
												<ul>
													<li><a href="#">Catostrophic Injury</a></li>
													<li><a href="#">Medical Malpractice</a></li>
													<li><a href="#">Negligence</a></li>
													<li><a href="#">Homicide</a></li>
												</ul>
											</div>
											<!--</div>-->
											<div class="legal-area-description">
												If you have a family member who has passed away due to an accident, medical malpractice, negligence, or intentional homicide — you may seek requital for your losses or damages in civil court.
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 ask-attorney">
								<span class="first-half">Don't see your case here?</span><span class="second-half"> You may still have a claim.</span>
								<button class="popover_info_v2">Ask An Attorney</button>
							</div>
						</div>
					</div>
				</div>

				<div class="carousel-swipe pt-4">
					<div class="container">
						<h2 class="legal-areas-title">Most Common Legal Areas</h2>
						<span class="short-underline">
							<hr>
						</span>
						<div class="owl-carousel owl-theme owl-loaded owl-drag">
							<div class="col-lg-12">
								<div class="populate_case_type card-deck legal-area">
									<div class="card mb-4">
										<div class="card-body mx-auto">
											<a href="#">
												<div class="auto-accident-icon-orange logo"></div>
											</a>
											<h4 class="card-title">Auto Accident</h4>
											<ul>
												<li>car</li>
												<li>Motorcycle</li>
												<li>Commercial</li>
												<li>Hit and Run</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="populate_case_type card-deck legal-area">
									<div class="card mb-4">
										<div class="card-body mx-auto">
											<a href="#">
												<div class="med-malpractice-icon-orange logo"></div>
											</a>
											<h4 class="card-title">Medical Malpractice</h4>
											<ul>
												<li><a href="#">Misdiagnosis</a></li>
												<li><a href="#">Negligence</a></li>
												<li><a href="#">Child Birth</a></li>
												<li><a href="#">Nursing Home</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="populate_case_type card-deck legal-area">
									<div class="card mb-4">
										<div class="card-body mx-auto">
											<a href="#">
												<div class="personal-injury-icon-orange logo"></div>
											</a>
											<h4 class="card-title">Personal Injury</h4>
											<ul>
												<li><a href="#">Slip and Fall</a></li>
												<li><a href="#">Wrongful Death</a></li>
												<li><a href="#">Slip and Fall</a></li>
												<li><a href="#">Premises Liability</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="populate_case_type card-deck legal-area">
									<div class="card mb-4">
										<div class="card-body mx-auto">
											<a href="#">
												<div class="slip-and-fall-blue logo"></div>
											</a>
											<h4 class="card-title">Slip And Fall</h4>
											<ul>
												<li><a href="#">Defective Design</a></li>
												<li><a href="#">Medical Device</a></li>
												<li><a href="#">Lack of Instruction</a></li>
												<li><a href="#">Risk/Benefit</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="populate_case_type card-deck legal-area">
									<div class="card mb-4">
										<div class="card-body mx-auto">
											<a href="#">
												<div class="workers-comp-icon-orange logo"></div>
											</a>
											<h4 class="card-title">Worker's Comp</h4>
											<ul>
												<li><a href="#">Injured at Work</a></li>
												<li><a href="#">Disability Insurance</a></li>
												<li><a href="#">ERISA Disability</a></li>
												<li><a href="#">Denied Compensation</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="populate_case_type card-deck legal-area">
									<div class="card mb-4">
										<div class="card-body mx-auto">
											<!--<div class=" ">-->
											<a href="#">
												<div class="wrongful-death-icon-orange logo"></div>
											</a>
											<h4 class="card-title">Wrongful Death</h4>
											<ul>
												<li><a href="#">Catostrophic Injury</a></li>
												<li><a href="#">Medical Malpractice</a></li>
												<li><a href="#">Negligence</a></li>
												<li><a href="#">Homicide</a></li>
											</ul>
											<!--</div>-->
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 ask-attorney">
								<span class="first-half">Don't see your case here?</span><span class="second-half"> You may still have a claim.</span>
								<button class="popover_info_v2">Ask An Attorney</button>
							</div>
						</div>
					</div>
				</div>
				<div class="articles-resource-center py-5">
					<div class="container d-none">
						<h2 class="legal-areas-title">About Us</h2>
						<span class="short-underline">
							<hr>
						</span>
						<div class="row">
							<div class="col-md-6">
								<div class="card-body resource-center">
									<h4 class="title">Our Mission</h4>
									<p class="content">The Most Important Thing Is That You Have A Speedy Recovery And Receive The Support You Need. The Last Thing You Should Worry About Is Finding An Attorney Or Settling Your Insurance Claim. Let Us Do The Heavy Lifting So You Can Focus On What’s Important.</p>
									<p class="content">Attorneys Must Complete An Extensive Application Process To Join Our Network. Once We Receive An Application, We Vet All Of The Information Provided To Us To Ensure We Feel Comfortable With Them Contacting You.</p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card-body resource-center">
									<h4 class="title">Our Vision</h4>
									<p class="content">It’s 2018. Do People Actually Retain Lawyers Who Advertise On Billboards Or Sing Toll-Free Numbers On Television? Why Can’t Lawyers Connect With Us The Same Way Ride-Hailing And Home-Improvement Services Do?</p>
									<p class="content">You’re Not Alone. We Thought It Didn’t Make Sense Too. Using The Same Recent Advances In Technology That You Are Accustomed To, You Never Have To Spend Time Speaking To A Call Center Ever Again. Get Your Consultation From An Attorney Who Contacts You Directly.</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="card-body resource-center">
									<h4 class="title">Our Process</h4>
									<p class="content">If You Have Always Wondered If You Have A Case, But Were Worried That You Do Not Have The Finances To Afford An Attorney — The Time Is Now To Get Your Case Evaluated. Accident Attorneys Are Paid On A Contingency Basis. This Means That You Only Have To Pay A Fee If You Win Your Case.</p>
									<p class="content">We Have Made The Process Of Getting A Consultation As Easy As Humanly Possible. On Average It Takes Our Users Less Than Two Minutes To Describe Their Accident And Submit Their Contact Details. Our Proprietary Algorithm Then Matches You To The Closest Attorney In Our Network Who Accepts Your Case Type. Our System Immediately Notifies Your Attorney On Their Phone To Ensure They Can Get In Contact Quickly, Wherever, And Whenever.</p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card-body resource-center">
									<h4 class="title">Our Security</h4>
									<p class="content">Passionate About Confidentiality, We Store Each User’s Information In Encrypted Form. Your Encrypted Information Is Erased Soon After Your Submission In Accordance With The GDPR Regulations. We Never Sell Or Rent Your Information To Third Parties.</p>
									<p class="content">Our Proprietary Algorithm Matches You And Your Attorney, Which Eliminates The Need For Anyone But Your Attorney To View Your Details. Contact Information Remains Private When Case Details Are Given. A Unique Identification Code Ensures That Only Your Attorney Can Contact You. We Then Use Temporary, Anonymous Numbers For Your Consultation To Make Sure Your Attorney Can Get In Touch Immediately.  Your Contact Details Are Shared With Each Other Once Your Consultation Is Over.</p>
								</div>
							</div>
						</div>
					</div>

					@include('common.testimonials')

					
				</div>
	</main>
	@include('partials.footer.footer_injury')
		</div>
		<script>
			var app_url = "{{url('/v2')}}";
			var case_type =  {!! json_encode($accident_type) !!};
			var test_mode =  {!! json_encode($test_mode) !!};
			
		</script>
</body>
</html>