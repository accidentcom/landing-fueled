<div class="step">
        <input type="hidden" name="data-injury" value="personal">
        <h3 class="main_question"><strong>2/9</strong>When did the incident occur?</h3>
        <input data-injury="personal" type="hidden" name="questions[accident_incident_year]" value="When did the incident occur?">
        <div class="form-group input data-injury="personal"-group mb-3">
               <!-- <input data-injury="personal" type="text"  name="accident_incident_year" class="date_p form-control" placeholder="MM/DD/YYYY" onchange="getVals(this, 'date');">
                <div class="input-group-append" data-injury="personal"><i class="date_picked_btn  fa fa-calendar"  style="color:black;cursor:pointer"></i>
                </div>-->

                <select data-injury="personal" name="accident_incident_year" class="wide22" onchange="getVals(this, 'date');">
                    <option value="" selected="selected">Select Incident Date</option>
                    <option value="Less than 1 Year">Less than 1 Year</option>
                    <option value="Less than 2 Years">Less than 2 Years</option>
                    <option value="Less than 3 Years">Less than 3 Years</option>
                    <option value="Less than 4 Years">Less than 4 Years</option>
                    <option value="5 or more Years">5 or more Years</option>
                </select>
        </div>
    
        <h3 class="main_question">Do you currently have a lawyer representing your claim?</h3>
        <input data-injury="personal" type="hidden" name="questions[got_attorney][personal]" value="Do you currently have a lawyer representing your claim?">
    
        <div class="form-group">
                <label class="container_radio version_2">Yes
                    <input data-injury="personal" type="radio" name="got_attorney[personal]" value="Yes" class="required" onchange="getVals(this, 'got_attorney[personal]');">
                    <span class="checkmark"></span>
                </label>
        </div>
        <div class="form-group">
            <label class="container_radio version_2">No
                <input data-injury="personal" type="radio" name="got_attorney[personal]" value="No" class="required" onchange="getVals(this, 'got_attorney[personal]');">
                <span class="checkmark"></span>
            </label>
        </div>
</div>
    
<div class="step">
    <h3 class="main_question"><strong>3/9</strong>Have you taken any action regarding your claim?</h3>
    <input data-injury="personal" type="hidden" name="questions[medical_claim][personal]" value="Have you taken any action regarding your claim?">
    <div class="form-group">
            <label class="container_radio version_2">No action taken yet
                <input data-injury="personal" type="radio" name="medical_claim[personal]" value="No action taken yet" class="required" onchange="getVals(this, 'medical_claim[personal]');">
                <span class="checkmark"></span>
            </label>
    </div>

    <div class="form-group">
        <label class="container_radio version_2">Demand Compensation
            <input data-injury="personal" type="radio" name="medical_claim[personal]" value="Demand Compensation" class="required" onchange="getVals(this, 'medical_claim[personal]');">
            <span class="checkmark"></span>
        </label>
    </div>

    <div class="form-group">
        <label class="container_radio version_2">Lawsuit Filed
            <input data-injury="personal" type="radio" name="medical_claim[personal]" value="Lawsuit Filed" class="required" onchange="getVals(this, 'medical_claim[personal]');">
            <span class="checkmark"></span>
        </label>
    </div>
</div>

<div class="step">
    <h3 class="main_question"><strong>4/9</strong>What is the primary type of injury?</h3>
    <input data-injury="personal" type="hidden" name="questions[primary_injury][personal]" value="What is the primary type of injury?">
    <div class="form-group">
        <label class="container_check version_2">Back or Neck Pain
            <input data-injury="personal" type="checkbox" name="primary_injury[personal][]" value="Back or Neck Pain" class="required" onchange="getVals(this, 'primary_injury[personal]');">
            <span class="checkmark"></span>
        </label>
    </div>
    <div class="form-group">
        <label class="container_check version_2">Broken Bones
            <input data-injury="personal" type="checkbox" name="primary_injury[personal][]" value="Broken Bones" class="required" onchange="getVals(this, 'primary_injury[personal]');">
            <span class="checkmark"></span>
        </label>
    </div>
    <div class="form-group">
        <label class="container_check version_2">Cut and bruises
            <input data-injury="personal" type="checkbox" name="primary_injury[personal][]" value="Cut and bruises" class="required" onchange="getVals(this, 'primary_injury[personal]');">
            <span class="checkmark"></span>
        </label>
    </div>
    <div class="form-group">
            <label class="container_check version_2">Headaches
                <input data-injury="personal" type="checkbox" name="primary_injury[personal][]" value="Headaches" class="required" onchange="getVals(this, 'primary_injury[personal]');">
                <span class="checkmark"></span>
            </label>
    </div>
    <div class="form-group">
            <label class="container_check version_2">Memory Loss
                <input data-injury="personal" type="checkbox" name="primary_injury[personal][]" value="Memory Loss" class="required" onchange="getVals(this, 'primary_injury[personal]');">
                <span class="checkmark"></span>
            </label>
    </div>
</div>

<div class="step">
    <h3 class="main_question"><strong>5/9</strong>What is the main cause of injury?</h3>
    <input data-injury="personal" type="hidden" name="questions[main_injury_type][personal]" value="What is the main cause of injury?">
    <div class="form-group">
            <label class="container_radio version_2">Traumatic Physical Injury
                <input data-injury="personal" type="radio" name="main_injury_type[personal]" value="Traumatic Physical Injury" class="required" onchange="getVals(this, 'main_injury_type[personal]');">
                <span class="checkmark"></span>
            </label>
    </div>
    <div class="form-group">
        <label class="container_radio version_2">Repeated Trauma Injury
            <input data-injury="personal" type="radio" name="main_injury_type[personal]" value="Repeated Trauma Injury" class="required" onchange="getVals(this, 'main_injury_type[personal]');">
            <span class="checkmark"></span>
        </label>
    </div>
    <div class="form-group">
        <label class="container_radio version_2">Mental Injury
            <input data-injury="personal" type="radio" name="main_injury_type[personal]" value="Mental Injury" class="required" onchange="getVals(this, 'main_injury_type[personal]');">
            <span class="checkmark"></span>
        </label>
    </div>
    <div class="form-group">
            <label class="container_radio version_2">Occupational Disease
                <input data-injury="personal" type="radio" name="main_injury_type[personal]" value="Occupational Disease" class="required" onchange="getVals(this, 'main_injury_type[personal]');">
                <span class="checkmark"></span>
            </label>
    </div>
</div>

    <div class="step">
        <h3 class="main_question"><strong>6/9</strong>Did the injury require hospitalization, medical treatment, or surgery?</h3>
        <input data-injury="personal" type="hidden" name="questions[medical_treatment][personal]" value="Did the injury require hospitalization, medical treatment, or surgery?">
            <div class="form-group">
                    <label class="container_radio version_2">Yes
                        <input data-injury="personal" type="radio" name="medical_treatment[personal]" value="Yes" class="required" onchange="getVals(this, 'medical_treatment[personal]');">
                        <span class="checkmark"></span>
                    </label>
            </div>
            <div class="form-group">
                <label class="container_radio version_2">No
                    <input data-injury="personal" type="radio" name="medical_treatment[personal]" value="No" class="required" onchange="getVals(this, 'medical_treatment[personal]');">
                    <span class="checkmark"></span>
                </label>
            </div>
        
    </div>
    
    <div class="step">
        <h3 class="main_question"><strong>7/10</strong>Help us better understand your situation with more details HERE</h3>
        <input data-injury="personal" type="hidden" name="questions[description]" value="Help us better understand your situation with more details HERE">
        <div class="form-group">
            <textarea name="description" maxlength="2500" class="form-control" style="height:233px;" placeholder="Type here..."
            onkeyup="getVals(this, 'description');"></textarea>
        </div>
    </div>
    
    <div class="step">
        <h3 class="main_question"><strong>8/9</strong>Contact Information</h3>
        <div class="form-group">
            <input data-injury="personal" type="text" name="first_name" class="form-control required" placeholder="First Name" autocomplete="off">
        </div>
        <div class="form-group">
            <input data-injury="personal" type="text" name="last_name" class="form-control required" placeholder="Last Name" autocomplete="off">
        </div>
        <div class="form-group">
            <input data-injury="personal" type="email" name="email" class="form-control required" placeholder="Your Email" autocomplete="off">
        </div>
        <div class="form-group">
                <input data-injury="personal" id="phone" type="text" name="phone" class="phone_number form-control required" placeholder="Your Phone" autocomplete="off">
        </div>
        
        <!-- /row -->
        <div class="form-group terms">
            <label class="container_check">
                    @include('common.terms_link_wilio_wizard')

                <input data-injury="personal" type="checkbox" name="terms" value="Yes" class="required">
                <span class="checkmark"></span>
            </label>
        </div>
    </div>
    <!-- /step-->
    
    <div class="submit step">
        <h3 class="main_question"><strong>9/9</strong>Summary</h3>
        <div class="summary">
            <ul>
                <li><strong>1</strong>
                    <h5>What type of Case were you involved in?</h5>
                    <p class="answer_accident_type"></p>	
                </li>
                <li><strong>2</strong>
                    <h5>When did the incident occur?</h5>
                    <p class="answer_date"></p>
                </li>
                <li><strong>3</strong>
                    <h5>Do you currently have a lawyer representing your claim?</h5>
                    <p class="answer_got_attorney[personal]"></p>
                </li>
                <li><strong>4</strong>
                    <h5>Have you taken any action regarding your claim?</h5>
                    <p class="answer_medical_claim[personal]"></p>
                </li>
                <li><strong>5</strong>
                    <h5>What is the primary type of injury?</h5>
                    <p class="answer_primary_injury[personal]"></p>
                </li>
                <li><strong>6</strong>
                    <h5>What is the main cause of injury?</h5>
                    <p class="answer_main_injury_type[personal]"></p>
                </li>
                <li><strong>7</strong>
                    <h5>Did the injury require hospitalization, medical treatment, or surgery?</h5>
                    <p class="answer_medical_treatment[personal]"></p>
                </li>
                <li><strong>8</strong>
                    <h5>Case Description</h5>
                    <p class="answer_description"></p>
                </li>
            </ul>
        </div>
    </div>