<div class="step">
        <input type="hidden" name="data-injury" value="auto">
        <h3 class="main_question"><strong>2/9</strong>When did the incident occur?</h3>
        <input data-injury="auto" type="hidden" name="questions[accident_incident_year]" value="When did the incident occur?">
        <div class="form-group input data-injury="auto"-group mb-3">
                <!--<input data-injury="auto" type="text"  name="accident_incident_year" class="date_p form-control" placeholder="MM/DD/YYYY" onchange="getVals(this, 'date');">
                <div class="input-group-append" data-injury="auto"><i class="date_picked_btn  fa fa-calendar" style="color:black;cursor:pointer"></i>
                </div>-->

                <select data-injury="auto" name="accident_incident_year" class="wide22" onchange="getVals(this, 'date');">
                    <option value="" selected="selected">Select Incident Date</option>
                    <option value="Less than 1 Year">Less than 1 Year</option>
                    <option value="Less than 2 Years">Less than 2 Years</option>
                    <option value="Less than 3 Years">Less than 3 Years</option>
                    <option value="Less than 4 Years">Less than 4 Years</option>
                    <option value="5 or more Years">5 or more Years</option>
                </select>
        </div>
        
        <h3 class="main_question">Do you currently have a lawyer representing your claim?</h3>
        <input data-injury="auto" type="hidden" name="questions[got_attorney][auto]" value="Do you currently have a lawyer representing your claim?">

        <div class="form-group">
                <label class="container_radio version_2">Yes
                    <input data-injury="auto" type="radio" name="got_attorney[auto]" value="Yes" class="required" onchange="getVals(this, 'got_attorney[auto]');">
                    <span class="checkmark"></span>
                </label>
        </div>
        <div class="form-group">
            <label class="container_radio version_2">No
                <input data-injury="auto" type="radio" name="got_attorney[auto]" value="No" class="required" onchange="getVals(this, 'got_attorney[auto]');">
                <span class="checkmark"></span>
            </label>
        </div>
</div>

<div class="step">
        <h3 class="main_question"><strong>3/9</strong>What type of vehicle accident were you involved in?</h3>
        <input data-injury="auto" type="hidden" name="questions[vehicle_type][auto]" value="What type of vehicle accident were you involved in?">
        <div class="form-group">
                <label class="container_radio version_2">Auto
                    <input data-injury="auto" type="radio" name="vehicle_type[auto]" value="Auto" class="required" onchange="getVals(this, 'vehicle_type[auto]');">
                    <span class="checkmark"></span>
                </label>
        </div>

        <div class="form-group">
                <label class="container_radio version_2">Truck
                    <input data-injury="auto" type="radio" name="vehicle_type[auto]" value="Truck" class="required" onchange="getVals(this, 'vehicle_type[auto]');">
                    <span class="checkmark"></span>
                </label>
        </div>

        <div class="form-group">
                <label class="container_radio version_2">SUV
                    <input data-injury="auto" type="radio" name="vehicle_type[auto]" value="SUV" class="required" onchange="getVals(this, 'vehicle_type[auto]');">
                    <span class="checkmark"></span>
                </label>
        </div>

        <div class="form-group">
                <label class="container_radio version_2">Motorcycle
                    <input data-injury="auto" type="radio" name="vehicle_type[auto]" value="Motorcycle" class="required" onchange="getVals(this, 'vehicle_type[auto]');">
                    <span class="checkmark"></span>
                </label>
        </div>
</div>


<div class="step">
        <h3 class="main_question"><strong>4/9</strong>What is the primary type of injury?</h3>
        <input data-injury="auto" type="hidden" name="questions[primary_injury][auto]" value="What is the primary type of injury?">
        <div class="form-group">
                <label class="container_check version_2">Back or Neck Pain
                    <input data-injury="auto" type="checkbox" name="primary_injury[auto][]" value="Back or Neck Pain" class="required" onchange="getVals(this, 'primary_injury[auto]');">
                    <span class="checkmark"></span>
                </label>
        </div>
        <div class="form-group">
            <label class="container_check version_2">Broken Bones
                <input data-injury="auto" type="checkbox" name="primary_injury[auto][]" value="Broken Bones" class="required" onchange="getVals(this, 'primary_injury[auto]');">
                <span class="checkmark"></span>
            </label>
        </div>
        <div class="form-group">
            <label class="container_check version_2">Cut and bruises
                <input data-injury="auto" type="checkbox" name="primary_injury[auto][]" value="Cut and bruises" class="required" onchange="getVals(this, 'primary_injury[auto]');">
                <span class="checkmark"></span>
            </label>
        </div>
        <div class="form-group">
                <label class="container_check version_2">Headaches
                    <input data-injury="auto" type="checkbox" name="primary_injury[auto][]" value="Headaches" class="required" onchange="getVals(this, 'primary_injury[auto]');">
                    <span class="checkmark"></span>
                </label>
        </div>
        <div class="form-group">
                <label class="container_check version_2">Memory Loss
                    <input data-injury="auto" type="checkbox" name="primary_injury[auto][]" value="Memory Loss" class="required" onchange="getVals(this, 'primary_injury[auto]');">
                    <span class="checkmark"></span>
                </label>
        </div>
        <div class="form-group">
                <label class="container_check version_2">Loss of Limb
                    <input data-injury="auto" type="checkbox" name="primary_injury[auto][]" value="Loss of Limb" class="required" onchange="getVals(this, 'primary_injury[auto]');">
                    <span class="checkmark"></span>
                </label>
        </div>
</div>

<div class="step">
    <h3 class="main_question"><strong>5/9</strong>Did the injury require hospitalization, medical treatment, or surgery?</h3>
    <input data-injury="auto" type="hidden" name="questions[medical_treatment][auto]" value="Did the injury require hospitalization, medical treatment, or surgery?">
        <div class="form-group">
                <label class="container_radio version_2">Yes
                    <input data-injury="auto" type="radio" name="medical_treatment[auto]" value="Yes" class="required" onchange="getVals(this, 'medical_treatment[auto]');">
                    <span class="checkmark"></span>
                </label>
        </div>
        <div class="form-group">
            <label class="container_radio version_2">No
                <input data-injury="auto" type="radio" name="medical_treatment[auto]" value="No" class="required" onchange="getVals(this, 'medical_treatment[auto]');">
                <span class="checkmark"></span>
            </label>
        </div>

    <h3 class="main_question">Did the injury require you to miss work?</h3>
    <input data-injury="auto" type="hidden" name="questions[missed_work][auto]" value="Did the injury require you to miss work?">
        <div class="form-group">
                <label class="container_radio version_2">Yes
                    <input data-injury="auto" type="radio" name="missed_work[auto]" value="Yes" class="required" onchange="getVals(this, 'missed_work[auto]');">
                    <span class="checkmark"></span>
                </label>
        </div>
        <div class="form-group">
            <label class="container_radio version_2">No
                <input data-injury="auto" type="radio" name="missed_work[auto]" value="No" class="required" onchange="getVals(this, 'missed_work[auto]');">
                <span class="checkmark"></span>
            </label>
        </div>
</div>


<div class="step">
    <h3 class="main_question"><strong>6/9</strong>Were you at fault for the accident?</h3>
    <input data-injury="auto" type="hidden" name="questions[at_fault][auto]" value="Were you at fault for the accident?">
    <div class="form-group">
        <label class="container_radio version_2">No
            <input data-injury="auto" type="radio" name="at_fault[auto]" value="No" class="required" onchange="getVals(this, 'at_fault[auto]');">
            <span class="checkmark"></span>
        </label>
    </div>
    <div class="form-group">
            <label class="container_radio version_2">Yes, I was cited or cause the accident
                <input data-injury="auto" type="radio" name="at_fault[auto]" value="Yes,I was cited or cause the accident" class="required" onchange="getVals(this, 'at_fault[auto]');">
                <span class="checkmark"></span>
            </label>
    </div>
    <div class="form-group">
            <label class="container_radio version_2">Yes, but it was the result of a tree, animal, or other act of nature
                <input data-injury="auto" type="radio" name="at_fault[auto]" value="Yes, but it was the result of a tree, animal, or other act of nature" class="required" onchange="getVals(this, 'at_fault[auto]');">
                <span class="checkmark"></span>
            </label>
    </div>
    <div class="form-group">
            <label class="container_radio version_2">Yes
                <input data-injury="auto" type="radio" name="at_fault[auto]" value="Yes" class="required" onchange="getVals(this, 'at_fault[auto]');">
                <span class="checkmark"></span>
            </label>
    </div>
</div>


<div class="step">
    <h3 class="main_question"><strong>7/9</strong>Help us better understand your situation with more details HERE</h3>
    <input data-injury="auto" type="hidden" name="questions[description]" value="Help us better understand your situation with more details HERE">
    <div class="form-group">
            <textarea name="description" maxlength="2500" class="form-control" style="height:233px;" placeholder="Type here..."
            onkeyup="getVals(this, 'description');"></textarea>
    </div>
</div>

<div class="step" data-state="end">
    <h3 class="main_question"><strong>8/9</strong>Contact Information</h3>
    <div class="form-group">
        <input data-injury="auto" type="text" name="first_name" class="form-control required" placeholder="First Name" autocomplete="off">
    </div>
    <div class="form-group">
        <input data-injury="auto" type="text" name="last_name" class="form-control required" placeholder="Last Name" autocomplete="off">
    </div>
    <div class="form-group">
        <input data-injury="auto" type="email" name="email" class="email form-control required" placeholder="Your Email" autocomplete="off">
    </div>
    <div class="form-group">
            <input data-injury="auto" id="phone" type="text" name="phone" class="phone_number form-control required" placeholder="Your Phone" autocomplete="off">
    </div>
    <!-- /row -->
    <div class="form-group terms">
        <label class="container_check">
                @include('common.terms_link_wilio_wizard')

            <input data-injury="auto" type="checkbox" name="terms" value="Yes" class="required">
            <span class="checkmark"></span>
        </label>
    </div>
</div>
<!-- /step-->

<div class="submit step" id="end">
    <h3 class="main_question"><strong>9/9</strong>Summary</h3>
    <div class="summary">
        <ul>
            <li><strong>1</strong>
                <h5>What type of vehicle accident were you involved in?</h5>
                <p class="answer_accident_type"></p>	
            </li>
            <li><strong>2</strong>
                <h5>When did the incident occur?</h5>
                <p class="answer_date"></p>
            </li>
            <li><strong>3</strong>
                <h5>Do you currently have a lawyer representing your claim?</h5>
                <p class="answer_got_attorney[auto]"></p>
            </li>
            <li><strong>4</strong>
                <h5>Did the injury require you to miss work?</h5>
                <p class="answer_missed_work[auto]"></p>
            </li>
            <li><strong>5</strong>
                <h5>What is the primary type of injury?</h5>
                <p class="answer_primary_injury[auto]"></p>
            </li>
            <li><strong>6</strong>
                <h5>Did the injury require hospitalization, medical treatment, or surgery?</h5>
                <p class="answer_medical_treatment[auto]"></p>
            </li>
            <li><strong>7</strong>
                <h5>Were you at fault for the accident?</h5>
                <p class="answer_at_fault[auto]"></p>
            </li>
            <li><strong>8</strong>
                <h5>What type of vehicle accident were you involved in?</h5>
                <p class="answer_vehicle_type[auto]"></p>
            </li>
            <li><strong>9</strong>
                <h5>Case Description</h5>
                <p class="answer_description"></p>
            </li>
        </ul>
    </div>
</div>
