<div class="step">
        <h3 class="main_question"><strong>2/7</strong>When did the incident occur?</h3>
        <input type="hidden" name="questions[accident_incident_year]" value="When did the incident occur?">
        <div class="form-group input-group mb-3">
               <!-- <input type="text  name="accident_incident_year" class="date_p form-control" placeholder="MM/DD/YYYY" onchange="getVals(this, 'date');">
                <div class="input-group-append"><i class="date_picked_btn  fa fa-calendar"  style="color:black;cursor:pointer"></i>
                </div> -->

                <select name="accident_incident_year" class="wide22" onchange="getVals(this, 'date');">
                    <option value="" selected="selected">Select Incident Date</option>
                    <option value="Less than 1 Year">Less than 1 Year</option>
                    <option value="Less than 2 Years">Less than 2 Years</option>
                    <option value="Less than 3 Years">Less than 3 Years</option>
                    <option value="Less than 4 Years">Less than 4 Years</option>
                    <option value="5 or more Years">5 or more Years</option>
                </select>
        </div>
    
        <h3 class="main_question">Do you currently have a lawyer representing your claim?</h3>
        <input type="hidden" name="questions[got_attorney]" value="Do you currently have a lawyer representing your claim?">
    
        <div class="form-group">
                <label class="container_radio version_2">Yes
                    <input type="radio" name="got_attorney" value="Yes" class="required" onchange="getVals(this, 'got_attorney');">
                    <span class="checkmark"></span>
                </label>
        </div>
        <div class="form-group">
            <label class="container_radio version_2">No
                <input type="radio" name="got_attorney" value="No" class="required" onchange="getVals(this, 'got_attorney');">
                <span class="checkmark"></span>
            </label>
        </div>
</div>
    
    <div class="step">
        <h3 class="main_question"><strong>3/7</strong>Did the injury require hospitalization, medical treatment, or surgery?</h3>
        <input type="hidden" name="questions[medical_treatment]" value="Did the injury require hospitalization, medical treatment, or surgery?">
            <div class="form-group">
                    <label class="container_radio version_2">Yes
                        <input type="radio" name="medical_treatment" value="Yes" class="required" onchange="getVals(this, 'medical_treatment');">
                        <span class="checkmark"></span>
                    </label>
            </div>
            <div class="form-group">
                <label class="container_radio version_2">No
                    <input type="radio" name="medical_treatment" value="No" class="required" onchange="getVals(this, 'medical_treatment');">
                    <span class="checkmark"></span>
                </label>
            </div>
        
    </div>
    
    
    <div class="step">
        <h3 class="main_question"><strong>4/7</strong>Were you at fault for the accident?</h3>
        <input type="hidden" name="questions[at_fault]" value="Were you at fault for the accident?">
        <div class="form-group">
            <label class="container_radio version_2">No
                <input type="radio" name="at_fault" value="No" class="required" onchange="getVals(this, 'at_fault');">
                <span class="checkmark"></span>
            </label>
        </div>
        <div class="form-group">
                <label class="container_radio version_2">Yes,I was cited or cause the accident
                    <input type="radio" name="at_fault" value="Yes,I was cited or cause the accident" class="required" onchange="getVals(this, 'at_fault');">
                    <span class="checkmark"></span>
                </label>
        </div>
        <div class="form-group">
                <label class="container_radio version_2">Yes, but it was the result of a tree, animal, or other act of nature
                    <input type="radio" name="at_fault" value="Yes, but it was the result of a tree, animal, or other act of nature" class="required" onchange="getVals(this, 'at_fault');">
                    <span class="checkmark"></span>
                </label>
        </div>
        <div class="form-group">
                <label class="container_radio version_2">Yes, but it was the result of a tree, animal, or other act of nature
                    <input type="radio" name="at_fault" value="Yes, but it was the result of a tree, animal, or other act of nature" class="required" onchange="getVals(this, 'at_fault');">
                    <span class="checkmark"></span>
                </label>
        </div>
        <div class="form-group">
                <label class="container_radio version_2">Yes
                    <input type="radio" name="at_fault" value="Yes" class="required" onchange="getVals(this, 'at_fault');">
                    <span class="checkmark"></span>
                </label>
        </div>
    </div>
    
    
    <div class="step">
        <h3 class="main_question"><strong>5/7</strong>Help us better understand your situation with more details HERE</h3>
        <input type="hidden" name="questions[description]" value="Help us better understand your situation with more details HERE">
        <div class="form-group">
            <textarea name="description" maxlength="2500" class="form-control" style="height:233px;" placeholder="Type here..."
            onkeyup="getVals(this, 'description');"></textarea>
        </div>
    </div>
    
    <div class="step">
        <h3 class="main_question"><strong>6/7</strong>Contact Information</h3>
        <div class="form-group">
            <input type="text" name="first_name" class="form-control required" placeholder="First Name" autocomplete="off">
        </div>
        <div class="form-group">
            <input type="text" name="last_name" class="form-control required" placeholder="Last Name" autocomplete="off">
        </div>
        <div class="form-group">
            <input type="email" name="email" class="form-control required" placeholder="Your Email" autocomplete="off">
        </div>
        <div class="form-group">
                <input id="phone" type="text" name="phone" class="phone_number form-control required" placeholder="Your Phone" autocomplete="off">
        </div>
        
        <!-- /row -->
        <div class="form-group terms">
            <label class="container_check">
                    @include('common.terms_link_wilio_wizard')

                <input type="checkbox" name="terms" value="Yes" class="required">
                <span class="checkmark"></span>
            </label>
        </div>
    </div>
    <!-- /step-->
    
    <div class="submit step">
        <h3 class="main_question"><strong>7/7</strong>Summary</h3>
        <div class="summary">
            <ul>
                <li><strong>1</strong>
                    <h5>What type of Case were you involved in?</h5>
                    <p class="answer_accident_type"></p>	
                </li>
                <li><strong>2</strong>
                    <h5>When did the incident occur?</h5>
                    <p class="answer_date"></p>
                </li>
                <li><strong>3</strong>
                    <h5>Do you currently have a lawyer representing your claim?</h5>
                    <p class="answer_got_attorney"></p>
                </li>
                <li><strong>4</strong>
                    <h5>Did the injury require hospitalization, medical treatment, or surgery?</h5>
                    <p class="answer_medical_treatment"></p>
                </li>
                <li><strong>5</strong>
                    <h5>Were you at fault for the accident?</h5>
                    <p class="answer_at_fault"></p>
                </li>
                <li><strong>6</strong>
                    <h5>Case Description</h5>
                    <p class="answer_description"></p>
                </li>
            </ul>
        </div>
    </div>