<div class="step">
        <input type="hidden" name="data-injury" value="medical">
        <h3 class="main_question"><strong>2/8</strong>When did the incident occur?</h3>
        <input data-injury="medical" type="hidden" name="questions[accident_incident_year]" value="When did the incident occur?">
        <div class="form-group input data-injury="medical"-group mb-3">
               <!-- <input data-injury="medical" type="text"  name="accident_incident_year" class="date_p form-control" placeholder="MM/DD/YYYY" onchange="getVals(this, 'date');">
                <div class="input-group-append" data-injury="medical"><i class="date_picked_btn  fa fa-calendar"  style="color:black;cursor:pointer"></i>
                </div> -->

                <select data-injury="medical" name="accident_incident_year" class="wide22" onchange="getVals(this, 'date');">
                    <option value="" selected="selected">Select Incident Date</option>
                    <option value="Less than 1 Year">Less than 1 Year</option>
                    <option value="Less than 2 Years">Less than 2 Years</option>
                    <option value="Less than 3 Years">Less than 3 Years</option>
                    <option value="Less than 4 Years">Less than 4 Years</option>
                    <option value="5 or more Years">5 or more Years</option>
                </select>
        </div>
    
        <h3 class="main_question">Do you currently have a lawyer representing your claim?</h3>
        <input data-injury="medical" type="hidden" name="questions[got_attorney][medical]" value="Do you currently have a lawyer representing your claim?">
    
        <div class="form-group">
                <label class="container_radio version_2">Yes
                    <input data-injury="medical" type="radio" name="got_attorney[medical]" value="Yes"  onchange="getVals(this, 'got_attorney[medical]');">
                    <span class="checkmark"></span>
                </label>
        </div>
        <div class="form-group">
            <label class="container_radio version_2">No
                <input data-injury="medical" type="radio" name="got_attorney[medical]" value="No"  onchange="getVals(this, 'got_attorney[medical]');">
                <span class="checkmark"></span>
            </label>
        </div>
</div>
    
    <div class="step">
        <h3 class="main_question"><strong>3/8</strong>What injuries resulted from Medical Malpractice?</h3>
        <input data-injury="medical" type="hidden" name="questions[medical_injury][medical]" value="What injuries resulted from Medical Malpractice?">
        <div class="form-group">
            <label class="container_check version_2">Minor injury
                <input data-injury="auto" type="checkbox" name="medical_injury[medical][]" value="Minor injury" class="required" onchange="getVals(this, 'medical_injury[medical]');">
                <span class="checkmark"></span>
            </label>
        </div>

        <div class="form-group">
                <label class="container_check version_2">Disfigurement or cosmetic injury
                    <input data-injury="auto" type="checkbox" name="medical_injury[medical][]" value="Disfigurement or cosmetic injury" class="required" onchange="getVals(this, 'medical_injury[medical]');">
                    <span class="checkmark"></span>
                </label>
        </div>

        <div class="form-group">
                <label class="container_check version_2">Loss of physical ability
                    <input data-injury="auto" type="checkbox" name="medical_injury[medical][]" value="Loss of physical ability" class="required" onchange="getVals(this, 'medical_injury[medical]');">
                    <span class="checkmark"></span>
                </label>
        </div>

        <div class="form-group">
                <label class="container_check version_2">Misdiagnosis
                    <input data-injury="auto" type="checkbox" name="medical_injury[medical][]" value="Misdiagnosis" class="required" onchange="getVals(this, 'medical_injury[medical]');">
                    <span class="checkmark"></span>
                </label>
        </div>

        <div class="form-group">
                <label class="container_check version_2">Death of Patient
                    <input data-injury="auto" type="checkbox" name="medical_injury[medical][]" value="Death of Patient" class="required" onchange="getVals(this, 'medical_injury[medical]');">
                    <span class="checkmark"></span>
                </label>
        </div>
        
    </div>

    <div class="step">
        <h3 class="main_question"><strong>4/8</strong>Have you taken any action regarding your claim?</h3>
        <input data-injury="medical" type="hidden" name="questions[medical_claim][medical]" value="Have you taken any action regarding your claim?">
        <div class="form-group">
                <label class="container_radio version_2">No action taken yet
                    <input data-injury="medical" type="radio" name="medical_claim[medical]" value="No action taken yet" class="required" onchange="getVals(this, 'medical_claim[medical]');">
                    <span class="checkmark"></span>
                </label>
        </div>

        <div class="form-group">
            <label class="container_radio version_2">Demand Compensation
                <input data-injury="medical" type="radio" name="medical_claim[medical]" value="Demand Compensation" class="required" onchange="getVals(this, 'medical_claim[medical]');">
                <span class="checkmark"></span>
            </label>
        </div>

        <div class="form-group">
            <label class="container_radio version_2">Lawsuit Filed
                <input data-injury="medical" type="radio" name="medical_claim[medical]" value="Lawsuit Filed" class="required" onchange="getVals(this, 'medical_claim[medical]');">
                <span class="checkmark"></span>
            </label>
        </div>
    </div>

    <div class="step">
        <h3 class="main_question"><strong>5/8</strong>Did the injury require hospitalization, medical treatment, or surgery?</h3>
        <input data-injury="medical" type="hidden" name="questions[medical_treatment][medical]" value="Did the injury require hospitalization, medical treatment, or surgery?">
            <div class="form-group">
                    <label class="container_radio version_2">Yes
                        <input data-injury="medical" type="radio" name="medical_treatment[medical]" value="Yes" class="required" onchange="getVals(this, 'medical_treatment[medical]');">
                        <span class="checkmark"></span>
                    </label>
            </div>
            <div class="form-group">
                <label class="container_radio version_2">No
                    <input data-injury="medical" type="radio" name="medical_treatment[medical]" value="No" class="required" onchange="getVals(this, 'medical_treatment[medical]');">
                    <span class="checkmark"></span>
                </label>
            </div>
        
    </div>
    
    
    <div class="step">
        <h3 class="main_question"><strong>6/8</strong>Help us better understand your situation with more details HERE</h3>
        <input data-injury="medical" type="hidden" name="questions[description]" value="Help us better understand your situation with more details HERE">
        <div class="form-group">
            <textarea name="description" maxlength="2500" class="form-control" style="height:233px;" placeholder="Type here..."
            onkeyup="getVals(this, 'description');"></textarea>
        </div>
    </div>
    
    <div class="step">
        <h3 class="main_question"><strong>7/8</strong>Contact Information</h3>
        <div class="form-group">
            <input data-injury="medical" type="text" name="first_name" class="form-control required" placeholder="First Name" autocomplete="off">
        </div>
        <div class="form-group">
            <input data-injury="medical" type="text" name="last_name" class="form-control required" placeholder="Last Name" autocomplete="off">
        </div>
        <div class="form-group">
            <input data-injury="medical" type="email" name="email" class="form-control required" placeholder="Your Email" autocomplete="off">
        </div>
        <div class="form-group">
                <input data-injury="medical" id="phone" type="text" name="phone" class="phone_number form-control required" placeholder="Your Phone" autocomplete="off">
        </div>
        
        <!-- /row -->
        <div class="form-group terms">
            <label class="container_check">
                    @include('common.terms_link_wilio_wizard')

                <input data-injury="medical" type="checkbox" name="terms" value="Yes" class="required">
                <span class="checkmark"></span>
            </label>
        </div>
    </div>
    <!-- /step-->
    
    <div class="submit step">
        <h3 class="main_question"><strong>8/8</strong>Summary</h3>
        <div class="summary">
            <ul>
                <li><strong>1</strong>
                    <h5>What type of Case were you involved in?</h5>
                    <p class="answer_accident_type"></p>	
                </li>
                <li><strong>2</strong>
                    <h5>When did the incident occur?</h5>
                    <p class="answer_date"></p>
                </li>
                <li><strong>3</strong>
                    <h5>Do you currently have a lawyer representing your claim?</h5>
                    <p class="answer_got_attorney[medical]"></p>
                </li>
                <li><strong>4</strong>
                    <h5>What injuries resulted from Medical Malpractice?</h5>
                    <p class="answer_medical_injury[medical]"></p>
                </li>
                <li><strong>5</strong>
                    <h5>Did the injury require hospitalization, medical treatment, or surgery?</h5>
                    <p class="answer_medical_treatment[medical]"></p>
                </li>
                <li><strong>6</strong>
                    <h5>Have you taken any action regarding your claim?</h5>
                    <p class="answer_medical_claim[medical]"></p>
                </li>
                <li><strong>7</strong>
                    <h5>Case Description</h5>
                    <p class="answer_description"></p>
                </li>
            </ul>
        </div>
    </div>