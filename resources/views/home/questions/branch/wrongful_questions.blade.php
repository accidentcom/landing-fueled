{{-- Victim's Death --}}
    <div class="step">
            <input type="hidden" name="data-injury" value="wrongful">
            <h3 class="main_question"><strong>2/6</strong>What is the primary cause of victim's death?</h3>
            <input data-injury="wrongful" type="hidden" name="questions[victim_death_cause][wrongful]" value="What is the primary cause of victim's death?">
            <div class="form-group">
                    <label class="container_radio version_2">Vehicle Accident
                        <input data-injury="wrongful" type="radio" name="victim_death_cause[wrongful]" value="Vehicle Accident" class="required" onchange="getVals(this, 'victim_death_cause[wrongful]');">
                        <span class="checkmark"></span>
                    </label>
            </div>

            <div class="form-group">
                    <label class="container_radio version_2">Reckless Act
                        <input data-injury="wrongful" type="radio" name="victim_death_cause[wrongful]" value="Reckless Act" class="required" onchange="getVals(this, 'victim_death_cause[wrongful]');">
                        <span class="checkmark"></span>
                    </label>
            </div>

            <div class="form-group">
                    <label class="container_radio version_2">Negligence/Careless Act
                        <input data-injury="wrongful" type="radio" name="victim_death_cause[wrongful]" value="Negligence/Careless Act" class="required" onchange="getVals(this, 'victim_death_cause[wrongful]');">
                        <span class="checkmark"></span>
                    </label>
            </div>
        
        </div>
        
        <div class="step">
                <h3 class="main_question"><strong>3/6</strong>Have any charges been filed against the responsible party?</h3>
                <input data-injury="wrongful" type="hidden" name="questions[filed_case][wrongful]" value="Have any charges been filed against the responsible party?">

                <div class="form-group">
                        <label class="container_radio version_2">Yes
                            <input data-injury="wrongful" type="radio" name="filed_case[wrongful]" value="Yes" class="required" onchange="getVals(this, 'filed_case[wrongful]');">
                            <span class="checkmark"></span>
                        </label>
                </div>
                <div class="form-group">
                    <label class="container_radio version_2">No
                        <input data-injury="wrongful" type="radio" name="filed_case[wrongful]" value="No" class="required" onchange="getVals(this, 'filed_case[wrongful]');">
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="form-group">
                        <label class="container_radio version_2">Not Sure
                            <input data-injury="wrongful" type="radio" name="filed_case[wrongful]" value="No" class="required" onchange="getVals(this, 'filed_case[wrongful]');">
                            <span class="checkmark"></span>
                        </label>
                </div>


                <h3 class="main_question">What is your relationship to the victim?</h3>
                <input data-injury="wrongful" type="hidden" name="questions[relationship][wrongful]" value="What is your relationship[wrongful] to the victim?">

                <div class="form-group">
                        <label class="container_radio version_2">Parent
                            <input data-injury="wrongful" type="radio" name="relationship[wrongful]" value="Parent" class="required" onchange="getVals(this, 'relationship[wrongful]');">
                            <span class="checkmark"></span>
                        </label>
                </div>
                <div class="form-group">
                        <label class="container_radio version_2">Spouse
                            <input data-injury="wrongful" type="radio" name="relationship[wrongful]" value="Spouse" class="required" onchange="getVals(this, 'relationship[wrongful]');">
                            <span class="checkmark"></span>
                        </label>
                </div>
                <div class="form-group">
                        <label class="container_radio version_2">Sibling
                            <input data-injury="wrongful" type="radio" name="relationship[wrongful]" value="Sibling" class="required" onchange="getVals(this, 'relationship[wrongful]');">
                            <span class="checkmark"></span>
                        </label>
                </div>
                <div class="form-group">
                        <label class="container_radio version_2">Friend
                            <input data-injury="wrongful" type="radio" name="relationship[wrongful]" value="Friend" class="required" onchange="getVals(this, 'relationship[wrongful]');">
                            <span class="checkmark"></span>
                        </label>
                </div>
        </div>

        <div class="step">
                <h3 class="main_question"><strong>4/6</strong>When did the incident occur?</h3>
                <input data-injury="wrongful" type="hidden" name="questions[accident_incident_year]" value="When did the incident occur?">
                <div class="form-group input data-injury="wrongful"-group mb-3">
                        <!--<input data-injury="wrongful" type="text"  name="accident_incident_year" class="date_p form-control" placeholder="MM/DD/YYYY" onchange="getVals(this, 'date');">
                        <div class="input-group-append" data-injury="wrongful"><i class="date_picked_btn  fa fa-calendar" style="color:black;cursor:pointer"></i>
                        </div> -->

                        <select data-injury="wrongful" name="accident_incident_year" class="wide22" onchange="getVals(this, 'date');">
                            <option value="" selected="selected">Select Incident Date</option>
                            <option value="Less than 1 Year">Less than 1 Year</option>
                            <option value="Less than 2 Years">Less than 2 Years</option>
                            <option value="Less than 3 Years">Less than 3 Years</option>
                            <option value="Less than 4 Years">Less than 4 Years</option>
                            <option value="5 or more Years">5 or more Years</option>
                        </select>
                </div>
    
                <h3 class="main_question">Help us better understand your situation with more details HERE</h3>
                <input data-injury="wrongful" type="hidden" name="questions[description]" value="Help us better understand your situation with more details HERE">
                <div class="form-group">
                        <textarea name="description" maxlength="2500" class="form-control" style="height:150px;" placeholder="Type here..."
                        onkeyup="getVals(this, 'description');"></textarea>
                </div>
        </div>
    
        <div class="step">
                <h3 class="main_question"><strong>5/6</strong>Contact Information</h3>
                <div class="form-group">
                    <input data-injury="wrongful" type="text" name="first_name" class="form-control required" placeholder="First Name" autocomplete="off">
                </div>
                <div class="form-group">
                    <input data-injury="wrongful" type="text" name="last_name" class="form-control required" placeholder="Last Name" autocomplete="off">
                </div>
                <div class="form-group">
                    <input data-injury="wrongful" type="email" name="email" class="form-control required" placeholder="Your Email" autocomplete="off">
                </div>
                <div class="form-group">
                        <input data-injury="wrongful" id="phone" type="text" name="phone" class="phone_number form-control required" placeholder="Your Phone" autocomplete="off">
                </div>
                <!-- /row -->
                <div class="form-group terms">
                    <label class="container_check">
                            @include('common.terms_link_wilio_wizard')

                        <input data-injury="wrongful" type="checkbox" name="terms" value="Yes" class="required">
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
            <!-- /step-->
        
        <div class="submit step">
            <h3 class="main_question"><strong>6/6</strong>Summary</h3>
            <div class="summary">
                <ul>
                    <li><strong>1</strong>
                        <h5>What type of case were you involved in?</h5>
                        <p class="answer_accident_type"></p>	
                    </li>
                    <li><strong>2</strong>
                        <h5>When did the incident occur?</h5>
                        <p class="answer_date"></p>
                    </li>
                    <li><strong>3</strong>
                        <h5>What is the primary cause of victim's death?</h5>
                        <p class="answer_victim_death_cause[wrongful]"></p>
                    </li>
                    <li><strong>4</strong>
                        <h5>Have any charges been filed against the responsible party?</h5>
                        <p class="answer_filed_case[wrongful]"></p>
                    </li>
                    <li><strong>5</strong>
                        <h5>What is your relationship to the victim?</h5>
                        <p class="answer_relationship[wrongful]"></p>
                    </li>
                    <li><strong>6</strong>
                        <h5>Case Description</h5>
                        <p class="answer_description"></p>
                    </li>
                </ul>
            </div>
        </div>    