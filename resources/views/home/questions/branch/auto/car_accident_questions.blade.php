<div class="step">
    <input type="hidden" name="data-injury" value="car">
    <h3 class="main_question"><strong>2/9</strong>When did the incident occur?</h3>
    <input data-injury="car" type="hidden" name="questions[accident_incident_year]" value="When did the incident occur?">
    <div class="form-group input-group mb-3" data-injury="car">
            <!-- <input data-injury="car" type="text"  name="accident_incident_year" class="date_p form-control" placeholder="MM/DD/YYYY" onchange="getVals(this, 'date');">
            <div class="input-group-append" data-injury="car"><i class="date_picked_btn  fa fa-calendar" style="color:black;cursor:pointer"></i>
            </div> -->

            <select data-injury="auto" name="accident_incident_year" class="wide22" onchange="getVals(this, 'date');">
                <option value="" selected="selected">Select Incident Date</option>
                <option value="Less than 1 Year">Less than 1 Year</option>
                <option value="Less than 2 Years">Less than 2 Years</option>
                <option value="Less than 3 Years">Less than 3 Years</option>
                <option value="Less than 4 Years">Less than 4 Years</option>
                <option value="5 or more Years">5 or more Years</option>
            </select>
    </div>
    
    <h3 class="main_question">Do you currently have a lawyer representing your claim?</h3>
    <input data-injury="car" type="hidden" name="questions[got_attorney][car]" value="Do you currently have a lawyer representing your claim?">

    <div class="form-group">
            <label class="container_radio version_2">Yes
                <input data-injury="car" type="radio" name="got_attorney[car]" value="Yes" class="required" onchange="getVals(this, 'got_attorney[car]');">
                <span class="checkmark"></span>
            </label>
    </div>
    <div class="form-group">
        <label class="container_radio version_2">No
            <input data-injury="car" type="radio" name="got_attorney[car]" value="No" class="required" onchange="getVals(this, 'got_attorney[car]');">
            <span class="checkmark"></span>
        </label>
    </div>
</div>


<div class="step">
    <h3 class="main_question"><strong>3/9</strong>What is the primary type of injury?</h3>
    <input data-injury="car" type="hidden" name="questions[primary_injury][car]" value="What is the primary type of injury?">
    <div class="form-group">
            <label class="container_check version_2">Back or Neck Pain
                <input data-injury="car" type="checkbox" name="primary_injury[car][]" value="Back or Neck Pain" class="required" onchange="getVals(this, 'primary_injury[car]');">
                <span class="checkmark"></span>
            </label>
    </div>
    <div class="form-group">
        <label class="container_check version_2">Broken Bones
            <input data-injury="car" type="checkbox" name="primary_injury[car][]" value="Broken Bones" class="required" onchange="getVals(this, 'primary_injury[car]');">
            <span class="checkmark"></span>
        </label>
    </div>
    <div class="form-group">
        <label class="container_check version_2">Cut and bruises
            <input data-injury="car" type="checkbox" name="primary_injury[car][]" value="Cut and bruises" class="required" onchange="getVals(this, 'primary_injury[car]');">
            <span class="checkmark"></span>
        </label>
    </div>
    <div class="form-group">
            <label class="container_check version_2">Headaches
                <input data-injury="car" type="checkbox" name="primary_injury[car][]" value="Headaches" class="required" onchange="getVals(this, 'primary_injury[car]');">
                <span class="checkmark"></span>
            </label>
    </div>
    <div class="form-group">
            <label class="container_check version_2">Memory Loss
                <input data-injury="car" type="checkbox" name="primary_injury[car][]" value="Memory Loss" class="required" onchange="getVals(this, 'primary_injury[car]');">
                <span class="checkmark"></span>
            </label>
    </div>
    <div class="form-group">
            <label class="container_check version_2">Loss of Limb
                <input data-injury="car" type="checkbox" name="primary_injury[car][]" value="Loss of Limb" class="required" onchange="getVals(this, 'primary_injury[car]');">
                <span class="checkmark"></span>
            </label>
    </div>
</div>

<div class="step">
<h3 class="main_question"><strong>4/9</strong>Did the injury require hospitalization, medical treatment, or surgery?</h3>
<input data-injury="car" type="hidden" name="questions[medical_treatment][car]" value="Did the injury require hospitalization, medical treatment, or surgery?">
    <div class="form-group">
            <label class="container_radio version_2">Yes
                <input data-injury="car" type="radio" name="medical_treatment[car]" value="Yes" class="required" onchange="getVals(this, 'medical_treatment[car]');">
                <span class="checkmark"></span>
            </label>
    </div>
    <div class="form-group">
        <label class="container_radio version_2">No
            <input data-injury="car" type="radio" name="medical_treatment[car]" value="No" class="required" onchange="getVals(this, 'medical_treatment[car]');">
            <span class="checkmark"></span>
        </label>
    </div>

<h3 class="main_question">Was a police report filed?</h3>
<input data-injury="car" type="hidden" name="questions[filed_case][car]" value="Was a police report filed?">
    <div class="form-group">
            <label class="container_radio version_2">Yes
                <input data-injury="car" type="radio" name="filed_case[car]" value="Yes" class="required" onchange="getVals(this, 'filed_case[car]');">
                <span class="checkmark"></span>
            </label>
    </div>
    <div class="form-group">
        <label class="container_radio version_2">No
            <input data-injury="car" type="radio" name="filed_case[car]" value="No" class="required" onchange="getVals(this, 'filed_case[car]');">
            <span class="checkmark"></span>
        </label>
    </div>
</div>

<div class="step">
    <h3 class="main_question"><strong>5/9</strong>Have you taken any action regarding your claim?</h3>
    <input data-injury="car" type="hidden" name="questions[medical_claim][car]" value="Have you taken any action regarding your claim?">
    <div class="form-group">
            <label class="container_radio version_2">No action taken yet
                <input data-injury="car" type="radio" name="medical_claim[car]" value="No action taken yet" class="required" onchange="getVals(this, 'medical_claim[car]');">
                <span class="checkmark"></span>
            </label>
    </div>

    <div class="form-group">
        <label class="container_radio version_2">Demand Compensation
            <input data-injury="car" type="radio" name="medical_claim[car]" value="Demand Compensation" class="required" onchange="getVals(this, 'medical_claim[car]');">
            <span class="checkmark"></span>
        </label>
    </div>

    <div class="form-group">
        <label class="container_radio version_2">Lawsuit Filed
            <input data-injury="product" type="radio" name="medical_claim[car]" value="Lawsuit Filed" class="required" onchange="getVals(this, 'medical_claim[car]');">
            <span class="checkmark"></span>
        </label>
    </div>
</div>

<div class="step">
<h3 class="main_question"><strong>6/9</strong>Were you at fault for the accident?</h3>
<input data-injury="car" type="hidden" name="questions[at_fault][car]" value="Were you at fault for the accident?">
<div class="form-group">
    <label class="container_radio version_2">No
        <input data-injury="car" type="radio" name="at_fault[car]" value="No" class="required" onchange="getVals(this, 'at_fault[car]');">
        <span class="checkmark"></span>
    </label>
</div>
<div class="form-group">
        <label class="container_radio version_2">Yes, I was cited or cause the accident
            <input data-injury="car" type="radio" name="at_fault[car]" value="Yes,I was cited or cause the accident" class="required" onchange="getVals(this, 'at_fault[car]');">
            <span class="checkmark"></span>
        </label>
</div>
<div class="form-group">
        <label class="container_radio version_2">Yes, but it was the result of a tree, animal, or other act of nature
            <input data-injury="car" type="radio" name="at_fault[car]" value="Yes, but it was the result of a tree, animal, or other act of nature" class="required" onchange="getVals(this, 'at_fault[car]');">
            <span class="checkmark"></span>
        </label>
</div>
<div class="form-group">
        <label class="container_radio version_2">Yes
            <input data-injury="car" type="radio" name="at_fault[car]" value="Yes" class="required" onchange="getVals(this, 'at_fault[car]');">
            <span class="checkmark"></span>
        </label>
</div>
</div>


<div class="step">
<h3 class="main_question"><strong>7/9</strong>Help us better understand your situation with more details HERE</h3>
<input data-injury="car" type="hidden" name="questions[description]" value="Help us better understand your situation with more details HERE">
<div class="form-group">
        <textarea name="description" maxlength="2500" class="form-control" style="height:233px;" placeholder="Type here..."
        onkeyup="getVals(this, 'description');"></textarea>
</div>
</div>

<div class="step" data-state="end">
<h3 class="main_question"><strong>8/9</strong>Contact Information</h3>
<div class="form-group">
    <input data-injury="car" type="text" name="first_name" class="form-control required" placeholder="First Name" autocomplete="off">
</div>
<div class="form-group">
    <input data-injury="car" type="text" name="last_name" class="form-control required" placeholder="Last Name" autocomplete="off">
</div>
<div class="form-group">
    <input data-injury="car" type="email" name="email" class="email form-control required" placeholder="Your Email" autocomplete="off">
</div>
<div class="form-group">
        <input data-injury="car" id="phone" type="text" name="phone" class="phone_number form-control required" placeholder="Your Phone" autocomplete="off">
</div>
<!-- /row -->
<div class="form-group terms">
    <label class="container_check">
            @include('common.terms_link_wilio_wizard')

        <input data-injury="car" type="checkbox" name="terms" value="Yes" class="required">
        <span class="checkmark"></span>
    </label>
</div>
</div>
<!-- /step-->

<div class="submit step" id="end">
<h3 class="main_question"><strong>9/9</strong>Summary</h3>
<div class="summary">
    <ul>
        <li><strong>1</strong>
            <h5>What type of vehicle accident were you involved in?</h5>
            <p class="answer_accident_type"></p>	
        </li>
        <li><strong>2</strong>
            <h5>When did the incident occur?</h5>
            <p class="answer_date"></p>
        </li>
        <li><strong>3</strong>
            <h5>Do you currently have a lawyer representing your claim?</h5>
            <p class="answer_got_attorney[car]"></p>
        </li>
        <li><strong>4</strong>
            <h5>Was a police report filed?</h5>
            <p class="answer_filed_case[car]"></p>
        </li>
        <li><strong>5</strong>
            <h5>Have you taken any action regarding your claim?</h5>
            <p class="answer_medical_claim[car]"></p>
        </li>
        
        <li><strong>6</strong>
            <h5>What is the primary type of injury?</h5>
            <p class="answer_primary_injury[car]"></p>
        </li>
        <li><strong>7</strong>
            <h5>Did the injury require hospitalization, medical treatment, or surgery?</h5>
            <p class="answer_medical_treatment[car]"></p>
        </li>
        <li><strong>8</strong>
            <h5>Were you at fault for the accident?</h5>
            <p class="answer_at_fault[car]"></p>
        </li>
        <li><strong>9</strong>
            <h5>Case Description</h5>
            <p class="answer_description"></p>
        </li>
    </ul>
</div>
</div>
