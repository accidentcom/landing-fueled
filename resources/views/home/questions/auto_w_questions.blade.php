<!DOCTYPE html>

<html lang="en" class="js no-touchevents no-browser-firefox browser-chrome gr__accidentCom-landing_com" style="">
    <head>
		@include('gtm_views.gtm_auto_script')
		<meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Injured in an Accident? Get a FREE Case Consultation- (Recommended)  @yield('title')</title>
		<!-- metadata -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="robots" content="noindex, nofollow">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		
		<!-- Favicons-->
		<link rel="shortcut icon" href="{{asset('/images/favicon.ico')}}" type="image/x-icon">


		<!-- GOOGLE WEB FONT -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,400" rel="stylesheet">

		<!-- css -->
		@include('common.stylesheets')
		<!-- javascripts -->
		@include('common.scripts')
		</head>
		<style>
			.custom_header{
				border: none;
				box-shadow: none !important;
				text-align: center;
			}
			.logoz{
				width:155px;
			}

			.grey_bg{
				background-color: #F2F4F6 !important;
			}

			@media screen and (max-width: 992px){
				#wizard_container{
					margin-top:53px;
				}

				.hero_p{
					font-size: 14px;
				}

				.bullets li{
					font-size: 20px;
				}
			}

			@media screen and (max-width: 1025px) and (min-width: 992px){
				#wizard_container{
					width: 681px;
				}

				.tst{
					margin-left: 157px !important; 
				}

				.ts2{
					width: 691px !important;
				}
			}
		</style>
	<body class="grey_bg">
		@include('gtm_views.gtm_auto_noscript')
		

		<header class="custom_header">
				<a href="{{url('/')}}">
						<img class="thank-you-accident-logo logoz" src="{{asset('/images/accident_logo1.png')}}" alt="accident-logo">
				</a>

		</header>

		<div id="preloader">
				<div data-loader="circle-side"></div>
			</div><!-- /Preload -->
		
		<div id="loader_form">
			<div data-loader="circle-side-2"></div>
		</div>
		<div class="container-fluid full-height grey_bg">
						<div class="row row-height">
							
								<div class="col-lg-6 tst" style="margin:0 auto;">
									<div id="wizard_container">
										<!-- /top-wizard -->
										<form name="accident_auto_wizard" id="accident_auto_wizard" method="POST">
											<div id="top-wizard">
												<div id="progressbar"></div>
											</div>
											<input id="website" name="website" type="text" value="">
											<input type="hidden" name="redirect_to" id="redirect_to" value="{{$template_data['redirect_to']}}">
											<input type="hidden" name="test_mode" id="testMode" value={{$template_data['test_mode']}}>

											<!-- Leave for security protection, read docs for details -->
											<div id="middle-wizard">
													<div class="step" data-state="accident_type">
														<h3 class="main_question" style="text-align:center;">{{--  <strong>1/8</strong>  --}}WHAT'S MY CLAIM WORTH?</h3>
			
															<div class="cc-selector">
																
																<ul id="case_type_wizard">
																		<li class="">
																				<input id="auto" type="radio" name="accident_type" value="Car Accident" onchange="getVals(this, 'accident_type');" checked/>
																				<label class="drinkcard-cc car-accident" for="auto"></label>
																				<span class="caption">Car Accident</span>
																		</li>
																		<li class="">
																				<input id="medical" type="radio" name="accident_type" value="Truck Accident" onchange="getVals(this, 'accident_type');" />
																				<label class="drinkcard-cc truck-accident"for="medical"></label>
																				<span class="caption">Truck Accident</span>
																		</li>
			
																		<li class="">
																				<input id="personal" type="radio" name="accident_type" value="Motorcycle Accident" onchange="getVals(this, 'accident_type');" />
																				<label class="drinkcard-cc motorcycle-accident" for="personal"></label>
																				<span class="caption">Motorcycle Accident</span>
																		</li>
			
																		<li class="">
																				<input id="productL" type="radio" name="accident_type" value="Bicycle Accident" onchange="getVals(this, 'accident_type');" />
																				<label class="drinkcard-cc bicycle-accident" for="productL"></label>
																				<span class="caption">Bicycle Accident</span>
																		</li>
			
																		<li class="">
																				<input id="worker" type="radio" name="accident_type" value="Work-Related Accident" onchange="getVals(this, 'accident_type');" />
																				<label class="drinkcard-cc work-related-accident" for="worker"></label>
																				<span class="caption">Work-Related</span>
																		</li>
																		<li class="">
																				<input id="wrongful" type="radio" name="accident_type" value="Pedestrian Accident" onchange="getVals(this, 'accident_type');" />
																				<label class="drinkcard-cc pedestrian-accident" for="wrongful"></label>
																				<span class="caption">Pedestrian Accident</span>
																		</li>
																</ul>
															</div>
			
			
															<div class="form-group form-zip">
															<input class="form-control required" type="text" name="address" id="address" placeholder="Your Zip Code" value="{{ $template_data['location'] }}">
										
																	<div class="address-info">

																	  		@if (strlen($template_data['address_ele']) > 0)
																								{!!$template_data['address_ele'] !!}
																				@else
																					<input type="hidden" name="address_street_number" id="address_street_number" data-geo="street_number">
																					<input type="hidden" name="address_street_address" id="address_street_address" data-geo="route">
																					<input type="hidden" name="address_zipcode" id="address_zipcode" data-geo="postal_code">
																					<input type="hidden" name="addresss_city" id="addresss_city" data-geo="locality">
																					<input type="hidden" name="address_county" id="address_county" data-geo="administrative_area_level_2">
																					<input type="hidden" name="addresss_state" id="addresss_state"  data-geo="administrative_area_level_1">
																					<input type="hidden" name="addresss_formated" id="addresss_formatted"  data-geo="formatted_address">
																				@endif
																	</div>
															</div>

															<div class="form-group form-case-type">
																<select name="accidentType" class="wide">
																	<option value="Car Accident" selected="selected">Car Accident</option>
                                                                    <option value="Motorcycle Accident">Motorcycle Accident</option>
                                                                    <option value="Truck Accident">Truck Accident</option>
                                                                    <option value="Bicycle Accident">Bicycle Accident</option>
                                                                    <option value="Work-Related Accident">Work-Related Accident</option>
                                                                    <option value="Pedestrian Accident">Pedestrian Accident</option>
																</select>
															</div>
														
													</div>
												
													{!! $steps !!}
												<!-- /step-->
											</div>
											<!-- /middle-wizard -->
											<div id="bottom-wizard">
												<button type="button" name="backward" class="backward">Prev</button>
												<button type="button" name="forward" class="forward">Next</button>
												<button type="submit" name="process" class="submit">Submit</button>
											</div>
											<!-- /bottom-wizard -->
										</form>
									</div>

									<div class="row ts2">
											<div class="col-sm-6">
													<ul class="bullets">
														{{--  <li><img src="{{asset("img/shield.png")}}" />Car accident</li>  --}}
														<li><img src="{{asset("img/shield_mono.png")}}" alt="shield" />Safe And Secure</li>
														<p class="hero_p">Your confidential information is encrypted to remain private, safe, and secure.</p>
														
														{{--  
														<li><img src="{{asset("img/shield2.png")}}" />Whiplash injuries</li>
														<li><img src="{{asset("img/checked.png")}}" />Rear end injuries</li>  --}}
													</ul>
												</div>
												<div class=" col-sm-6">
													<ul class="bullets">
														<li><img src="{{asset("img/success_mono.png")}}" alt="success" />Satisfaction Guaranteed</li>
														<p class="hero_p">For all injury claim inquiries, we guarantee a 48 hours response.</p>
													</ul>
												</div>

									</div>
								</div>
								
							</div>


						
			
	<script>
			var loaderGif = "{{asset('images/loader.gif')}}";
			var app_url = "{{url('/')}}";
			var case_type =  {!! json_encode($accident_type) !!};
			var test_mode =  {!! json_encode($test_mode) !!};


			$( window ).on( "load", function() {
				$("input[name=accident_type][value='" + case_type + "']").prop('checked', true);
				$('button[name="forward"]').trigger('click');
			});
	</script>
</body>

</html>