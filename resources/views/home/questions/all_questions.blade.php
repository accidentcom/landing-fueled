<div class="branch" id="Auto Accident">    
    @include('home.questions.branch.4ll_tr_auto_questions')
</div>

<div class="branch" id="Medical Malpractice">
        @include('home.questions.branch.medical_mal_questions')
</div>

<div class="branch" id="Personal Injury">
        @include('home.questions.branch.personal_questions')
</div>

<div class="branch" id="Slip And Fall">
    @include('home.questions.branch.slip_and_fall_questions')
</div>

<div class="branch" id="Workers Compensation">
        @include('home.questions.branch.worker_questions')
</div>

<div class="branch" id="Wrongful Death">
        @include('home.questions.branch.wrongful_questions')
</div>
