<div class="branch" id="Car Accident">    
    @include('home.questions.branch.auto.car_accident_questions')
</div>
<div class="branch" id="Pedestrian Accident">
        @include('home.questions.branch.auto.pedestrian_questions')
</div>
<div class="branch" id="Bicycle Accident">
        @include('home.questions.branch.auto.bicycle_questions')
</div>
<div class="branch" id="Work-Related Accident">
        @include('home.questions.branch.auto.work_r_questions')
</div>
<div class="branch" id="Truck Accident">
        @include('home.questions.branch.auto.truck_questions')
</div>
<div class="branch" id="Motorcycle Accident">
        @include('home.questions.branch.auto.motor_questions')
</div>

