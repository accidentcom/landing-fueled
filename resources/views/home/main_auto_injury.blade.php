<!DOCTYPE html>

<html lang="en" class="js no-touchevents no-browser-firefox browser-chrome gr__accidentCom-landing_com">
    <head>
		@include('gtm_views.gtm_auto_script')
        <title>Injured in an Accident? Get a FREE Case Consultation- (Recommended)  @yield('title')</title>
		<!-- metadata -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="description" content="Don’t Let Insurance Companies Take Advantage Of You. Briefly Describe Your Case to Speak with a Top Attorney Now. Fast, Free, and Confidential Consultation.">
		<meta name="keywords" content="accident,attorney,injury,lawyer,case,consultation,medical malpractice,wrongful,compensation,claim,wrongful,personal,slip,hospital,death">
		<!--<meta name="robots" content="">-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<!-- Favicons-->
		<link rel="shortcut icon" href="{{asset('/images/favicon.ico')}}" type="image/x-icon">
		<!-- GOOGLE WEB FONT -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,400" rel="stylesheet">
		<!-- css -->
		@include('common.stylesheets')
		<!-- javascripts -->
		@include('common.scripts')
    </head>
	<body>
		@include('gtm_views.gtm_auto_noscript')
		<div id="preloader">
			<div data-loader="circle-side"></div>
		</div><!-- /Preload -->
		<div id="loader_form">
			<div data-loader="circle-side-2"></div>
		</div>
		<div class="container-fluid page-wrapper">
			@include('partials.auto_navbar')
			<section class="content-bgz">
				<div class="container full-height">
						<div class="row">
								<div class="col-lg-7 d-none d-sm-block">
									<div class="top_content content-right-wrapper">
										<div class="column_content">
											<div id="cta_main_part">
												<h1 style="color:white;"><b>Injured in an Accident?</b></h1>
												<p class="d-none bullets">Find out how much you're owed in compensation from an attorney near you!</p>
											</div>
									{{--  		<div id="bullet_list">
												<div class="clearfix"></div>
												<p style="font-size:17px;margin-top:20px;" class="d-md-none d-lg-block d-sm-none d-md-block d-none d-sm-block">Lorem
													Ipsum has been the
													industry's standard dummy text ever since the 1500s</p>
												<div class="row">
													<div class="col-sm-6">
														<ul class="bullets">
															<li><img src="{{asset("img/checked.png")}}" />Car accident</li>
															<li><img src="{{asset("img/checked.png")}}" />Hit and run</li>
															<li><img src="{{asset("img/checked.png")}}" />Whiplash injuries</li>
															<li><img src="{{asset("img/checked.png")}}" />Rear end injuries</li>
														</ul>
													</div>
													<div class="col-sm-6">
														<ul class="bullets">
															<li><img src="{{asset("img/checked.png")}}" />Truck accident</li>
															<li><img src="{{asset("img/checked.png")}}" />Drunk driving</li>
															<li><img src="{{asset("img/checked.png")}}" />Motorcycle accident</li>
															<li><img src="{{asset("img/checked.png")}}" />and more...</li>
														</ul>
													</div>
												</div>
											</div>  --}}
											<div id="bullet_list">
												<div class="clearfix"></div>
												<p style="font-size:18px;margin-top:30px;" class="d-md-none d-lg-block d-sm-none d-md-block d-none d-sm-block">Find out how much you're owed in compensation from an attorney near you!</p>
												<div class="row" style="
												padding-top: 40px;
												padding-bottom: 33px;
											">
													<div class="col-sm-6">
														<ul class="bullets">
															{{--  <li><img src="{{asset("img/shield.png")}}" />Car accident</li>  --}}
														<li><img src="{{asset("img/shield.png")}}" alt="shield" />Safe And Secure</li>
															<p class="hero_p">Your confidential information is encrypted to remain private, safe, and secure.</p>
															
															{{--  
															<li><img src="{{asset("img/shield2.png")}}" />Whiplash injuries</li>
															<li><img src="{{asset("img/checked.png")}}" />Rear end injuries</li>  --}}
														</ul>
													</div>
													<div class=" col-sm-6">
														<ul class="bullets">
															<li><img src="{{asset("img/success.png")}}" alt="success" />Satisfaction Guaranteed</li>
															<p class="hero_p">For all injury claim inquiries, we guarantee a 48 hours response.</p>
														</ul>
													</div>
												</div>
											</div>
											<div>
												<h4 style="color:white;margin-top:20px;" class="blockquote d-md-none d-lg-block d-sm-none d-md-block d-none d-sm-block"><i style="
													font-size: 24px;
													font-weight: bolder;
												">don't settle your injury claim for less than you deserve</i>
												</h4>
												<p class="hero_p">Have someone experienced guide you through your claim, who will not only give you important legal advice but put you on the fast track to your settlement.</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-5">
									<div id="wizard_containerv2">
										<!-- /top-wizard -->
										<form method="post" action="{{ route('freeConsultation.store') }}" class="hero-form" id="accident_auto_wizardv2" method="POST" rel="popover" data-content="Enter Your Zip Code And Select The Injury Type To Start The Evaluation">
                                            @csrf
													<div class="step">
														<h3 style="text-align:center;" class="main_question">{{--  <strong>1/8</strong>  --}}WHAT'S MY CLAIM WORTH?</h3>
			
															<div class="cc-selector">
															<input type="hidden" name="redirect_url" id="redirect_url" value="<?php echo URL::current(); ?>">
																<ul id="case_type_wizard">
																		<li class="case-type-m">
																				<input id="auto" type="radio" name="accident_type" value="Car Accident" checked/>
																				<label class="drinkcard-cc car-accident" for="auto"></label>
																				<span class="caption">Car Accident</span>
																		</li>
																		<li class="case-type-m">
																				<input id="medical" type="radio" name="accident_type" value="Truck Accident"/>
																				<label class="drinkcard-cc truck-accident"for="medical"></label>
																				<span class="caption">Truck Accident</span>
																		</li>
			
																		<li class="case-type-m">
																				<input id="personal" type="radio" name="accident_type" value="Bicycle Accident" />
																				<label class="drinkcard-cc bicycle-accident" for="personal"></label>
																				<span class="caption">Bicycle Accident</span>
																		</li>
			
																		<li class="case-type-m">
																				<input id="productL" type="radio" name="accident_type" value="Motorcycle Accident"/>
																				<label class="drinkcard-cc motorcycle-accident" for="productL"></label>
																				<span class="caption">Motorcycle Accident</span>
																		</li>
			
																		<li class="case-type-m">
																				<input id="worker" type="radio" name="accident_type" value="Work-Related Accident"/>
																				<label class="drinkcard-cc work-related-accident" for="worker"></label>
																				<span class="caption">Work-Related</span>
																		</li>
																		<li class="case-type-m">
																				<input id="wrongful" type="radio" name="accident_type" value="Pedestrian Accident"/>
																				<label class="drinkcard-cc pedestrian-accident" for="wrongful"></label>
																				<span class="caption">Pedestrian Accident</span>
																		</li>
                                                                </ul>
                                                                <input type="hidden" name="case_type" id="casaType">
																<input type="hidden" name="test_mode" id="testMode" value="false">
															</div>
			
			
															<div class="form-group form-zip">
																	<input class="geo-complete form-control required" type="text" name="address" id="address" placeholder="Your Zip Code">

                                                                    <div class="address-info">
                                                                            <input type="hidden" name="address_street_number" id="address_street_number" data-geo="street_number">
                                                                            <input type="hidden" name="address_street_address" id="address_street_address" data-geo="route">
                                                                            <input type="hidden" name="address_zipcode" id="address_zipcode" data-geo="postal_code">
                                                                            <input type="hidden" name="addresss_city" id="addresss_city" data-geo="locality">
                                                                            <input type="hidden" name="address_county" id="address_county" data-geo="administrative_area_level_2">
                                                                            <input type="hidden" name="addresss_state" id="addresss_state"  data-geo="administrative_area_level_1">
                                                                            <input type="hidden" name="addresss_formated" id="addresss_formatted"  data-geo="formatted_address">
                                                                    </div>
															</div>
															<div class="form-group form-case-type">
																<select name="accidentType" class="wide">
																	<option value="Car Accident" selected="selected">Car Accident</option>
																	<option value="Motorcycle Accident">Motorcycle Accident</option>
																	<option value="Truck Accident">Truck Accident</option>
																	<option value="Bicycle Accident">Bicycle Accident</option>
																	<option value="Work-Related Accident">Work-Related Accident</option>
																	<option value="Pedestrian Accident">Pedestrian Accident</option>
																</select>
															</div>
														
                                                    </div>
                                                    
                                                    <div>   
                                                        <button id="submitv2" type="submit">Start Evaluation</button>

                                                    </div>
											
										</form>
									</div>
								</div>
							</div>
				</div>
			</section>
			<main class="content">
				<div class="hiw pt-4 pb-4">
					<div class="container">
						<div class="row">
							<h1 class="hiw-title">How It Works</h1>
							<span class="short-underline">
								<hr>
							</span>
							<div class="col-lg-4 hiw-box">
								<div><i class="pe-7s-search"></i></div>
								<div class="hiw-box-content">
									<h2>1. Find a Lawyer</h2>
									<p>Submit a contact form via desktop or mobile device</p>
								</div>
							</div>
							<div class="col-lg-4 hiw-box">
								<div><i class="pe-7s-info"></i></div>
								<div class="hiw-box-content">
									<h2>2. Get Connected</h2>
									<p>We directly connect your injury attorney in your area to you</p>
								</div>
							</div>
							<div class="col-lg-4 hiw-box">
								<div><i class="pe-7s-like2"></i></div>
								<div class="hiw-box-content">
									<h2>3. Free Consultation</h2>
									<p>You get a free, no obligation injury claim consultation</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="common-legal-areas pt-4">
					<div class="container">
						<div class="row">
							<h2 class="legal-areas-title">Most Common Legal Areas</h2>
							<span class="short-underline">
								<hr>
							</span>
							<div class="col-lg-4">
								<div class="populate_case_type card-deck toggle-hover-description legal-area">
									<div class="card mb-4">
										<div class="card-body">
											<a href="#" class="legal-area-image">
												<div class="car-accident-icon-blue logo"></div>
											</a>
											<h4 class="card-title">Car Accident</h4>
											<div class="legal-area-info ml-5 mr-5">
												<ul>
													<li>Car</li>
													<li>Motorcycle</li>
													<li>Commercial</li>
													<li>Hit and Run</li>
												</ul>
											</div>
											<div class="legal-area-description">
												Car Accidents — especially those that include body injuries — can bring about difficult times. 5.5 million car accidents occur every year in the U.S. — 3 million cause injuries and 40,000 are fatal. Speak with a top car accident lawyer today. We are here to help!
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="populate_case_type card-deck toggle-hover-description legal-area">
									<div class="card mb-4">
										<div class="card-body">
											<a href="#" class="legal-area-image">
												<div class="motor-accident-blue logo"></div>
											</a>
											<h4 class="card-title">Motorcycle Accident</h4>
											<div class="legal-area-info ml-5 mr-5">
												<ul>
													<li><a href="#">Misdiagnosis</a></li>
													<li><a href="#">Negligence</a></li>
													<li><a href="#">Child Birth</a></li>
													<li><a href="#">Nursing Home</a></li>
												</ul>
											</div>
											<div class="legal-area-description">
												Motorcycle accident injuries range from road burn and broken bones to spinal cord injuries and comas. Traumatic injuries can mean ambulances, hospitalization, MRIs, casts, and crutches. To get the maximum settlement, an injured biker needs an accident attorney.
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="populate_case_type card-deck toggle-hover-description legal-area">
									<div class="card mb-4">
										<div class="card-body">
											<a href="#" class="legal-area-image">
												<div class="truck-accident-blue logo"></div>
											</a>
											<h4 class="card-title">Truck Accident</h4>
											<div class="legal-area-info ml-5 mr-5">
												<ul>
													<li><a href="#">Slip and Fall</a></li>
													<li><a href="#">Wrongful Death</a></li>
													<li><a href="#">Premises Liability</a></li>
												</ul>
											</div>
											<div class="legal-area-description">
												Over the last 20 years, the amount of truck accidents has increased by 20%. According to the Federal Motor Carrier Safety Administration (FMCSA), more than 4,000 truck accidents result in fatalities and 130,000 people are injured each year. Get a free consultation today.
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-4">
								<div class="populate_case_type card-deck toggle-hover-description legal-area">
									<div class="card mb-4">
										<div class="card-body">
											<a href="#" class="legal-area-image">
												<div class="work-related-accident-blue logo"></div>
											</a>
											<h4 class="card-title">Work-Related</h4>
											<div class="legal-area-info ml-5 mr-5">
												<ul>
													<li><a href="#">Defective Design</a></li>
													<li><a href="#">Medical Device</a></li>
													<li><a href="#">Lack of Instruction</a></li>
													<li><a href="#">Risk/Benefit</a></li>
												</ul>
											</div>
											<div class="legal-area-description">
												Injured On The Job? U.S. Employers Spend Over $95 Billion Every Year On Workers’ Compensation Insurance. Get financial support for lost wages, medical bills, or disability. Over 140 Million Workers Are Covered — Chances Are, You Are Too. Get a free case review today.
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="populate_case_type card-deck toggle-hover-description legal-area">
									<div class="card mb-4">
										<div class="card-body">
											<a href="#" class="legal-area-image">
												<div class="ped-accident-blue logo"></div>
											</a>
											<h4 class="card-title">Pedestrian Accident</h4>
											<div class="legal-area-info ml-5 mr-5">
												<ul>
													<li><a href="#">Injured at Work</a></li>
													<li><a href="#">Disability Insurance</a></li>
													<li><a href="#">ERISA Disability</a></li>
													<li><a href="#">Denied Compensation</a></li>
												</ul>
											</div>
											<div class="legal-area-description">
												Have you been severely injured by a negligent driver? Accident.com believes no one should ever bear the burden of a recovery alone. Get help filing a claim against the vehicle owner’s or driver’s auto liability insurance policy. Speak with an accident lawyer today.
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="populate_case_type card-deck toggle-hover-description legal-area">
									<div class="card mb-4">
										<div class="card-body">
											<!--<div class=" ">-->
											<a href="#" class="legal-area-image">
												<div class="bicycle-accident-blue logo"></div>
											</a>
											<h4 class="card-title">Bicycle Accident</h4>
											<div class="legal-area-info ml-5 mr-5">
												<ul>
													<li><a href="#">Catostrophic Injury</a></li>
													<li><a href="#">Medical Malpractice</a></li>
													<li><a href="#">Negligence</a></li>
													<li><a href="#">Homicide</a></li>
												</ul>
											</div>
											<!--</div>-->
											<div class="legal-area-description">
												The National Highway Traffic Safety Administration (NHTSA) defines a bicycle crash as an accident that involves one or more motor vehicles when at least one of the vehicles was in transport. This type of accident generally occurs on a public traffic-way such as a road or highway.
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 ask-attorney">
								<span class="first-half">Don't see your case here?</span><span class="second-half"> You may still have a claim.</span>
								<button class="popover_info">Ask An Attorney</button>
							</div>
						</div>
					</div>
				</div>

				<div class="carousel-swipe pt-4">
					<div class="container">
							<h2 class="legal-areas-title">Most Common Legal Areas</h2>
							<span class="short-underline">
								<hr>
							</span>
						<div class="owl-carousel owl-theme owl-loaded owl-drag">
							<div class="col-lg-12">
								<div class="populate_case_type card-deck legal-area">
									<div class="card mb-4">
										<div class="card-body mx-auto">
											<a href="#">
												<div class="car-accident-icon-blue logo"></div>
											</a>
											<h4 class="card-title">Car Accident</h4>
											<ul>
												<li>car</li>
												<li>Motorcycle</li>
												<li>Commercial</li>
												<li>Hit and Run</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="populate_case_type card-deck legal-area">
									<div class="card mb-4">
										<div class="card-body mx-auto">
											<a href="#">
												<div class="motor-accident-blue logo"></div>
											</a>
											<h4 class="card-title">Motorcycle Accident</h4>
											<ul>
												<li><a href="#">Misdiagnosis</a></li>
												<li><a href="#">Negligence</a></li>
												<li><a href="#">Child Birth</a></li>
												<li><a href="#">Nursing Home</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="populate_case_type card-deck legal-area">
									<div class="card mb-4">
										<div class="card-body mx-auto">
											<a href="#">
												<div class="truck-accident-blue logo"></div>
											</a>
											<h4 class="card-title">Truck Accident</h4>
											<ul>
												<li><a href="#">Slip and Fall</a></li>
												<li><a href="#">Wrongful Death</a></li>
												<li><a href="#">Slip and Fall</a></li>
												<li><a href="#">Premises Liability</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="populate_case_type card-deck legal-area">
									<div class="card mb-4">
										<div class="card-body mx-auto">
											<a href="#">
												<div class="work-related-accident-blue logo"></div>
											</a>
											<h4 class="card-title">Work-Related</h4>
											<ul>
												<li><a href="#">Defective Design</a></li>
												<li><a href="#">Medical Device</a></li>
												<li><a href="#">Lack of Instruction</a></li>
												<li><a href="#">Risk/Benefit</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="populate_case_type card-deck legal-area">
									<div class="card mb-4">
										<div class="card-body mx-auto">
											<a href="#">
												<div class="ped-accident-blue logo"></div>
											</a>
											<h4 class="card-title">Pedestrian Accident</h4>
											<ul>
												<li><a href="#">Injured at Work</a></li>
												<li><a href="#">Disability Insurance</a></li>
												<li><a href="#">ERISA Disability</a></li>
												<li><a href="#">Denied Compensation</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="populate_case_type card-deck legal-area">
									<div class="card mb-4">
										<div class="card-body mx-auto">
											<!--<div class=" ">-->
											<a href="#">
												<div class="bicycle-accident-blue logo"></div>
											</a>
											<h4 class="card-title">Bicycle Accident</h4>
											<ul>
												<li><a href="#">Catostrophic Injury</a></li>
												<li><a href="#">Medical Malpractice</a></li>
												<li><a href="#">Negligence</a></li>
												<li><a href="#">Homicide</a></li>
											</ul>
											<!--</div>-->
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 ask-attorney">
								<span class="first-half">Don't see your case here?</span><span class="second-half"> You may still have a claim.</span>
								<button class="popover_info">Ask An Attorney</button>
							</div>
						</div>
					</div>
				</div>
				
				<div class="articles-resource-center py-5">
					<div class="container d-none">
						<h2 class="legal-areas-title">About Us</h2>
						<span class="short-underline">
							<hr>
						</span>
						<div class="row">
							<div class="col-md-6">
								<div class="card-body resource-center">
									<h4 class="title">Our Mission</h4>
									<p class="content">The Most Important Thing Is That You Have A Speedy Recovery And Receive The Support You Need. The Last Thing You Should Worry About Is Finding An Attorney Or Settling Your Insurance Claim. Let Us Do The Heavy Lifting So You Can Focus On What’s Important.</p>
									<p class="content">Attorneys Must Complete An Extensive Application Process To Join Our Network. Once We Receive An Application, We Vet All Of The Information Provided To Us To Ensure We Feel Comfortable With Them Contacting You.</p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card-body resource-center">
									<h4 class="title">Our Vision</h4>
									<p class="content">It’s 2018. Do People Actually Retain Lawyers Who Advertise On Billboards Or Sing Toll-Free Numbers On Television? Why Can’t Lawyers Connect With Us The Same Way Ride-Hailing And Home-Improvement Services Do?</p>
									<p class="content">You’re Not Alone. We Thought It Didn’t Make Sense Too. Using The Same Recent Advances In Technology That You Are Accustomed To, You Never Have To Spend Time Speaking To A Call Center Ever Again. Get Your Consultation From An Attorney Who Contacts You Directly.</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="card-body resource-center">
									<h4 class="title">Our Process</h4>
									<p class="content">If You Have Always Wondered If You Have A Case, But Were Worried That You Do Not Have The Finances To Afford An Attorney — The Time Is Now To Get Your Case Evaluated. Accident Attorneys Are Paid On A Contingency Basis. This Means That You Only Have To Pay A Fee If You Win Your Case.</p>
									<p class="content">We Have Made The Process Of Getting A Consultation As Easy As Humanly Possible. On Average It Takes Our Users Less Than Two Minutes To Describe Their Accident And Submit Their Contact Details. Our Proprietary Algorithm Then Matches You To The Closest Attorney In Our Network Who Accepts Your Case Type. Our System Immediately Notifies Your Attorney On Their Phone To Ensure They Can Get In Contact Quickly, Wherever, And Whenever.</p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card-body resource-center">
									<h4 class="title">Our Security</h4>
									<p class="content">Passionate About Confidentiality, We Store Each User’s Information In Encrypted Form. Your Encrypted Information Is Erased Soon After Your Submission In Accordance With The GDPR Regulations. We Never Sell Or Rent Your Information To Third Parties.</p>
									<p class="content">Our Proprietary Algorithm Matches You And Your Attorney, Which Eliminates The Need For Anyone But Your Attorney To View Your Details. Contact Information Remains Private When Case Details Are Given. A Unique Identification Code Ensures That Only Your Attorney Can Contact You. We Then Use Temporary, Anonymous Numbers For Your Consultation To Make Sure Your Attorney Can Get In Touch Immediately.  Your Contact Details Are Shared With Each Other Once Your Consultation Is Over.</p>
								</div>
							</div>
						</div>
					</div>

					@include('common.testimonials')
				</div>
		</div>


	</main>
	@include('partials.footer.footer_auto')
	<script>
			var loaderGif = "{{asset('images/loader.gif')}}";
            var app_url = "{{url('/v_typeform')}}";
            var case_type =  {!! json_encode($accident_type) !!} ;
			var test_mode =  {!! json_encode($test_mode) !!};
             
            $(window).on('load', function () {
                
                if($("input[name='accident_type']:checked").val() != ''){
                        $("#casaType").val($("input[name='accident_type']:checked").val());
                }

                $("input[name='accident_type']").change(function(){
                    if($(this).val() != ''){
                        $("#casaType").val($(this).val());
                    }
                });
            }); 

	</script>
</body>
</html>