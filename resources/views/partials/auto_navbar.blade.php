<nav id="mobile-menu">
	<a href="#" class="btn-close"></a>
	<ul class="nav-links">
		<li><a class="">Car Accident</a></li>
		<li><a class="">Truck Accident</a></li>
		<li><a class="">Motorcycle Accident</a></li>
		<li><a class="">Bicycle Accident</a></li>
		<li><a class="">Work-Related Accident</a></li>
		<li><a class="">Pedestrian Accident</a></li>
	</ul>
</nav>
<header class="main-header-wrap header-mobile">
	<div class="main-header">
		<a href="#" class="btn-menu-toggle"></a>
		<a href="/" class="logo">
			<!--<div class="site-logo"><div class="accident-logo"></div></div>-->
			<img src="{{URL::asset('images/accident_logo.png')}}">
		</a>
		<a href="#" class="popover_info_v2 btn-consultation btn-standard btn-orange text-get-started">Get Started</a>
		<a href="#" class="popover_info_v2 btn-consultation btn-standard btn-orange icon-get-started"><i class="icon-phone"></i></a>
	</div>
</header>
<header class="main-header-wrap header-desktop">
	<div class="main-header">
		<ul class="header-menu">
			<li><a>Car Accident</a></li>
			<li><a>Truck Accident</a></li>
			<li class="nav-item dropdown" data-toggle="collapse" data-target="#test">
				<a class="dropdown-toggle">Other Accidents</a>
				<ul class="nav nav-list collapse dropdown-content" id="test">
					<li><a>Motorcycle Accident</a></li>
					<li><a>Bicycle Accident</a></li>
					<li><a>Work-Related Accident</a></li>
					<li><a>Pedestrian Accident</a></li>
				</ul>
			</li>
		</ul>
		<a href="/" class="logo">
			<!--<div class="site-logo"><div class="accident-logo"></div></div>-->
			<img src="{{URL::asset('images/accident_logo.png')}}">
		</a>
		<a href="#" class="popover_info_v2 btn-consultation btn-standard btn-orange">Free Consultation</a>
	</div>
</header>