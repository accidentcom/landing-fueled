<nav id="mobile-menu">
	<a href="#" class="btn-close"></a>
	<ul class="nav-links">
		<li><a class="">Auto Accident</a></li>
		<li><a class="">Medical Malpractice</a></li>
		<li><a class="">Personal Injury</a></li>
		<li><a class="">Product Liability</a></li>
		<li><a class="">Worker's Comp</a></li>
		<li><a class="">Wrongful Death</a></li>
	</ul>
</nav>
<header class="main-header-wrap header-mobile">
	<div class="main-header">
		<a href="#" class="btn-menu-toggle"></a>
		<a href="/" class="logo">
			<!--<div class="site-logo"><div class="accident-logo"></div></div>-->
			<img src="{{URL::asset('images/accident_logo.png')}}">
		</a>
		<a href="#" class="popover_info_v2 btn-consultation btn-standard btn-orange text-get-started">Get Started</a>
		<a href="#" class="popover_info_v2 btn-consultation btn-standard btn-orange icon-get-started"><i class="icon-phone"></i></a>
	</div>
</header>
<header class="main-header-wrap header-desktop">
	<div class="main-header sml white-header">
		<ul class="header-menu">
			<li><a>Auto Accident</a></li>
			<li><a>Medical Malpractice</a></li>
			<li class="nav-item dropdown" data-toggle="collapse" data-target="#test">
				<a class="dropdown-toggle">Other Accidents</a>
				<ul class="nav nav-list collapse dropdown-content" id="test">
					<li><a>Personal Injury</a></li>
					<li><a>Product Liability</a></li>
					<li><a>Worker's Comp</a></li>
					<li><a>Wrongful Death</a></li>
				</ul>
			</li>
		</ul>
		<a href="/" class="logo">
			<!--<div class="site-logo"><div class="accident-logo"></div></div>-->
			<img src="{{URL::asset('images/accident_logo.png')}}">
		</a>
		<a href="#" class="popover_info_v2 btn-consultation btn-standard btn-orange">Free Consultation</a>
	</div>
</header>