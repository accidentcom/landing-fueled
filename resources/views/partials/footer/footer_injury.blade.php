<footer class="bg-accident-deepnavy page-footer">
	<div class="container text-md-left">
		<div class="row footer-links text-md-left pb-1">
			<div class="col-md-2 footer-logo mx-auto mt-3">
				<h6 class="mb-4 text-color-manatee font-weight-bold">Accident.com</h6>
			</div>
			<div class="col-md-3 mx-auto mt-3">
				<h6 class="mb-4 text-color-manatee font-weight-bold">Practice Areas</h6>
				<ul class="text-uppercase footer-font-small">
					<li class="populate_from_footer mb-2"><a href="#">Auto Accident</a></li>
					<li class="populate_from_footer mb-2"><a href="#">Medical Malpractice</a></li>
					<li class="populate_from_footer mb-2"><a href="#">Personal Injury</a></li>
					<li class="populate_from_footer mb-2"><a href="#">Product Liability</a></li>
					<li class="populate_from_footer mb-2"><a href="#">Worker's Comp</a></li>
					<li class="populate_from_footer mb-2"><a href="#">Wrongful Death</a></li>
				</ul>
			</div>
			<div class="d-none col-md-2 mx-auto mt-3">
				<h6 class="mb-4 text-color-manatee font-weight-bold">About Us</h6>
				<ul class="text-uppercase footer-font-small">
					<li class="mb-2"><a href="#">Our Mission</a></li>
					<li class="mb-2"><a href="#">Our Vision</a></li>
					<li class="mb-2"><a href="#">Our Process</a></li>
					<li class="mb-2"><a href="#">Our Security</a></li>
				</ul>
			</div>
			<div class="col-md-3 mx-auto mt-3">
				<h6 class="mb-4 text-color-manatee font-weight-bold">Important Links</h6>
				<ul class="text-uppercase footer-font-small">
					<li class="mb-2"><a href="{{url('/terms')}}" target="blank">Terms Of Service</a></li>
					<li class="mb-2 popover_info_v2"><a href="#">Contact A Lawyer</a></li>
				</ul>
			</div>
			<div class="col-md-3 contact-email mx-auto mt-3">
				<h6 class="mb-4 text-color-manatee font-weight-bold">We are always in touch with our clients</h6>
				<p class="text-uppercase footer-font-small">
					concierge@accident.com</p>
			</div>
		</div>
		<hr class="seperator">
		<div class="row d-flex align-items-center">
			<div class="col-md-12">
				<p class="text-color-manatee footer-font-small text-md-left">
					<small>ATTORNEY ADVERTISING. The information provided on this website is not legal advice. No attorney-client relationship is formed by the use of this site. The attorney listings on the site are paid-for attorney advertisements and do not constitute a referral or endorsement by a state agency or bar association. It is not stated or implied that a lawyer is certified as a specialist in any particular field of law. No results are guaranteed, and prior results do not guarantee a similar outcome. This site is informational, only, not dispositive; it is up to you to decide whether a particular lawyer is right for you. Contingency fee refers only to attorney’s fees; a client may incur or be liable for other costs or expenses. Use of this site is subject to your agreement to these Terms and Conditions. If you are seeking an attorney in Florida, please read the additional state advertising disclosure.</small>
				</p>
				<p class="text-center text-color-manatee footer-font-small text-md-left">
					<small>Copyright 2017. All right reserved.</small>
				</p>
			</div>
		</div>
	</div>
</footer>