<?php

return [
	'lead_qualify' => [
		'case_type' => [
			'Auto Accident' => [
				'autoaccidentZip' 					=> 'Where do you need the Auto Accident Attorney ?',
				'autoaccidentZipHidden' 			=> 'Where do you need the Auto Accident Attorney ?',
				'autoaccidentIncidentTime' 			=> 'How long ago did the incident occur ?',
				'autoaccidentPrimaryType' 			=> 'What is the Primary Type of Injury ?',
				'autoaccidentPoliceCase' 			=> 'Was a police report filed ?',
			],
			"Personal Injury" => [
				'personalinjuryZip' 				=> 'Where do you need the Personal Injury Attorney ?',
				'personalinjuryZipHidden' 			=> 'Where do you need the Personal Injury Attorney ?',
				'personalinjuryCause' 				=> 'What is the cause of the injury that you incurred ?',
				'personalinjuryClaimAction' 		=> 'Have you taken any action regarding your claim ?',
				'personalinjuryPrimaryType' 		=> 'What is the Primary Type of Injury ?',
				'personalinjuryIncidentTime' 		=> 'How long ago did the incident occur ?',
			],
			"Workers' Compensation" => [
				'workersCompensationZip' 			=> "Where do you need the Workers' Compensation Attorney ?",
				'workersCompensationZipHidden' 		=> "Where do you need the Workers' Compensation Attorney ?",
				'workersCompensationIncidentTime' 	=> 'How long ago did the incident occur ?',
				'workersCompensationClaimAction' 	=> 'Have you taken any action regarding your claim ?',
				'workersCompensationPrimaryType' 	=> 'What is the Primary Type of Injury ?',
			],
			"Wrongful Death" => [
				'wrongfulDeathZip' 					=> 'Where do you need the Wrongful Death Attorney ?',
				'wrongfulDeathZipHidden' 			=> 'Where do you need the Wrongful Death Attorney ?',
				'wrongfulDeathIncidentTime' 		=> "How long ago did the incident occur ?",
				'wrongfulDeathVictimRelation' 		=> 'What is your relationship to the victim ?',
				'wrongfulDeathVictimDeathCause' 	=> "What was the primary cause of the victim's death ?",
				'wrongfulDeathCriminalChargesFiled' => 'Were criminal charges filed against the responsible party ?',
			],
			"Medical Malpractice" => [
				'medicalMalpracticeZip' 			=> 'Where do you need the Medical Malpractice Attorney ?',
				'medicalMalpracticeZipHidden' 		=> 'Where do you need the Medical Malpractice Attorney ?',
				'medicalMalpracticeInjury' 			=> "Injuries from medical malpractice ?",
				'medicalMalpracticeIncidentTime' 	=> 'How long ago did the incident occur ?',
				'medicalMalpracticeClaimAction' 	=> "Have you taken any action regarding your claim?",
			],
			"Product Liability" => [
				'productliabilityZip' 				=> 'Where do you need the Medical Malpractice Attorney ?',
				'productliabilityZipHidden' 		=> 'Where do you need the Medical Malpractice Attorney ?',
				'productliabilityCause' 			=> "What is the cause of the injury that you incurred ?",
				'productliabilityIncidentTime' 		=> 'How long ago did the incident occur ?',
				'productliabilityPrimaryType' 		=> 'What is the Primary Type of Injury ?',
				'productliabilityClaimAction' 		=> "Have you taken any action regarding your claim?",
			]
		],
		'injuredAtWork' 	=> 'Did the Injury Occur at Work ?',
		'underwentSurgery' 	=> 'Did you Undergo a Surgery as a result of this incident ?',
		'caseDescription' 	=> 'Briefly describe your case.',
	],
	'LEAD_STATUS_NEW' => 6,
	'ATTORNEY_PRESENT_LEAD_STATUS' => 10,
	'ATTORNEY_NOT_PRESENT_LEAD_STATUS' => 11,
	'ASSIGNED_ATTORNEY_PRESENT_LEAD_STATUS' => 12,
	'LEAD_SPAM_SCORE' =>[
		'valid_lead'  => 1,
		'valid_email' => 2,
		'valid_phone' => 3,
		'spam_lead'   => 4
	],
	'EMAIL_SENT_EMAIL_TO_ATTORNEY' => 1,
	'EMAIL_SENT_EMAIL_TO_LEAD' => 2,
	'EMAIL_SENT_EMAIL_TO_ADMIN' => 3,
	'EMAIL_SENT_EMAIL_TO_NEW_LEAD_FEEDBACK' => 4,
	'EMAIL_SENT_TYPE_RATING' => 1,
	'EMAIL_SENT_TYPE_NOTIFICATION' => 2,
	'EMAIL_SENT_TYPE_VALUE' => [
		'Great' 	=> 1,
		'Good' 		=> 2,
		'Neutral' 	=> 3,
		'Bad' 		=> 4,
	],
	'EMAIL_SENT_STATUS_INACTIVE' => 0,
	'EMAIL_SENT_STATUS_ACTIVE' => 1,
	'FEEDBACK_SITE_URL' => 'https://accident.com/feedback',
	'SALT_KEY' => '1Qh6B0ekGiM3OCSIUAL4xZHJ1nTBe64P',
	'MIN_HASH_LENGTH' => 30,
	'UNSUBSCRIBE_LINK' => "https://customer.accident.com/unsubscribe",
	'TERMS_OF_SERVICE_LINK' => "https://accident.com/terms-of-service/",
	'CUSTOMER_SERVICE_EMAIL' => 'customerservice@accident.com',
	'TEAM_EMAIL' => 'team@accident.com',
	'DEVELOPERS_EMAIL' => 'developers@accident.com',
	'TEST_INTERCOM_INBOUND_EMAIL' => 'w4ecijk0@accidentcom-test.intercom-mail.com',
	'LIVE_INTERCOM_INBOUND_EMAIL' => 'b22xvgrz@inbound.intercom-mail.com',
	'CONTACTS_TO_SMS' => [
		'+16467011444', '+17187647159', '+16466457697'
	],
	'TEST_CONTACTS' => [
		'+919923260875'
	]
];