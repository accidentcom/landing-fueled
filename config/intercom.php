<?php
require_once base_path().'/../AccidentApp.php';

$accidentApp = new \AccidentApp();

return [
    'access_token' => $accidentApp->getIntercomToken(),
];